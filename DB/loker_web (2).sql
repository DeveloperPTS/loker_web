-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 27 Des 2018 pada 10.43
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loker_web`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `nama` varchar(25) DEFAULT NULL,
  `password` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `username`, `nama`, `password`, `created_at`) VALUES
(1, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', '2018-09-06 00:39:20'),
(2, 'kino', 'Budi kosim', 'e10adc3949ba59abbe56e057f20f883e', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin_loker`
--

CREATE TABLE `tb_admin_loker` (
  `id_admin_loker` int(11) NOT NULL,
  `nama_perusahaan` varchar(50) NOT NULL,
  `no_siup` varchar(30) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` text NOT NULL,
  `no_telp` varchar(14) NOT NULL,
  `email` varchar(30) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `ktp_admin` varchar(30) NOT NULL,
  `foto_sk` varchar(30) NOT NULL,
  `foto_perusahaan` varchar(30) NOT NULL,
  `status_aktif` enum('0','1') NOT NULL,
  `c_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `m_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_admin_loker`
--

INSERT INTO `tb_admin_loker` (`id_admin_loker`, `nama_perusahaan`, `no_siup`, `username`, `password`, `no_telp`, `email`, `alamat`, `ktp_admin`, `foto_sk`, `foto_perusahaan`, `status_aktif`, `c_date`, `m_date`, `deleted`) VALUES
(1, 'PT. HINO TEGAL', '34234234', 'hino', 'b56adba4c9052afc1b0afb17e67f4080', '0283446766', 'hino_tegal@hino.com', 'Kramat', '', '', '15362917673252.jpg', '1', '0000-00-00 00:00:00', '2018-12-26 22:01:15', ''),
(7, '', '', 'sinyo', 'ee4bd97b420ab6fde53c0f5eae8796fb', '', 'sinyo@gmail.com', '', '', '', '', '0', '2018-09-06 19:15:20', '2018-09-07 01:21:26', '0'),
(27, 'Pt.indo', '0898989', 'nazzul', 'b56adba4c9052afc1b0afb17e67f4080', '0789789', 'nazzul13@gmail.com', 'Tegal', '1536291767325.jpg', '15362917673251.jpg', '15362917673252.jpg', '1', '2018-09-07 10:42:47', '2018-09-25 21:09:38', '0'),
(33, 'tes', '09809809', 'uiuiuiuui', '7ad394af5bf6eb438fcfb77e999022e7', '098879988', 'sandytesar@gmial.com', 'ttt', '1543762717220.png', '15437627172201.png', '15437627172202.png', '1', '2018-12-02 21:58:37', '2018-12-02 22:09:05', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_loker_masuk`
--

CREATE TABLE `tb_loker_masuk` (
  `id_loker_masuk` int(11) NOT NULL,
  `id_post_loker` int(11) NOT NULL,
  `id_pendaftar` int(11) NOT NULL,
  `status` enum('terima','tidak') DEFAULT NULL,
  `lampiran` varchar(30) NOT NULL,
  `tanggal_masuk` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_loker_masuk`
--

INSERT INTO `tb_loker_masuk` (`id_loker_masuk`, `id_post_loker`, `id_pendaftar`, `status`, `lampiran`, `tanggal_masuk`) VALUES
(1, 4, 4, 'terima', '4_4.zip', '0000-00-00 00:00:00'),
(2, 3, 4, 'terima', '4_41.zip', '2018-09-23 07:15:53'),
(3, 4, 3, NULL, '4_42.zip', '2018-09-25 16:12:22'),
(4, 5, 4, NULL, '4_43.zip', '2018-09-26 02:06:33'),
(5, 6, 4, NULL, '6_4.zip', '2018-11-19 11:18:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pendaftar`
--

CREATE TABLE `tb_pendaftar` (
  `id_pendaftar` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `no_telp` varchar(14) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `tempat_lhr` varchar(20) NOT NULL,
  `tanggal_lhr` date NOT NULL,
  `pendidikan` varchar(20) NOT NULL,
  `jurusan` varchar(30) NOT NULL,
  `foto_ktp` varchar(30) NOT NULL,
  `foto_profil` varchar(30) NOT NULL,
  `c_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `m_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_aktif` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pendaftar`
--

INSERT INTO `tb_pendaftar` (`id_pendaftar`, `nama`, `username`, `password`, `email`, `no_telp`, `alamat`, `tempat_lhr`, `tanggal_lhr`, `pendidikan`, `jurusan`, `foto_ktp`, `foto_profil`, `c_date`, `m_date`, `status_aktif`) VALUES
(2, 'Adi Rangga', 'adibanyak', 'b69cb5ee6af2b11cb48cbdda620944f3', 'adi_rangga@gmail.com', '0877889888', 'Losari', 'Brebes', '1996-01-10', 'D4', 'Teknik Informatika', '', '', '2018-09-01 08:12:23', '2018-09-01 08:16:48', '1'),
(4, 'Ardi Fitra', 'ardi', '0264391c340e4d3cbba430cee7836eaf', 'ardi_fitra@gmail.com', '0877889888', 'Maribaya', 'Tegal', '1996-01-10', 'SMK', 'Otomotif Ringan', '', '', '2018-09-01 08:12:23', '2018-09-01 08:16:48', '1'),
(5, 'Ibnu Bachtiar Ulum', 'ibnu', '195ace8d50de761419faf08845304398', 'ibnu_bachtiar@gmail.com', '08778896786', 'Larangan, Brebes', 'Tegal', '1995-03-18', 'D4', 'Teknik Informatika', '', '', '2018-09-06 18:19:26', '2018-09-06 18:19:26', '1'),
(8, 'Tesar Sandy', 'ecang', '25d55ad283aa400af464c76d713c07ad', 'sandy@gmail.com', '085200532191', '', 'Tegal', '1994-12-08', '', '', '', '', '2018-12-01 02:10:02', '2018-12-26 22:14:09', '1'),
(9, 'Ecangs', 'sandy', '28b662d883b6d76fd96e4ddc5e9ba780', 'tes@gmail,co', '090999999090', 'asas', 'dss', '2018-12-13', '', '', '', '', '2018-12-01 02:12:30', '2018-12-01 02:12:30', '0'),
(10, 'Zidan', 'zidan', '6ec08f41d4c1f51a41e6a9b2f9a3b6d4', 'ecangsandy@gmail.com', '090999999090', 'tegal', 'Tegal', '1994-12-08', '', '', '', '', '2018-12-05 23:01:34', '2018-12-05 23:03:51', '1'),
(11, 'Rian gunawan', 'rian68gunawan', '6ec08f41d4c1f51a41e6a9b2f9a3b6d4', 'double68ki@gmail.com', '089667053375', 'brebes', 'brebes', '1996-12-08', '', '', '', '', '2018-12-08 18:25:22', '2018-12-08 18:25:22', '0'),
(12, 'Tesar', 'sandytesar', 'dca549085bc9f80efd9461bcf0884a1a', 'sandytesar@gmail.com', '9908993939', 'sandkdkkkkm', 'Tegal', '2002-12-06', '', '', '', '', '2018-12-08 23:12:04', '2018-12-08 23:12:04', '1'),
(13, 'Tes', 'tes123', '7678664c4db48a2ce3b6d9821b1ce884', 'sandytesar@gmail.com', '0999090', '<p>Tegak</p>', 'Tegal', '2009-02-07', 'S1', 'informatika', '', '', '2018-12-26 22:10:46', '2018-12-26 22:10:46', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_post_loker`
--

CREATE TABLE `tb_post_loker` (
  `id_post_loker` int(11) NOT NULL,
  `id_admin_loker` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `jenjang` enum('UMUM','SD','SMP','SMA','D1','D2','D3','D4','S1','S2') NOT NULL,
  `tanggal_berlaku` date NOT NULL,
  `foto_loker` varchar(30) NOT NULL,
  `tanggal_buat` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_post_loker`
--

INSERT INTO `tb_post_loker` (`id_post_loker`, `id_admin_loker`, `judul`, `deskripsi`, `jenjang`, `tanggal_berlaku`, `foto_loker`, `tanggal_buat`) VALUES
(1, 1, 'DIBUTUHKAN MEKANIK KENDARAAN BESAR', 'PT Hino Motors Manufacturing Indonesia sedang membuka lowongan kerja untuk posisi sebagai Operator Produksi. Jika Anda berminat, Anda harus memenuhi persyaratan yang sudah ditentukan sebagai berikut:\r\n\r\nPersyaratan:\r\nLaki-Laki\r\nsia Minimal 18 - 25 Maksimal\r\nPendidikan Maksimal SMK Jurusan Mesin Produksi, Otomotif, Listrik, Eloktro, maupun Mekotronika/SMA sederajat yang relavan dengan nilai rata-rata\r\nTinggi baadan minimal 165cm dengan berat badan Propesional.\r\nKesehatan dalam kondisi prima, tidak buta warna dan tidak berkacamata.\r\nBersedia kerja shift\r\nBisa bekerja dalam kelompok dan dalam tekanan\r\nMemiliki Motivasi disiplin jujur dan propesional kerja lebih di Utamakan\r\nPengalaman kerja di manufacture otomotif menjadi nilai tambah.\r\nPerempuan\r\nUsia 25 Maksimal\r\nPendidikan Maksimal SMK Jurusan, Listrik, Eloktro, maupun IPS / IPS/SMA sederajat yang relavan dengan nilai rata-rata\r\nTinggi baadan minimal 150cm dengan berat badan 50 kl,\r\nKesehatan dalam kondisi prima, tidak buta warna dan tidak berkacamata.\r\nBersedia kerja shift\r\nBisa bekerja dalam kelompok dan dalam tekanan\r\nMemiliki Motivasi disiplin jujur dan propesional kerja lebih di Utamakan\r\nTidak sedang bekerja / kuliah lebih di utama yang belum menikah\r\nPengalaman kerja di manufacture otomotif menjadi nilai tambah.\r\n\r\nBagi yang sesuai kualifikasi dan berminat untuk bekerjaan sebagai Operator Produksi di PT Hino Motors Manufacturing Indonesia, Anda bisa mempersiapkan berkas lamaran dibawah ini:\r\nDokumen lamaran\r\nSurat lamaran dan daftar riwayat hidup\r\nFoto copy Ijazah dan Transkrip Nilai\r\nFoto copy SKCK\r\nFoto copy KTP / KK / NPWP\r\nFoto copy Surat Keterangan Sehat dari Dokter\r\nFoto copy Kartu Pencari Kerja (Jika Ada)\r\nRefrensi Kerja (yang sudah pernah kerja)\r\nPas foto ukuran 3X4 2.lembar) dan 4X6 2.lembar)\r\n\r\nJika anda berminat dan dan sesuai dengan kualifikasi diatas untuk mengikuti penyeleksian kerja operator produksi PT Hino Motors Manufacturing Indonesia Tingkat Smk/Sma Sederajat', 'D3', '2018-10-10', '', '2018-09-06'),
(3, 1, 'Dibutuhkan Karyawan ', 'Dibutuhkan karyawan dengan kriteria :\r\n1. Manusia\r\n2. bependidikan\r\n3. Beragama', 'UMUM', '2018-09-28', '1537662503565.jpg', '0000-00-00'),
(4, 1, 'Dibutuhkan Karyawan ', '<p>Ini contoh Lamarana Pekerjaan </p><p>1. ini baris 1</p>', 'UMUM', '2018-09-26', '1537668416013.jpg', '0000-00-00'),
(5, 27, 'Membutuhkan Operator', '<p>membutuhkan operator dengan kriteria :<br></p><p>1. Wanita (20-28th)</p><p>2. Pendidikan SMA</p>', 'SMA', '2018-09-30', '1537884692687.jpg', '0000-00-00'),
(6, 1, 'Membutuhkan Operator', '<p>uuuu?</p>', 'UMUM', '2018-11-23', '1542622679029.jpg', '0000-00-00'),
(7, 1, 'Membutuhkan Operator 1', '<p>Memiliki Keahlian yang memadai</p>', 'UMUM', '2018-11-24', '1542809621938.jpg', '0000-00-00'),
(8, 1, 'Membutuhkan Operator', '<p>sdsdsds</p>', 'UMUM', '2018-12-30', '1543594820218.jpg', '0000-00-00'),
(9, 1, 'Admin', '<p>\r\n\r\n</p><div><p>Mengurusi hal-hal promosi seperti SNS dan media promosi lainnya.</p></div><h5>Tanggung Jawab Pekerjaan :</h5><div><p>Mengurusi dan bertanggung jawab atas media promosi seperti social media dan Website</p></div><h5>Syarat Pengalaman :</h5><div>Minimal 1 tahun di Bidang Promotion</div><h5>Keahlian :</h5><div><p>Mengerti segala macam promosi dan internet</p></div><h5>Kualifikasi :</h5><div><p>Pria/Wanita 25-30</p></div><h5>Tunjangan :</h5><div><p>Gaji, tempat tinggal</p></div><h5>Insentif :</h5><div><p>Bonus. insentif</p></div><h5>Waktu Bekerja :</h5><div>Senin – Minggu 9-19 setiap bulan 6 hari libur</div>\r\n\r\n<br><p></p>', 'D3', '2018-12-28', '1543931250054.jpg', '0000-00-00'),
(10, 1, 'Supervisor', '<p>Membutuhkan orang yang :</p><p>1. Cerdas</p><p>2. Jujur</p>', 'S1', '2018-12-31', '1545835975541.jpg', '2018-12-26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `tb_admin_loker`
--
ALTER TABLE `tb_admin_loker`
  ADD PRIMARY KEY (`id_admin_loker`);

--
-- Indexes for table `tb_loker_masuk`
--
ALTER TABLE `tb_loker_masuk`
  ADD PRIMARY KEY (`id_loker_masuk`);

--
-- Indexes for table `tb_pendaftar`
--
ALTER TABLE `tb_pendaftar`
  ADD PRIMARY KEY (`id_pendaftar`);

--
-- Indexes for table `tb_post_loker`
--
ALTER TABLE `tb_post_loker`
  ADD PRIMARY KEY (`id_post_loker`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_admin_loker`
--
ALTER TABLE `tb_admin_loker`
  MODIFY `id_admin_loker` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `tb_loker_masuk`
--
ALTER TABLE `tb_loker_masuk`
  MODIFY `id_loker_masuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_pendaftar`
--
ALTER TABLE `tb_pendaftar`
  MODIFY `id_pendaftar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tb_post_loker`
--
ALTER TABLE `tb_post_loker`
  MODIFY `id_post_loker` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
