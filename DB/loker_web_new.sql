-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 30 Jan 2019 pada 02.37
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loker_web`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `nama` varchar(25) DEFAULT NULL,
  `password` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `username`, `nama`, `password`, `created_at`) VALUES
(1, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', '2018-09-06 00:39:20'),
(2, 'kino', 'Budi kosim', 'e10adc3949ba59abbe56e057f20f883e', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin_loker`
--

CREATE TABLE `tb_admin_loker` (
  `id_admin_loker` int(11) NOT NULL,
  `nama_perusahaan` varchar(50) NOT NULL,
  `no_siup` varchar(30) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` text NOT NULL,
  `no_telp` varchar(14) NOT NULL,
  `email` varchar(30) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `ktp_admin` varchar(30) NOT NULL,
  `foto_sk` varchar(30) NOT NULL,
  `foto_perusahaan` varchar(30) NOT NULL,
  `status_aktif` enum('0','1') NOT NULL,
  `c_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `m_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_admin_loker`
--

INSERT INTO `tb_admin_loker` (`id_admin_loker`, `nama_perusahaan`, `no_siup`, `username`, `password`, `no_telp`, `email`, `alamat`, `ktp_admin`, `foto_sk`, `foto_perusahaan`, `status_aktif`, `c_date`, `m_date`, `deleted`) VALUES
(1, 'PT. HINO TEGAL', '34234234', 'hino', 'b56adba4c9052afc1b0afb17e67f4080', '0283446766', 'hino_tegal@hino.com', 'Kramat', '', '', '15362917673252.jpg', '1', '0000-00-00 00:00:00', '2018-12-26 22:01:15', ''),
(7, '', '', 'sinyo', 'ee4bd97b420ab6fde53c0f5eae8796fb', '', 'sinyo@gmail.com', '', '', '', '', '0', '2018-09-06 19:15:20', '2018-09-07 01:21:26', '0'),
(27, 'Pt.indo', '0898989', 'nazzul', 'b56adba4c9052afc1b0afb17e67f4080', '0789789', 'nazzul13@gmail.com', 'Tegal', '1536291767325.jpg', '15362917673251.jpg', '15362917673252.jpg', '1', '2018-09-07 10:42:47', '2018-09-25 21:09:38', '0'),
(28, 'Flux Cafe', '11908767', 'Flux Cafe', 'b271e110d043cc439dc6bf6b232bab6e', '08978000559', 'ardifx68@gmail.com', 'Jl. Halmahera no.23 Kota Tegal', '1547504418823.jpg', '15475044188231.jpg', '15475044188232.jpg', '1', '2019-01-15 06:20:18', '2019-01-15 06:23:56', '0'),
(33, 'tes', '09809809', 'uiuiuiuui', '7ad394af5bf6eb438fcfb77e999022e7', '098879988', 'sandytesar@gmial.com', 'ttt', '1543762717220.png', '15437627172201.png', '15437627172202.png', '1', '2018-12-02 21:58:37', '2018-12-02 22:09:05', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jenis_pekerjaan`
--

CREATE TABLE `tb_jenis_pekerjaan` (
  `id_jenis_pekerjaan` int(11) NOT NULL,
  `jenis_pekerjaan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jenis_pekerjaan`
--

INSERT INTO `tb_jenis_pekerjaan` (`id_jenis_pekerjaan`, `jenis_pekerjaan`) VALUES
(1, 'Sales1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_loker_masuk`
--

CREATE TABLE `tb_loker_masuk` (
  `id_loker_masuk` int(11) NOT NULL,
  `id_post_loker` int(11) NOT NULL,
  `id_pendaftar` int(11) NOT NULL,
  `status` enum('terima','tidak') DEFAULT NULL,
  `lampiran` varchar(30) NOT NULL,
  `tanggal_masuk` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_loker_masuk`
--

INSERT INTO `tb_loker_masuk` (`id_loker_masuk`, `id_post_loker`, `id_pendaftar`, `status`, `lampiran`, `tanggal_masuk`) VALUES
(1, 4, 4, 'terima', '4_4.zip', '0000-00-00 00:00:00'),
(2, 3, 4, 'terima', '4_41.zip', '2018-09-23 07:15:53'),
(3, 4, 3, NULL, '4_42.zip', '2018-09-25 16:12:22'),
(4, 5, 4, NULL, '4_43.zip', '2018-09-26 02:06:33'),
(5, 6, 4, NULL, '6_4.zip', '2018-11-19 11:18:54'),
(6, 9, 12, 'terima', '9_12.zip', '2019-01-29 18:02:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pendaftar`
--

CREATE TABLE `tb_pendaftar` (
  `id_pendaftar` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `no_telp` varchar(14) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `tempat_lhr` varchar(20) NOT NULL,
  `tanggal_lhr` date NOT NULL,
  `pendidikan` varchar(20) NOT NULL,
  `jurusan` varchar(30) NOT NULL,
  `foto_ktp` varchar(30) NOT NULL,
  `foto_profil` varchar(30) NOT NULL,
  `c_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `m_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_aktif` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pendaftar`
--

INSERT INTO `tb_pendaftar` (`id_pendaftar`, `nama`, `username`, `password`, `email`, `no_telp`, `alamat`, `tempat_lhr`, `tanggal_lhr`, `pendidikan`, `jurusan`, `foto_ktp`, `foto_profil`, `c_date`, `m_date`, `status_aktif`) VALUES
(2, 'Adi Rangga', 'adibanyak', 'b69cb5ee6af2b11cb48cbdda620944f3', 'adi_rangga@gmail.com', '0877889888', 'Losari', 'Brebes', '1996-01-10', 'D4', 'Teknik Informatika', '', '', '2018-09-01 08:12:23', '2018-09-01 08:16:48', '1'),
(4, 'Ardi Fitra', 'ardi', '0264391c340e4d3cbba430cee7836eaf', 'ardi_fitra@gmail.com', '0877889888', 'Maribaya', 'Tegal', '1996-01-10', 'SMK', 'Otomotif Ringan', '', '', '2018-09-01 08:12:23', '2018-09-01 08:16:48', '1'),
(5, 'Ibnu Bachtiar Ulum', 'ibnu', '195ace8d50de761419faf08845304398', 'ibnu_bachtiar@gmail.com', '08778896786', 'Larangan, Brebes', 'Tegal', '1995-03-18', 'D4', 'Teknik Informatika', '', '', '2018-09-06 18:19:26', '2018-09-06 18:19:26', '1'),
(8, 'Tesar Sandy', 'ecang', '25d55ad283aa400af464c76d713c07ad', 'sandy@gmail.com', '085200532191', '', 'Tegal', '1994-12-08', '', '', '', '', '2018-12-01 02:10:02', '2018-12-26 22:14:09', '1'),
(9, 'Ecangs', 'sandy', '28b662d883b6d76fd96e4ddc5e9ba780', 'tes@gmail,co', '090999999090', 'asas', 'dss', '2018-12-13', '', '', '', '', '2018-12-01 02:12:30', '2018-12-01 02:12:30', '0'),
(10, 'Zidan', 'zidan', '6ec08f41d4c1f51a41e6a9b2f9a3b6d4', 'ecangsandy@gmail.com', '090999999090', 'tegal', 'Tegal', '1994-12-08', '', '', '', '', '2018-12-05 23:01:34', '2018-12-05 23:03:51', '1'),
(11, 'Rian gunawan', 'rian68gunawan', '6ec08f41d4c1f51a41e6a9b2f9a3b6d4', 'double68ki@gmail.com', '089667053375', 'brebes', 'brebes', '1996-12-08', '', '', '', '', '2018-12-08 18:25:22', '2018-12-08 18:25:22', '0'),
(12, 'Tesar', 'sandytesar', '8a48e778ccfe208c579d35801c92490e', 'sandytesar@gmail.com', '9908993939', 'sandkdkkkkm', 'Tegal', '2002-12-06', '', '', '', '', '2018-12-08 23:12:04', '2019-01-30 00:01:14', '1'),
(13, 'Tes', 'tes123', '7678664c4db48a2ce3b6d9821b1ce884', 'sandytesar@gmail.com', '0999090', '<p>Tegak</p>', 'Tegal', '2009-02-07', 'S1', 'informatika', '', '', '2018-12-26 22:10:46', '2018-12-26 22:10:46', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_post_loker`
--

CREATE TABLE `tb_post_loker` (
  `id_post_loker` int(11) NOT NULL,
  `id_admin_loker` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `jenjang` enum('UMUM','SD','SMP','SMA','D1','D2','D3','D4','S1','S2') NOT NULL,
  `tanggal_berlaku` date NOT NULL,
  `foto_loker` varchar(30) NOT NULL,
  `tanggal_buat` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_post_loker`
--

INSERT INTO `tb_post_loker` (`id_post_loker`, `id_admin_loker`, `id_jenis`, `judul`, `deskripsi`, `jenjang`, `tanggal_berlaku`, `foto_loker`, `tanggal_buat`) VALUES
(1, 1, 0, 'DIBUTUHKAN MEKANIK KENDARAAN BESAR', 'PT Hino Motors Manufacturing Indonesia sedang membuka lowongan kerja untuk posisi sebagai Operator Produksi. Jika Anda berminat, Anda harus memenuhi persyaratan yang sudah ditentukan sebagai berikut:\r\n\r\nPersyaratan:\r\nLaki-Laki\r\nsia Minimal 18 - 25 Maksimal\r\nPendidikan Maksimal SMK Jurusan Mesin Produksi, Otomotif, Listrik, Eloktro, maupun Mekotronika/SMA sederajat yang relavan dengan nilai rata-rata\r\nTinggi baadan minimal 165cm dengan berat badan Propesional.\r\nKesehatan dalam kondisi prima, tidak buta warna dan tidak berkacamata.\r\nBersedia kerja shift\r\nBisa bekerja dalam kelompok dan dalam tekanan\r\nMemiliki Motivasi disiplin jujur dan propesional kerja lebih di Utamakan\r\nPengalaman kerja di manufacture otomotif menjadi nilai tambah.\r\nPerempuan\r\nUsia 25 Maksimal\r\nPendidikan Maksimal SMK Jurusan, Listrik, Eloktro, maupun IPS / IPS/SMA sederajat yang relavan dengan nilai rata-rata\r\nTinggi baadan minimal 150cm dengan berat badan 50 kl,\r\nKesehatan dalam kondisi prima, tidak buta warna dan tidak berkacamata.\r\nBersedia kerja shift\r\nBisa bekerja dalam kelompok dan dalam tekanan\r\nMemiliki Motivasi disiplin jujur dan propesional kerja lebih di Utamakan\r\nTidak sedang bekerja / kuliah lebih di utama yang belum menikah\r\nPengalaman kerja di manufacture otomotif menjadi nilai tambah.\r\n\r\nBagi yang sesuai kualifikasi dan berminat untuk bekerjaan sebagai Operator Produksi di PT Hino Motors Manufacturing Indonesia, Anda bisa mempersiapkan berkas lamaran dibawah ini:\r\nDokumen lamaran\r\nSurat lamaran dan daftar riwayat hidup\r\nFoto copy Ijazah dan Transkrip Nilai\r\nFoto copy SKCK\r\nFoto copy KTP / KK / NPWP\r\nFoto copy Surat Keterangan Sehat dari Dokter\r\nFoto copy Kartu Pencari Kerja (Jika Ada)\r\nRefrensi Kerja (yang sudah pernah kerja)\r\nPas foto ukuran 3X4 2.lembar) dan 4X6 2.lembar)\r\n\r\nJika anda berminat dan dan sesuai dengan kualifikasi diatas untuk mengikuti penyeleksian kerja operator produksi PT Hino Motors Manufacturing Indonesia Tingkat Smk/Sma Sederajat', 'D3', '2018-10-10', '', '2018-09-06'),
(3, 1, 1, 'Dibutuhkan Karyawan ', 'Dibutuhkan karyawan dengan kriteria :\r\n1. Manusia\r\n2. bependidikan\r\n3. Beragama', 'UMUM', '2018-12-28', '1537662503565.jpg', '2018-12-05'),
(8, 28, 1, 'Di butuhkan karyawan Cafe', '<p>Dibutuhkan karyawan FLUX CAFE Tegal</p><p>Jl.Halmahera no.56 Kota Tegal</p><p>Persaratan : </p><p>- Pria/Wanita usia Max.24 tahun (Belum menikah)</p><p>- Bepenampilan menarik</p><p>- Dapat mengendarai kendaraan  bermotor dan Mempunyai SIM C</p><p>Lampiran : </p><p>- Foto ijazah terakhir</p><p>- Foto SKCK</p><p>- Foto KTP</p><p>- Foto SIM C</p><p>- FOTO Diri 4x6</p>', 'UMUM', '2019-01-31', '1547505912905.jpg', '2019-01-14'),
(9, 1, 1, 'Di butuhkan karyawan manufacturing', '<p>Dibutuhkan karyawan mekanik kendaraan besar PT.HINO TEGAL</p><p>\r\n\r\nJl. Raya Keramat No.56, 445067\r\n\r\n<br></p><p>Persaratan :</p><p>- Pria usia max.30 tahun</p><p>- Lulusan SMK</p><p>- Sehat dan tidak memiliki riwayat penyakit </p><p>- Memiliki keterampilan di bidang otomotif</p><p>Lampiran :</p><p>- Foto ijazah</p><p>- Foto SKCK</p><p>- Foto surat keterangan sehat</p><p>- Foto sertifikat keahlian yang dimiliki</p><p>- Foto KTP</p><p>- Foto Diri 4x6</p>', 'SMA', '2019-02-15', '1548779275125.JPG', '2019-01-15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `tb_admin_loker`
--
ALTER TABLE `tb_admin_loker`
  ADD PRIMARY KEY (`id_admin_loker`);

--
-- Indexes for table `tb_jenis_pekerjaan`
--
ALTER TABLE `tb_jenis_pekerjaan`
  ADD PRIMARY KEY (`id_jenis_pekerjaan`);

--
-- Indexes for table `tb_loker_masuk`
--
ALTER TABLE `tb_loker_masuk`
  ADD PRIMARY KEY (`id_loker_masuk`);

--
-- Indexes for table `tb_pendaftar`
--
ALTER TABLE `tb_pendaftar`
  ADD PRIMARY KEY (`id_pendaftar`);

--
-- Indexes for table `tb_post_loker`
--
ALTER TABLE `tb_post_loker`
  ADD PRIMARY KEY (`id_post_loker`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_admin_loker`
--
ALTER TABLE `tb_admin_loker`
  MODIFY `id_admin_loker` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `tb_jenis_pekerjaan`
--
ALTER TABLE `tb_jenis_pekerjaan`
  MODIFY `id_jenis_pekerjaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_loker_masuk`
--
ALTER TABLE `tb_loker_masuk`
  MODIFY `id_loker_masuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_pendaftar`
--
ALTER TABLE `tb_pendaftar`
  MODIFY `id_pendaftar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tb_post_loker`
--
ALTER TABLE `tb_post_loker`
  MODIFY `id_post_loker` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
