<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('M_login', 'M_admin_loker'));
        $this->load->library('form_validation');
    }

    function index()
    {
    	$this->load->view('login');
    }

    function pilih_reg()
    {
        $this->load->view('pilih_reg');
    }

    function reg_admin()
    {
        $this->load->view('reg_admin');
    }

    function reg_user()
    {
        $this->load->view('reg_user');
    }
    function proses_reguser()
    {
        $data = array(
            'email' => $this->input->post('email',TRUE),
            'username' => $this->input->post('username',TRUE),
            'nama' => $this->input->post('nama',TRUE),
            'alamat' => $this->input->post('alamat',TRUE),
            'tanggal_lhr' => $this->input->post('tanggal_lhr',TRUE),
            'tempat_lhr' => $this->input->post('tempat_lhr',TRUE),
            'no_telp' => $this->input->post('no_telp',TRUE),
            'password' => md5($this->input->post('password',TRUE)),
            'status_aktif' => 1,
            );

        $this->M_login->reg_user($data);
        $this->session->set_flashdata('regist', 'Pendaftaran berhasil, admin akan meninjau permintaan anda');
        redirect(site_url('admin'));
    }

    public function proseslogin()
    {
        $where = array(
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password'))
            );
        $superadmin = $this->M_login->superadmin($where);
        $admin = $this->M_login->admin($where);
        $user = $this->M_login->user($where);

        if($superadmin->num_rows() > 0){
            $user = $superadmin->row();
            $session = array(
                'usertipe' => 'superadmin',
                'status_login' => true,
                'loginAdmin'  => true,
                'userdata' => $user,
                );
            $this->session->set_userdata($session);
            redirect(base_url('Admin/beranda'));
        }
        else if($admin->num_rows() > 0) {
            $user = $admin->row();
            if ($user->status_aktif == "1") {
                $session = array(
                    'usertipe' => 'admin',
                    'status_login' => true,
                    'loginAdmin'  => true,
                    'userdata' => $user,
                    );
                $this->session->set_userdata($session);
                redirect(base_url('Admin/beranda'));
            }else {
                $this->session->set_flashdata('error', 'Akun anda belum disetujui admin');
                redirect(base_url('Admin'));
            }
            
        }else if($user->num_rows() > 0) {
            $user = $user->row();
            $session = array(
                'usertipe' => 'user',
                'status_login' => true,
                'userdata' => $user,
                );
            $this->session->set_userdata($session);
            redirect(base_url('Web'));
        }
        else {
            $this->session->set_flashdata('error', 'Username atau Password salah!');
            redirect(base_url('Admin'));
        }
    }

    function beranda()
    {
        $this->template->load('template_back','admin/beranda');
    }
    public function jenis_loker()
    {
        $data['jenis_kerjaan'] = $this->db->get('tb_jenis_pekerjaan')->result();
        $this->template->load('template_back','jenis/jenis_list', $data);
    }
    public function jenis_loker_add()
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('Admin/create_action_jenis'),
            'id_jenis_pekerjaan' => set_value('id_jenis_pekerjaan'),
        
            'jenis_kerjaan' => set_value('jenis_kerjaan'),
            // 'keterangan' => set_value('keterangan'),
    );
        $this->template->load('template_back','jenis/jenis_form', $data);
        // $data['jenis_kerjaan'] = $this->db->get('tb_jenis_pekerjaan')->result();
        // $this->template->load('template_back','jenis/jenis_list', $data);
    }
    public function create_action_jenis()
    {
        $data = array(
            'jenis_pekerjaan' => set_value('jenis_kerjaan'),
            // 'keterangan' => set_value('keterangan'),
        );
        $this->db->insert('tb_jenis_pekerjaan', $data);
         $this->session->set_flashdata('message', 'Create Record Success');
        redirect(site_url('Admin/jenis_loker'));
    }
    public function update_jenis($id) 
    {
        $row = $this->db->get_where('tb_jenis_pekerjaan' , array('id_jenis_pekerjaan' =>$id))->row();

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('admin/update_jenis_action'),
        'id_jenis_pekerjaan' => set_value('id_post_loker', $row->id_jenis_pekerjaan),
        'jenis_kerjaan' => set_value('id_admin_loker', $row->jenis_pekerjaan),
        );
            $this->template->load('template_back','jenis/jenis_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('c_post_loker'));
        }
    }
    public function update_jenis_action()
    {
        $data = array(
            'jenis_pekerjaan' => $this->input->post('jenis_kerjaan'),
            // 'keterangan' => set_value('keterangan'),
        );
        $this->db->where('id_jenis_pekerjaan', $this->input->post('id_jenis_pekerjaan', TRUE))->update('tb_jenis_pekerjaan', $data);
        $this->session->set_flashdata('message', 'Update Record Success');
        redirect(site_url('admin/jenis_loker'));
    }
    public function delete_jenis($id='')
    {
        $this->db->where('id_jenis_pekerjaan', $id);
        $this->db->delete('tb_jenis_pekerjaan');
        $this->session->set_flashdata('message', 'Hapus Data berhasil');
        redirect(site_url('admin/jenis_loker'));
    }

    public function daftar_admin() 
    {
        $pass = $this->password_random();
        $config['upload_path']   = './upload/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']      = 2500;
        $config['file_name']     = round(microtime(true) * 1000);

        $this->upload->initialize($config);
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('ktp', $config))
        {
            $ktp = $this->upload->data();
        }
        if ($this->upload->do_upload('sk'))
        {
            $sk = $this->upload->data();
        }if($this->upload->do_upload('perusahaan'))
        {       
            
            $perusahaan = $this->upload->data();

            $data = array(
                'username' => $this->input->post('username'),
                'password' => md5($pass),
                'email' => $this->input->post('email'),
                'nama_perusahaan' => $this->input->post('nama_perusahaan'),
                'no_siup' => $this->input->post('no_siup'),
                'no_telp' => $this->input->post('no_telp'),
                'alamat' => $this->input->post('alamat'),
                'ktp_admin' => $ktp['file_name'],
                'foto_sk' => $sk['file_name'],
                'foto_perusahaan' => $perusahaan['file_name']
                );
            $cek = $this->M_admin_loker->get_by_email($this->input->post('email'));
            if ($cek->num_rows() > 0) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Email sudah terdaftar</div>');
                redirect(site_url('Admin/reg_admin'));
            }else{
                $this->M_admin_loker->insert($data);
                $id_tersimpan = $this->db->insert_id();

                $this->load->library('email');
                $config['protocol'] = "smtp";
                $config['smtp_host'] = 'ssl://smtp.gmail.com';
                $config['smtp_port'] = 465; 
                $config['smtp_user'] = 'pondoktegalku@gmail.com';
                // $config['smtp_from_name'] = 'Admin';
                $config['smtp_pass'] = 'Sandi123';
                $config['wordwrap'] = TRUE;
                $config['newline'] = "\r\n";
                $config['mailtype'] = 'html';
                $this->email->initialize($config);

                $this->email->from('noreply', 'Kata Sandi Akun Loker Tegal');
                $list = array($this->input->post('email'));
                $this->email->to($list);
                $this->email->subject('Kata Sandi Akun Loker Tegal');
                $this->email->message('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns:th="http://www.thymeleaf.org" xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <title>VERIFIKASI AKUN</title>

                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

                        <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css"/>
                        <style>
                            body {
                                font-family: "Roboto", sans-serif;
                                font-size: 48px;
                            }
                        </style>
                    </head>
                    <body style="margin: 0; padding: 0;">

                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                            <tr>
                                <td align="center" bgcolor="#4485f5" style="padding: 40px 0 30px 0;">
                                    <h2 style="color : #fff">Loker Tegal</h2>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" bgcolor="#eaeaea" style="padding: 40px 30px 40px 30px;">
                                    <h4>Akun anda berhasil disimpan, gunakan password</h4>
                                    <h1>'.$pass.'</h1>
                                    <h4>Silahkan login menggunakan nama pengguna anda dengan kata sandi di atas</h4>
                                    <h4>Terimakasih</h4>
                                </td>
                            </tr>
                        </table>

                    </body>
                    </html>');

                if ($this->email->send()) {
                    $this->session->set_flashdata('message', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Pendaftaran berhasil, kata sandi anda akan dikirim melalui email</div>');
                    redirect(site_url('Admin/reg_admin'));        
                }else{
                    $this->db->where('id_admin_loker', $id_tersimpan)->delete('tb_admin_loker');
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Pendaftaran gagal, lakukan beberapa saat lagi '.$this->email->print_debugger().'</div>');
                    redirect(site_url('Admin/reg_admin'));
                }           

            }

        }else{
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Pendaftaran gagal, gambar tidak dapat disimpan</div>');
            redirect(site_url('Admin/reg_admin'));
        }  
        
    }

    function unggah_foto()
    {
        $config['upload_path']          = './upload/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = round(microtime(true) * 1000);
 
        $this->upload->initialize($config);
 
        if(!$this->upload->do_upload('gambar')) //upload and validate
        {
            $data['inputerror'][] = 'gambar';
            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        }
        return $this->upload->data('file_name');
    }

    public function password_random(){

        $string = "abcdefghijklmnopqrstuvwxyz0123456789";
        for($i=0; $i<=8; $i++){
            $pos = rand(0,36);
            $str .= $string{$pos};
        }
        return $str;
    }

    public function profil($id) 
    {
        $row = $this->M_admin_loker->get_by_id($id);
            $session = array(
                'usertipe' => 'admin',
                'status_login' => true,
                'loginAdmin'  => true,
                'userdata' => $row,
                );
            $this->session->set_userdata($session);

        $data = array(
            'button' => 'Ubah',
            'action' => site_url('Admin/simpan_profil'),
            'id_admin_loker' => set_value('id_admin_loker', $row->id_admin_loker),
            'nama_perusahaan' => set_value('nama_perusahaan', $row->nama_perusahaan),
            'no_siup' => set_value('no_siup', $row->no_siup),
            'username' => set_value('username', $row->username),
            'no_telp' => set_value('no_telp', $row->no_telp),
            'email' => set_value('email', $row->email),
            'alamat' => set_value('alamat', $row->alamat),
            'ktp_admin' => set_value('ktp_admin', $row->ktp_admin),
            'foto_sk' => set_value('foto_sk', $row->foto_sk),
            'foto_perusahaan' => set_value('foto_perusahaan', $row->foto_perusahaan),
            );
        $this->template->load('template_back','admin/profil_saya', $data);
        
    }

    public function simpan_profil() 
    {
       
            $data = array(
        'nama_perusahaan' => $this->input->post('nama_perusahaan'),
        'no_siup' => $this->input->post('no_siup'),
        'username' => $this->input->post('username'),
        'no_telp' => $this->input->post('no_telp'),
        'email' => $this->input->post('email'),
        'alamat' => $this->input->post('alamat'),
        'ktp_admin' => $this->input->post('ktp_admin'),
        'foto_sk' => $this->input->post('foto_sk'),
        'foto_perusahaan' => $this->input->post('foto_perusahaan'),
        );

            $this->M_admin_loker->update($this->input->post('id_admin_loker'), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('Admin/profil/'.$this->input->post('id_admin_loker')));
        
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url('Admin'));
    }
    public function tes()
    {
        $this->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = 465; 
        $config['smtp_user'] = 'pondoktegalku@gmail.com';
        // $config['smtp_from_name'] = 'Admin';
        $config['smtp_pass'] = 'Sandi123';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';
                $this->email->initialize($config);

                $this->email->from('noreply', 'Kata Sandi Akun Loker Tegal');
                $list = array('');
                $this->email->to('sandytesar@gmail.com');
                $this->email->subject('Kata Sandi Akun Loker Tegal');
                $this->email->message('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns:th="http://www.thymeleaf.org" xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <title>VERIFIKASI AKUN</title>

                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

                        <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css"/>
                        <style>
                            body {
                                font-family: "Roboto", sans-serif;
                                font-size: 48px;
                            }
                        </style>
                    </head>
                    <body style="margin: 0; padding: 0;">

                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                            <tr>
                                <td align="center" bgcolor="#4485f5" style="padding: 40px 0 30px 0;">
                                    <h2 style="color : #fff">Loker Tegal</h2>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" bgcolor="#eaeaea" style="padding: 40px 30px 40px 30px;">
                                    <h4>Akun anda berhasil disimpan, gunakan password</h4>
                                    <h1>ss</h1>
                                    <h4>Silahkan login menggunakan nama pengguna anda dengan kata sandi di atas</h4>
                                    <h4>Terimakasih</h4>
                                </td>
                            </tr>
                        </table>

                    </body>
                    </html>');

                if ($this->email->send()) {

                    $this->session->set_flashdata('message', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Pendaftaran berhasil, kata sandi anda akan dikirim melalui email</div>');
                    redirect(site_url('Admin/reg_admin'));        
                }else{
                    $error = $this->email->print_debugger();
                    // $this->db->where('id_admin_loker', $id_tersimpan)->delete('tb_admin_loker');
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Pendaftaran gagal, '.$error.'</div>');
                    redirect(site_url('Admin/reg_admin'));
                }
    }
}