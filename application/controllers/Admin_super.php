<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_super extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin');
        $this->load->library('form_validation');
    }
}