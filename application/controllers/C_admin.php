<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'M_admin/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'M_admin/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'M_admin/index.html';
            $config['first_url'] = base_url() . 'M_admin/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->M_admin->total_rows($q);
        $M_admin = $this->M_admin->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'M_admin_data' => $M_admin,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('M_admin/tb_admin_list', $data);
    }

    public function read($id) 
    {
        $row = $this->M_admin->get_by_id($id);
        if ($row) {
            $data = array(
		'id_admin' => $row->id_admin,
		'username' => $row->username,
		'nama' => $row->nama,
		'password' => $row->password,
		'created_at' => $row->created_at,
	    );
            $this->load->view('M_admin/tb_admin_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('M_admin'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('M_admin/create_action'),
	    'id_admin' => set_value('id_admin'),
	    'username' => set_value('username'),
	    'nama' => set_value('nama'),
	    'password' => set_value('password'),
	    'created_at' => set_value('created_at'),
	);
        $this->load->view('M_admin/tb_admin_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'username' => $this->input->post('username',TRUE),
		'nama' => $this->input->post('nama',TRUE),
		'password' => $this->input->post('password',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
	    );

            $this->M_admin->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('M_admin'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->M_admin->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('M_admin/update_action'),
		'id_admin' => set_value('id_admin', $row->id_admin),
		'username' => set_value('username', $row->username),
		'nama' => set_value('nama', $row->nama),
		'password' => set_value('password', $row->password),
		'created_at' => set_value('created_at', $row->created_at),
	    );
            $this->load->view('M_admin/tb_admin_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('M_admin'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_admin', TRUE));
        } else {
            $data = array(
		'username' => $this->input->post('username',TRUE),
		'nama' => $this->input->post('nama',TRUE),
		'password' => $this->input->post('password',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
	    );

            $this->M_admin->update($this->input->post('id_admin', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('M_admin'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->M_admin->get_by_id($id);

        if ($row) {
            $this->M_admin->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('M_admin'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('M_admin'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('username', 'username', 'trim|required');
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('password', 'password', 'trim|required');
	$this->form_validation->set_rules('created_at', 'created at', 'trim|required');

	$this->form_validation->set_rules('id_admin', 'id_admin', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file M_admin.php */
/* Location: ./application/controllers/M_admin.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-08-31 08:41:37 */
/* http://harviacode.com */