<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_admin_loker extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin_loker');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['c_admin_loker_data'] = $this->M_admin_loker->get_nonactive();
        $this->template->load('template_back','c_admin_loker/tb_admin_loker_list', $data);
    }

    public function aktif()
    {
        $data['c_admin_loker_data'] = $this->M_admin_loker->get_active();
        $this->template->load('template_back','c_admin_loker_aktif/tb_admin_loker_list', $data);
    }

    public function read($id) 
    {
        $row = $this->M_admin_loker->get_by_id($id);
        if ($row) {
            $data = array(
		'id_admin_loker' => $row->id_admin_loker,
		'nama_perusahaan' => $row->nama_perusahaan,
		'no_siup' => $row->no_siup,
		'username' => $row->username,
		'password' => $row->password,
		'no_telp' => $row->no_telp,
		'email' => $row->email,
		'alamat' => $row->alamat,
		'ktp_admin' => $row->ktp_admin,
		'foto_sk' => $row->foto_sk,
		'foto_perusahaan' => $row->foto_perusahaan,
		'status_aktif' => $row->status_aktif,
		'c_date' => $row->c_date,
		'm_date' => $row->m_date,
		'deleted' => $row->deleted,
	    );
            $this->template->load('template_back','c_admin_loker/tb_admin_loker_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('c_admin_loker'));
        }
    }

    public function detail($id) 
    {
        $row = $this->M_admin_loker->get_by_id($id);
        if ($row) {
            $data = array(
		'id_admin_loker' => $row->id_admin_loker,
		'nama_perusahaan' => $row->nama_perusahaan,
		'no_siup' => $row->no_siup,
		'username' => $row->username,
		'password' => $row->password,
		'no_telp' => $row->no_telp,
		'email' => $row->email,
		'alamat' => $row->alamat,
		'ktp_admin' => $row->ktp_admin,
		'foto_sk' => $row->foto_sk,
		'foto_perusahaan' => $row->foto_perusahaan,
		'status_aktif' => $row->status_aktif,
		'c_date' => $row->c_date,
		'm_date' => $row->m_date,
		'deleted' => $row->deleted,
	    );
            $this->template->load('template_back','c_admin_loker_aktif/tb_admin_loker_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('c_admin_loker'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('c_admin_loker/create_action'),
	    'id_admin_loker' => set_value('id_admin_loker'),
	    'nama_perusahaan' => set_value('nama_perusahaan'),
	    'no_siup' => set_value('no_siup'),
	    'username' => set_value('username'),
	    'password' => set_value('password'),
	    'no_telp' => set_value('no_telp'),
	    'email' => set_value('email'),
	    'alamat' => set_value('alamat'),
	    'ktp_admin' => set_value('ktp_admin'),
	    'foto_sk' => set_value('foto_sk'),
	    'foto_perusahaan' => set_value('foto_perusahaan'),
	    'status_aktif' => set_value('status_aktif'),
	    'c_date' => set_value('c_date'),
	    'm_date' => set_value('m_date'),
	    'deleted' => set_value('deleted'),
	);
        $this->template->load('template_back','c_admin_loker/tb_admin_loker_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_perusahaan' => $this->input->post('nama_perusahaan',TRUE),
		'no_siup' => $this->input->post('no_siup',TRUE),
		'username' => $this->input->post('username',TRUE),
		'password' => $this->input->post('password',TRUE),
		'no_telp' => $this->input->post('no_telp',TRUE),
		'email' => $this->input->post('email',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'ktp_admin' => $this->input->post('ktp_admin',TRUE),
		'foto_sk' => $this->input->post('foto_sk',TRUE),
		'foto_perusahaan' => $this->input->post('foto_perusahaan',TRUE),
		'status_aktif' => $this->input->post('status_aktif',TRUE),
		'c_date' => $this->input->post('c_date',TRUE),
		'm_date' => $this->input->post('m_date',TRUE),
		'deleted' => $this->input->post('deleted',TRUE),
	    );

            $this->M_admin_loker->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('c_admin_loker'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->M_admin_loker->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Ubah',
                'action' => site_url('c_admin_loker/update_action'),
		'id_admin_loker' => set_value('id_admin_loker', $row->id_admin_loker),
		'nama_perusahaan' => set_value('nama_perusahaan', $row->nama_perusahaan),
		'no_siup' => set_value('no_siup', $row->no_siup),
		'username' => set_value('username', $row->username),
		'password' => set_value('password', $row->password),
		'no_telp' => set_value('no_telp', $row->no_telp),
		'email' => set_value('email', $row->email),
		'alamat' => set_value('alamat', $row->alamat),
		'ktp_admin' => set_value('ktp_admin', $row->ktp_admin),
		'foto_sk' => set_value('foto_sk', $row->foto_sk),
		'foto_perusahaan' => set_value('foto_perusahaan', $row->foto_perusahaan),
		'status_aktif' => set_value('status_aktif', $row->status_aktif),
		'c_date' => set_value('c_date', $row->c_date),
		'm_date' => set_value('m_date', $row->m_date),
		'deleted' => set_value('deleted', $row->deleted),
	    );
            $this->template->load('template_back','c_admin_loker/tb_admin_loker_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('c_admin_loker'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_admin_loker', TRUE));
        } else {
            $data = array(
		'nama_perusahaan' => $this->input->post('nama_perusahaan',TRUE),
		'no_siup' => $this->input->post('no_siup',TRUE),
		'username' => $this->input->post('username',TRUE),
		'no_telp' => $this->input->post('no_telp',TRUE),
		'email' => $this->input->post('email',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'ktp_admin' => $this->input->post('ktp_admin',TRUE),
		'foto_sk' => $this->input->post('foto_sk',TRUE),
		'foto_perusahaan' => $this->input->post('foto_perusahaan',TRUE),
	    );

            $this->M_admin_loker->update($this->input->post('id_admin_loker', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('c_admin_loker'));
        }
    }

    public function setujui($id_admin_loker)
    {
        $this->M_admin_loker->update($id_admin_loker, array('status_aktif'=>'1'));
        redirect(site_url('c_admin_loker'));
    }
    
    public function delete($id) 
    {
        $row = $this->M_admin_loker->get_by_id($id);

        if ($row) {
            $this->M_admin_loker->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('c_admin_loker'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('c_admin_loker'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_perusahaan', 'nama perusahaan', 'trim|required');
	$this->form_validation->set_rules('no_siup', 'no siup', 'trim|required');
	$this->form_validation->set_rules('username', 'username', 'trim|required');
	$this->form_validation->set_rules('password', 'password', 'trim|required');
	$this->form_validation->set_rules('no_telp', 'no telp', 'trim|required');
	$this->form_validation->set_rules('email', 'email', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
	$this->form_validation->set_rules('ktp_admin', 'ktp admin', 'trim|required');
	$this->form_validation->set_rules('foto_sk', 'foto sk', 'trim|required');
	$this->form_validation->set_rules('foto_perusahaan', 'foto perusahaan', 'trim|required');
	$this->form_validation->set_rules('status_aktif', 'status aktif', 'trim|required');
	$this->form_validation->set_rules('c_date', 'c date', 'trim|required');
	$this->form_validation->set_rules('m_date', 'm date', 'trim|required');
	$this->form_validation->set_rules('deleted', 'deleted', 'trim|required');

	$this->form_validation->set_rules('id_admin_loker', 'id_admin_loker', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}
	public function cetak_laporan()
    {
        $data['c_admin_loker'] = $this->db->select('*')
        ->get('tb_admin_loker')->result();
        $this->load->view('c_admin_loker_aktif/cetak_laporan', $data);
        $html = $this->output->get_output();

        $this->dompdf->set_paper('legal', 'landscape');

        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("Laporan.pdf", array('Attachment'=>0));
    }

}

/* End of file C_admin_loker.php */
/* Location: ./application/controllers/C_admin_loker.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-08-31 08:42:18 */
/* http://harviacode.com */