<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_loker_masuk extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_loker_masuk');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $id = $this->session->userdata('userdata')->id_admin_loker;
        $data['c_post_loker'] = $this->db->get_where('tb_post_loker', array('id_admin_loker'=> $id))->result();
        // echo $id;
        $this->template->load('template_back','c_loker_masuk/tb_list_loker', $data);
    }

    public function loker_masuk($id)
    {
        // $id = $this->session->userdata('userdata')->id_admin_loker;
        $data['c_post_loker_masuk'] = $this->M_loker_masuk->get_by_admin($id);
        // echo json_encode($data);
        $this->template->load('template_back','c_loker_masuk/tb_loker_masuk_list', $data);
    }

    // public function index()
    // {
    //     $q = urldecode($this->input->get('q', TRUE));
    //     $start = intval($this->input->get('start'));
        
    //     if ($q <> '') {
    //         $config['base_url'] = base_url() . 'c_loker_masuk/index.html?q=' . urlencode($q);
    //         $config['first_url'] = base_url() . 'c_loker_masuk/index.html?q=' . urlencode($q);
    //     } else {
    //         $config['base_url'] = base_url() . 'c_loker_masuk/index.html';
    //         $config['first_url'] = base_url() . 'c_loker_masuk/index.html';
    //     }

    //     $config['per_page'] = 10;
    //     $config['page_query_string'] = TRUE;
    //     $config['total_rows'] = $this->M_loker_masuk->total_rows($q);
    //     $c_loker_masuk = $this->M_loker_masuk->get_limit_data($config['per_page'], $start, $q);

    //     $this->load->library('pagination');
    //     $this->pagination->initialize($config);

    //     $data = array(
    //         'c_loker_masuk_data' => $c_loker_masuk,
    //         'q' => $q,
    //         'pagination' => $this->pagination->create_links(),
    //         'total_rows' => $config['total_rows'],
    //         'start' => $start,
    //     );
    //     $this->load->view('c_loker_masuk/tb_loker_masuk_list', $data);
    // }

    public function read($id) 
    {
        $row = $this->M_loker_masuk->get_by_id($id);
        if ($row) {
            $data = array(
		'judul' => $row->judul,
        'id_post_loker' => $row->id_post_loker,
		'nama' => $row->nama,
        'no_telp' => $row->no_telp,
		'id_pendaftar' => $row->id_pendaftar,
		'status' => $row->status,
		'lampiran' => $row->lampiran,
		'tanggal_masuk' => $row->tanggal_masuk,
	    );
             $this->template->load('template_back','c_loker_masuk/tb_loker_masuk_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('c_loker_masuk'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('c_loker_masuk/create_action'),
	    'id_loker_masuk' => set_value('id_loker_masuk'),
	    'id_post_loker' => set_value('id_post_loker'),
	    'id_pendaftar' => set_value('id_pendaftar'),
	    'status' => set_value('status'),
	    'lampiran' => set_value('lampiran'),
	    'tanggal_masuk' => set_value('tanggal_masuk'),
	);
        $this->load->view('c_loker_masuk/tb_loker_masuk_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_post_loker' => $this->input->post('id_post_loker',TRUE),
		'id_pendaftar' => $this->input->post('id_pendaftar',TRUE),
		'status' => $this->input->post('status',TRUE),
		'lampiran' => $this->input->post('lampiran',TRUE),
		'tanggal_masuk' => $this->input->post('tanggal_masuk',TRUE),
	    );

            $this->M_loker_masuk->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('c_loker_masuk'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->M_loker_masuk->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('c_loker_masuk/update_action'),
                'id_loker_masuk' => set_value('id_loker_masuk', $row->id_loker_masuk),
                'id_post_loker' => set_value('id_post_loker', $row->id_post_loker),
                'id_pendaftar' => set_value('id_pendaftar', $row->id_pendaftar),
                'status' => set_value('status', $row->status),
                'lampiran' => set_value('lampiran', $row->lampiran),
                'tanggal_masuk' => set_value('tanggal_masuk', $row->tanggal_masuk),
            );
            $this->template->load('template_back','c_loker_masuk/tb_loker_masuk_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('c_loker_masuk'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();
        $id = $this->input->post('id_post_loker');
        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_loker_masuk', TRUE));
        } else {
            $data = array(
                'id_post_loker' => $this->input->post('id_post_loker',TRUE),
                'id_pendaftar' => $this->input->post('id_pendaftar',TRUE),
                'status' => $this->input->post('status',TRUE),
                'lampiran' => $this->input->post('lampiran',TRUE),
                'tanggal_masuk' => $this->input->post('tanggal_masuk',TRUE),
            );
           $get_pendaftar = $this->db->get_where('tb_pendaftar', array('id_pendaftar' => $this->input->post('id_pendaftar',TRUE)))->row();
        $this->send_mail($get_pendaftar->email);
        if ($this->email->send()) {
        $this->M_loker_masuk->update($this->input->post('id_loker_masuk', TRUE), $data);
        redirect('c_loker_masuk/lihat_lamaran_masuk/'.$id, 'refresh');
        } else {
            $this->session->set_flashdata('message', 'Gagal merubah status');
            redirect(site_url('c_post_loker_masuk'));
        }
        }
    }
    
    public function delete($id) 
    {
        $row = $this->M_loker_masuk->get_by_id($id);

        if ($row) {
            $this->M_loker_masuk->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('c_loker_masuk'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('c_loker_masuk'));
        }
    }

    public function lihat_lamaran_masuk($id_post_loker)
    {
        $row = $this->M_loker_masuk->get_by_loker($id_post_loker);
        $data['data_loker_masuk'] = $row;


        $this->template->load('template_back','c_loker_masuk/tb_loker_list_daftar', $data);
    }
    public function download($file){
        $this->load->helper('download');
        $path = 'lamaran/'.$file;
        force_download($path,NULL);
    }
    public function get_ajax($id)
    {
        $row = $this->M_loker_masuk->get_by_id($id);
        echo json_encode($row);
    }

    public function ubah_status()
    {
        $id = $this->input->post('id_loker_masuk');
        $row = $this->M_loker_masuk->get_by_id($id);
        $update = array (
            'status' => $this->input->post('status')
        ); 
        $get_pendaftar = $this->db->get_where('tb_pendaftar', array('id_pendaftar' => $row->id_pendaftar))->row();
        $this->send_mail($get_pendaftar->email,$row->judul);
        if ($this->input->post('status') == 'terima') {
            if ($this->email->send()) {
            $this->M_loker_masuk->update($id, $update);
            redirect('c_loker_masuk/lihat_lamaran_masuk/'.$row->id_post_loker, 'refresh');
            } else {
                $this->session->set_flashdata('message', 'Gagal merubah status');
                redirect(site_url('c_loker_masuk'));
            }
        } else {
            $this->M_loker_masuk->update($id, $update);
            redirect('c_loker_masuk/lihat_lamaran_masuk/'.$row->id_post_loker, 'refresh');
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_post_loker', 'id post loker', 'trim|required');
	$this->form_validation->set_rules('id_pendaftar', 'id pendaftar', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');
	$this->form_validation->set_rules('lampiran', 'lampiran', 'trim|required');
	$this->form_validation->set_rules('tanggal_masuk', 'tanggal masuk', 'trim|required');

	$this->form_validation->set_rules('id_loker_masuk', 'id_loker_masuk', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function send_mail($email,$judul)
    {
        $this->load->library('email');
                $config['protocol'] = "smtp";
                $config['smtp_host'] = 'ssl://smtp.gmail.com';
                $config['smtp_port'] = 465; 
                $config['smtp_user'] = 'pondoktegalku@gmail.com';
                // $config['smtp_from_name'] = 'Admin';
                $config['smtp_pass'] = 'Sandi123';
                $config['wordwrap'] = TRUE;
                $config['newline'] = "\r\n";
                $config['mailtype'] = 'html';
                $this->email->initialize($config);

                $this->email->from('noreply', 'Loker Tegal');
                $list = array('sandytesar@gmail.com');
                $this->email->to($list);
                $this->email->subject('Info Loker Tegal');
                $this->email->message('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns:th="http://www.thymeleaf.org" xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <title>VERIFIKASI AKUN</title>

                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

                        <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css"/>
                        <style>
                            body {
                                font-family: "Roboto", sans-serif;
                                font-size: 48px;
                            }
                        </style>
                    </head>
                    <body style="margin: 0; padding: 0;">

                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                            <tr>
                                <td align="center" bgcolor="#4485f5" style="padding: 40px 0 30px 0;">
                                    <h2 style="color : #fff">Loker Tegal</h2>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" bgcolor="#eaeaea" style="padding: 40px 30px 40px 30px;">
                                    <h4>SELAMAT</h4>
                                    <h1>Permohonan Lamaran anda pada '.$judul.'</h1><br>
                                    <h4>Utuk tahap selanjutnya anda akan dihubungi oleh perusahaan terkait</h4>
                                    <h4>Terimakasih</h4>
                                </td>
                            </tr>
                        </table>

                    </body>
                    </html>');
    }

}

/* End of file C_loker_masuk.php */
/* Location: ./application/controllers/C_loker_masuk.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-08-31 08:42:39 */
/* http://harviacode.com */