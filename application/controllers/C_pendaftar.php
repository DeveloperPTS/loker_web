<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_pendaftar extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('M_pendaftar','M_tgl_indo'));
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['c_pendaftar_data'] = $this->M_pendaftar->get_all();
        $this->template->load('template_back','c_pendaftar/tb_pendaftar_list', $data);
    }

    public function read($id) 
    {
        $row = $this->M_pendaftar->get_by_id($id);
        if ($row) {
            $data = array(
            'id_pendaftar' => $row->id_pendaftar,
            'nama' => $row->nama,
            'username' => $row->username,
            'password' => $row->password,
            'email' => $row->email,
            'no_telp' => $row->no_telp,
            'alamat' => $row->alamat,
            'tempat_lhr' => $row->tempat_lhr,
            'tanggal_lhr' => $row->tanggal_lhr,
            'pendidikan' => $row->pendidikan,
            'jurusan' => $row->jurusan,
            'foto_ktp' => $row->foto_ktp,
            'foto_profil' => $row->foto_profil,
            'c_date' => $row->c_date,
            'm_date' => $row->m_date,
            'status_aktif' => $row->status_aktif,
            );
            $this->template->load('template_back','c_pendaftar/tb_pendaftar_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('c_pendaftar'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('c_pendaftar/create_action'),
	    'id_pendaftar' => set_value('id_pendaftar'),
	    'nama' => set_value('nama'),
	    'username' => set_value('username'),
	    'password' => set_value('password'),
	    'email' => set_value('email'),
	    'no_telp' => set_value('no_telp'),
	    'alamat' => set_value('alamat'),
	    'tempat_lhr' => set_value('tempat_lhr'),
	    'tanggal_lhr' => set_value('tanggal_lhr'),
	    'pendidikan' => set_value('pendidikan'),
	    'jurusan' => set_value('jurusan'),
	    'foto_ktp' => set_value('foto_ktp'),
	    'foto_profil' => set_value('foto_profil'),
	    'c_date' => set_value('c_date'),
	    'm_date' => set_value('m_date'),
	    'status_aktif' => set_value('status_aktif'),
	);
        $this->template->load('template_back','c_pendaftar/tb_pendaftar_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
		'username' => $this->input->post('username',TRUE),
		'password' => md5($this->password_random()),
		'email' => $this->input->post('email',TRUE),
		'no_telp' => $this->input->post('no_telp',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'tempat_lhr' => $this->input->post('tempat_lhr',TRUE),
		'tanggal_lhr' => $this->input->post('tanggal_lhr',TRUE),
		'pendidikan' => $this->input->post('pendidikan',TRUE),
		'jurusan' => $this->input->post('jurusan',TRUE),
		'status_aktif' => "1",
	    );

            $this->M_pendaftar->insert($data);
            $id_tersimpan = $this->db->insert_id();

            $ci = get_instance();
            $ci->load->library('email');
            $ci = get_instance();
            $ci->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = "tloker137@gmail.com";
            $config['smtp_pass'] = "jokerboy137";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";
            
            $ci->email->initialize($config);

            $ci->email->from('noreply', 'Kata Sandi Akun Loker Tegal');
            $list = array($this->input->post('email'));
            $ci->email->to($list);
            $ci->email->subject('Kata Sandi Akun Loker Tegal');
            $ci->email->message('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns:th="http://www.thymeleaf.org" xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <title>VERIFIKASI AKUN</title>

                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

                    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css"/>
                    <style>
                        body {
                            font-family: "Roboto", sans-serif;
                            font-size: 48px;
                        }
                    </style>
                </head>
                <body style="margin: 0; padding: 0;">

                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                        <tr>
                            <td align="center" bgcolor="#4485f5" style="padding: 40px 0 30px 0;">
                                <h2 style="color : #fff">Loker Tegal</h2>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#eaeaea" style="padding: 40px 30px 40px 30px;">
                                <h4>Akun anda berhasil disimpan, gunakan password</h4>
                                <h1>'.$pass.'</h1>
                                <h4>Silahkan login menggunakan nama pengguna anda dengan kata sandi di atas</h4>
                                <h4>Terimakasih</h4>
                            </td>
                        </tr>
                    </table>

                </body>
                </html>');

            if ($this->email->send()) {
                $this->session->set_flashdata('message', '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Data disimpan</div>');
                redirect(site_url('c_pendaftar'));     
            }else{
                $this->db->where('id_admin_loker', $id_tersimpan)->delete('tb_admin_loker');
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Data gagal disimpan</div>');
                redirect(site_url('c_pendaftar'));
            }           
            
        }
    }
    
    public function update($id) 
    {
        $row = $this->M_pendaftar->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Perbarui',
                'action' => site_url('c_pendaftar/update_action'),
		'id_pendaftar' => set_value('id_pendaftar', $row->id_pendaftar),
		'nama' => set_value('nama', $row->nama),
		'username' => set_value('username', $row->username),
		'password' => set_value('password', $row->password),
		'email' => set_value('email', $row->email),
		'no_telp' => set_value('no_telp', $row->no_telp),
		'alamat' => set_value('alamat', $row->alamat),
		'tempat_lhr' => set_value('tempat_lhr', $row->tempat_lhr),
		'tanggal_lhr' => set_value('tanggal_lhr', $row->tanggal_lhr),
		'pendidikan' => set_value('pendidikan', $row->pendidikan),
		'jurusan' => set_value('jurusan', $row->jurusan),
		'foto_ktp' => set_value('foto_ktp', $row->foto_ktp),
		'foto_profil' => set_value('foto_profil', $row->foto_profil),
		'c_date' => set_value('c_date', $row->c_date),
		'm_date' => set_value('m_date', $row->m_date),
		'status_aktif' => set_value('status_aktif', $row->status_aktif),
	    );
            $this->template->load('template_back','c_pendaftar/tb_pendaftar_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('c_pendaftar'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_pendaftar', TRUE));
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
		'username' => $this->input->post('username',TRUE),
		'email' => $this->input->post('email',TRUE),
		'no_telp' => $this->input->post('no_telp',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'tempat_lhr' => $this->input->post('tempat_lhr',TRUE),
		'tanggal_lhr' => $this->input->post('tanggal_lhr',TRUE),
		'pendidikan' => $this->input->post('pendidikan',TRUE),
		'jurusan' => $this->input->post('jurusan',TRUE),
		'foto_ktp' => $this->input->post('foto_ktp',TRUE),
		'foto_profil' => $this->input->post('foto_profil',TRUE),
	    );

            $this->M_pendaftar->update($this->input->post('id_pendaftar', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('c_pendaftar'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->M_pendaftar->get_by_id($id);

        if ($row) {
            $this->M_pendaftar->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('c_pendaftar'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('c_pendaftar'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('username', 'username', 'trim|required');/*
	$this->form_validation->set_rules('password', 'password', 'trim|required');*/
	$this->form_validation->set_rules('email', 'email', 'trim|required');
	$this->form_validation->set_rules('no_telp', 'no telp', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
	$this->form_validation->set_rules('tempat_lhr', 'tempat lhr', 'trim|required');
	$this->form_validation->set_rules('tanggal_lhr', 'tanggal lhr', 'trim|required');
	$this->form_validation->set_rules('pendidikan', 'pendidikan', 'trim|required');
	$this->form_validation->set_rules('jurusan', 'jurusan', 'trim|required');
	/*$this->form_validation->set_rules('foto_ktp', 'foto ktp', 'trim|required');
	$this->form_validation->set_rules('foto_profil', 'foto profil', 'trim|required');
	$this->form_validation->set_rules('c_date', 'c date', 'trim|required');
	$this->form_validation->set_rules('m_date', 'm date', 'trim|required');
	$this->form_validation->set_rules('status_aktif', 'status aktif', 'trim|required');*/

	$this->form_validation->set_rules('id_pendaftar', 'id_pendaftar', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function password_random(){

        $string = "abcdefghijklmnopqrstuvwxyz0123456789";
        for($i=0; $i<=8; $i++){
            $pos = rand(0,36);
            $str .= $string{$pos};
        }
        return $str;
    }
    public function cetak_laporan()
    {
        $data['c_pendaftar'] = $this->db->select('*')
        ->get('tb_pendaftar')->result();
        $this->load->view('c_pendaftar/cetak_laporan', $data);
        $html = $this->output->get_output();

        $this->dompdf->set_paper('legal', 'landscape');

        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("Laporan.pdf", array('Attachment'=>0));
    }

}

/* End of file C_pendaftar.php */
/* Location: ./application/controllers/C_pendaftar.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-08-31 08:42:56 */
/* http://harviacode.com */