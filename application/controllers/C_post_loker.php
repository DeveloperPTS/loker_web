<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_post_loker extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_post_loker');
        $this->load->library('form_validation');
    }

    public function index()
    {
        if ($this->session->userdata('usertipe') == 'admin') {
            $data['c_post_loker_data'] = $this->db->query("SELECT * FROM tb_post_loker WHERE tanggal_berlaku >= CURDATE() and id_admin_loker='".$this->session->userdata('userdata')->id_admin_loker."'")->result();
        }else{
            $data['c_post_loker_data'] = $this->db->query("SELECT * FROM tb_post_loker WHERE tanggal_berlaku >= CURDATE()")->result();
        }
        
        $this->template->load('template_back','c_post_loker/tb_post_loker_list', $data);
    }

    public function tutup()
    {
        if ($this->session->userdata('usertipe') == 'admin') {
             $data['c_post_loker_data'] = $this->db->query("SELECT * FROM tb_post_loker WHERE tanggal_berlaku < CURDATE() and id_admin_loker='".$this->session->userdata('userdata')->id_admin_loker."'")->result();
        }else{
             $data['c_post_loker_data'] = $this->db->query("SELECT * FROM tb_post_loker WHERE tanggal_berlaku < CURDATE()")->result();
        }
        $this->template->load('template_back','c_post_loker_lama/tb_post_loker_list', $data);
    }

    public function cetak_laporan()
    {
        $date = $this->input->post('bulan');
        $now = date('Y-m-d');
        $data['c_post_loker_data'] = $this->db->select('*')
        ->join('tb_admin_loker', 'tb_post_loker.id_admin_loker=tb_admin_loker.id_admin_loker')
        ->where('MONTH(tanggal_buat)', $date)
        // ->where('tanggal_berlaku <=',  $now)
        ->get('tb_post_loker')->result();
        $this->load->view('c_post_loker_lama/cetak_laporan', $data);
       $html = $this->output->get_output();

        $this->dompdf->set_paper('legal', 'landscape');

        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("Laporan_loker_tutup.pdf", array('Attachment'=>0));
    
    }
     public function cetak_laporan_aktif()
    {
        
        $now = date('Y-m-d');
        $data['c_post_loker_data'] = $this->db->select('*')
        ->join('tb_admin_loker', 'tb_post_loker.id_admin_loker=tb_admin_loker.id_admin_loker')
        ->where('tanggal_berlaku >=' , $now)
        // ->where('tanggal_berlaku ' >= $now)
        ->get('tb_post_loker')->result();
        $this->load->view('c_post_loker/laporan_aktif', $data);
       $html = $this->output->get_output();

        $this->dompdf->set_paper('legal', 'landscape');

        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("Laporan_loker_aktif.pdf", array('Attachment'=>0));
    
    }

    public function read($id) 
    {
        $row = $this->M_post_loker->get_by_id($id);
        if ($row) {
            $data = array(
            'id_post_loker' => $row->id_post_loker,
            'id_admin_loker' => $row->id_admin_loker,
            'jenjang' => $row->jenjang,
            'judul' => $row->judul,
            'deskripsi' => $row->deskripsi,
            'tanggal_berlaku' => $row->tanggal_berlaku,
            'tanggal_buat' => $row->tanggal_buat,
            'foto_loker' => $row->foto_loker,
            );
            $this->template->load('template_back','c_post_loker/tb_post_loker_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('c_post_loker'));
        }
    }

    public function readtutup($id) 
    {
        $row = $this->M_post_loker->get_by_id($id);
        if ($row) {
            $data = array(
            'id_post_loker' => $row->id_post_loker,
            'id_admin_loker' => $row->id_admin_loker,
            'jenjang' => $row->jenjang,
            'judul' => $row->judul,
            'deskripsi' => $row->deskripsi,
            'tanggal_berlaku' => $row->tanggal_berlaku,
            'tanggal_buat' => $row->tanggal_buat,
            'foto_loker' => $row->foto_loker,
            );
            $this->template->load('template_back','c_post_loker_lama/tb_post_loker_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('c_post_loker'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('c_post_loker/create_action'),
	    'id_post_loker' => set_value('id_post_loker'),
	    'id_admin_loker' => set_value('id_admin_loker'),
	    'judul' => set_value('judul'),
	    'deskripsi' => set_value('deskripsi'),
	    'tanggal_berlaku' => set_value('tanggal_berlaku'),
	    'foto_loker' => set_value('foto_loker'),
	);
        $this->template->load('template_back','c_post_loker/tb_post_loker_form', $data);
    }
    
    public function create_action() 
    {
        $now = date('Y-m-d');
     $config['upload_path']   = './upload/';
     $config['allowed_types'] = 'gif|jpg|png|jpeg';
     $config['max_size']      = 2500;
     $config['file_name']     = round(microtime(true) * 1000);

     $this->upload->initialize($config);
     $this->load->library('upload', $config);
     if ($this->upload->do_upload('foto_loker', $config))
     {
        $foto = $this->upload->data();
        $data = array(
            'id_admin_loker' => $this->input->post('id_admin_loker',TRUE),
            'judul' => $this->input->post('judul',TRUE),
            'jenjang' => $this->input->post('jenjang'),
            'deskripsi' => $this->input->post('deskripsi',TRUE),
            'tanggal_berlaku' => $this->input->post('tanggal_berlaku',TRUE),
            'tanggal_buat' => $now,
            'foto_loker' => $foto['file_name'],
            );

        $this->M_post_loker->insert($data);
        $this->session->set_flashdata('message', 'Create Record Success');
        redirect(site_url('c_post_loker'));
    }else{
        $this->session->set_flashdata('message', $this->upload->display_errors());
        redirect(site_url('c_post_loker'));
    }
    }

    public function update($id) 
    {
        $row = $this->M_post_loker->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('c_post_loker/update_action'),
		'id_post_loker' => set_value('id_post_loker', $row->id_post_loker),
		'id_admin_loker' => set_value('id_admin_loker', $row->id_admin_loker),
		'judul' => set_value('judul', $row->judul),
        'jenis' =>$row->jenis_pekerjaan,
		'deskripsi' => set_value('deskripsi', $row->deskripsi),
		'tanggal_berlaku' => set_value('tanggal_berlaku', $row->tanggal_berlaku),
		'foto_loker' =>  $row->foto_loker,
	    );
            $this->template->load('template_back','c_post_loker/tb_post_loker_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('c_post_loker'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_post_loker', TRUE));
        } else {
            // $config = array(
            //     'upload_path' => './upload/',
            //     'allowed_types' => 'gif|jpg|png|jpeg',
            //     'max_size' => '2500',
            //     // 'max_width' => '2000',
            //     // 'max_height' => '2000',
            //     'file_name' => round(microtime(true) * 1000),
            // );
            // $this->load->library('upload', $config);
            $config['upload_path']   = './upload/';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['max_size']      = 2500;
                    $config['file_name']     = round(microtime(true) * 1000);

                    $this->upload->initialize($config);
                    $this->load->library('upload', $config);
            if(!empty($_FILES['foto_loker'])){
                if (!$this->upload->do_upload('foto_loker')) {
                    $this->session->set_flashdata('error',  $this->upload->display_errors() );
                    redirect(site_url('c_post_loker'));
                } else {
                    $row = $this->M_post_loker->get_by_id($this->input->post('id_post_loker'));
                    unlink('./upload/'.$row->foto_loker);

                    // $config['upload_path']   = './upload/';
                    // $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    // $config['max_size']      = 2500;
                    // $config['file_name']     = round(microtime(true) * 1000);

                    // $this->upload->initialize($config);
                    // $this->load->library('upload', $config);
                    $file = $this->upload->data();
                    
                    $data = array(
                        'id_admin_loker' => $this->input->post('id_admin_loker',TRUE),
                        'judul' => $this->input->post('judul',TRUE),
                        'id_jenis' => $this->input->post('jenis',TRUE),
                        'deskripsi' => $this->input->post('deskripsi',TRUE),
                        'tanggal_berlaku' => $this->input->post('tanggal_berlaku',TRUE),
                        'foto_loker' => $file['file_name'],
                    );
                    $this->M_post_loker->update($this->input->post('id_post_loker', TRUE), $data);
                    $this->session->set_flashdata('message', 'Update Record Success');
                    redirect(site_url('c_post_loker'));
                }
            } else {
                $data = array(
                        'id_admin_loker' => $this->input->post('id_admin_loker',TRUE),
                        'judul' => $this->input->post('judul',TRUE),
                        'id_jenis' => $this->input->post('jenis',TRUE),
                        'deskripsi' => $this->input->post('deskripsi',TRUE),
                        'tanggal_berlaku' => $this->input->post('tanggal_berlaku',TRUE),
                        // 'foto_loker' => $file['file_name'],
                ); 
                $this->M_post_loker->update($this->input->post('id_post_loker', TRUE), $data);
                $this->session->set_flashdata('message', 'Update Record Success');
                redirect(site_url('c_post_loker'));
            }
        }   
    }
    
    public function delete($id) 
    {
        $row = $this->M_post_loker->get_by_id($id);

        if ($row) {
            $this->M_post_loker->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('c_post_loker'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('c_post_loker'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_admin_loker', 'id admin loker', 'trim|required');
	$this->form_validation->set_rules('judul', 'judul', 'trim|required');
	$this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');
	$this->form_validation->set_rules('tanggal_berlaku', 'tanggal berlaku', 'trim|required');
	if (empty($_FILES['foto_loker']['name']))
{
    $this->form_validation->set_rules('foto_loker', 'Document', 'required');
}

	$this->form_validation->set_rules('id_post_loker', 'id_post_loker', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
    

}

/* End of file C_post_loker.php */
/* Location: ./application/controllers/C_post_loker.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-08-31 08:43:12 */
/* http://harviacode.com */