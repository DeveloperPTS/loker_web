<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Web extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_loker');
        $this->load->library('form_validation');
    }

    function index()
    {
        $data['loker'] = $this->M_loker->get_loker_aktif();
        // $data['recent_post'] = $this->M_loker->get_loker();
        $data['post'] = 'frontend/post_loker';
        $data['right_bar'] = 'frontend/recent_post';
        $limit_per_page = 5;
        $start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $total_records = $this->M_loker->get_loker_aktif();
 
        if ($total_records > 0) 
        {
            // get current page records
            $data["results"] = $this->M_loker->get_current_page_records_loker($limit_per_page, $start_index);
             
            $config['base_url'] = base_url() . 'web/index';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;
             
            $this->pagination->initialize($config);
             
            // build paging links
            $data["links"] = $this->pagination->create_links();
        }
    	$this->load->view('template_front', $data);
    }
    function detail($id)
    {
        $data['isi'] = $this->M_loker->get_lamaran($id)->row();
        // $data['login'] = $this->session->userdata('userdata')->id_pendaftar;
        $data['post'] = 'frontend/detail';
        $data['right_bar'] = 'frontend/recent_post';
        $this->load->view('template_front', $data);
    }
    function profile($id='')
    {
        $data['loker'] = $this->M_loker->get_post_by_admin($id)->result();
        // $data['login'] = $this->session->userdata('userdata')->id_pendaftar;
        $data['post'] = 'frontend/admin_profil';
        $limit_per_page = 4;
        $start_index = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $total_records = $this->M_loker->get_total_by_admin($id);
        $data['perusahaan'] = $this->session->userdata('userdata')->nama_perusahaan;
        $data['foto_perusahaan'] = $this->session->userdata('userdata')->foto_perusahaan;
 
        if ($total_records > 0) 
        {
            // get current page records
            $data["results"] = $this->M_loker->get_current_page_records($limit_per_page, $start_index,$id);
             
            $config['base_url'] = base_url() . 'web/profile/'.$id;
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 4;
             
            $this->pagination->initialize($config);
             
            // build paging links
            $data["links"] = $this->pagination->create_links();
        }
        

        $this->load->view('template_front', $data);
    }

    function lamar($id)
    {
        $id_pendaftar = $this->session->userdata('userdata')->id_pendaftar;
        if (empty($id_pendaftar)) {
            redirect('admin');
        }
        $data['id_post_loker'] = $id;
        $data['post'] = 'frontend/post_lamaran';
        $data['id_pendaftar'] = $this->session->userdata('userdata')->id_pendaftar;
        
        $this->load->view('template_front', $data);
    }
    function proses_lamar()
    {
        $data['id_post_loker'] = $this->input->post('id_post_loker');
        $data['post'] = 'frontend/post_lamaran';
        $data['id_pendaftar'] = $this->session->userdata('userdata')->id_pendaftar;
        
        $config['upload_path']   = './lamaran/';
         $config['allowed_types'] = 'zip|rar';
         $config['max_size']      = 2500;
         $config['file_name']     = $this->input->post('id_post_loker').'_'.$this->session->userdata('userdata')->id_pendaftar;


            

     $this->upload->initialize($config);
     $this->load->library('upload', $config);
 
        $this->load->library('upload', $config);
 
        if ( ! $this->upload->do_upload('berkas')){
            $data['error'] =  $this->upload->display_errors();
            $this->load->view('template_front', $data);
        }else{
            $file = $this->upload->data();
            $date = date('Y-m-d H:i:s' );
            $data = array('upload_data' => $this->upload->data());
            $input = array('id_post_loker' => $this->input->post('id_post_loker'),
                'id_pendaftar' => $this->input->post('id_pendaftar'),
                'lampiran' => $file['file_name'],
                'tanggal_masuk' => $date );
            $this->M_loker->lamar_kerja($input);
            $this->session->set_flashdata('lamaran', 'Lamaran terkirim');

            redirect('web');
        }

    }
    public function filter($id_jenis)
    {
        $limit_per_page = 5;
        $start_index = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $total_records = $this->M_loker->get_loker_filter($id_jenis);
 
        if ($total_records > 0) 
        {
            // get current page records
            $data["results"] = $this->M_loker->get_current_page_filter($limit_per_page, $start_index,$id_jenis);
             
            $config['base_url'] = base_url() . 'web/index';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 4;
             
            $this->pagination->initialize($config);
             
            // build paging links
            $data["links"] = $this->pagination->create_links();
        }
        else{
            $data['notfound'] = true;
        }
        $this->load->view('frontend/post_filter', $data);
        // $loker = $this->M_loker->filter_data($jenjang)->result();
        // $data = array('result' => $loker , );
    }

    public function contac_us()
    {
        $data['post'] = 'frontend/contac_us';
        $data['right_bar'] = 'frontend/recent_post';
        $this->load->view('template_front', $data);
    }

      public function halaman_baru()
    {
        $data['post'] = 'frontend/halaman_baru';
        $data['right_bar'] = 'frontend/recent_post';
        $data['pesan'] = $this->db->order_by('date_time', 'desc')->get('tb_pesan');
        $this->load->view('template_front', $data);
    }
    public function send_pesan()
    {
        $this->form_validation->set_rules('nama', 'Nama Lengkap', 'required|callback_alpha_dash_space',array('required'=>'Form Ini Harus Diisi','alpha'=>'Masukan Nama dengan benar'));
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email',array('required'=>'Form Ini Harus Diisi','valid_email'=>'Masukan Email dengan benar'));
        $this->form_validation->set_rules('pesan', 'Pesan', 'required|min_length[10]',array('required'=>'Form Ini Harus Diisi','min_length' => 'Masukan Minimal 10 Charakter'));

        if ($this->form_validation->run() == FALSE)
        {
             $this->halaman_baru();
        }else {
            $save = array(
                'nama' => $this->input->post('nama'),
                'email' => $this->input->post('email'),
                'pesan' => $this->input->post('pesan'),
                );
            $this->db->insert('tb_pesan',$save);
            $this->session->set_flashdata('lamaran', 'Terimakasih Telah mengisi buku tamu');
            redirect('web/halaman_baru','refresh');
        }
        
    }
    function alpha_dash_space($fullname){
        if (! preg_match('/^[a-zA-Z\s]+$/', $fullname)) {
            $this->form_validation->set_message('alpha_dash_space', 'Masukan Nama Lengkap dengan benar');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function lamaran_saya()
    {
        $id = $this->session->userdata('userdata')->id_pendaftar;
        $data['lamaran'] = $this->get_by_id($id);
        // $data['lamaran'] = $this->db->get_where('tb_loker_masuk' , array('id_pendaftar' => $id, ))->result();
        $data['post'] = 'frontend/histori_lamaran';
        // $data['right_bar'] = 'frontend/recent_post';
        $this->load->view('template_front', $data);
    }
    function get_by_id($id)
    {
        $this->db->select('tb_pendaftar.*, tb_post_loker.*, tb_loker_masuk.*');
        $this->db->join('tb_post_loker', 'tb_post_loker.id_post_loker=tb_loker_masuk.id_post_loker');
        $this->db->join('tb_pendaftar', 'tb_pendaftar.id_pendaftar=tb_loker_masuk.id_pendaftar');
        $this->db->where('tb_loker_masuk.id_pendaftar', $id);
        return $this->db->get('tb_loker_masuk')->result();
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url());
    }
}