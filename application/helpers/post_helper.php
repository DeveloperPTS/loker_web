<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('recent_post'))
    {
        function recent_post()
        {
        	$CI = get_instance();
        	$CI->load->model('M_loker');
        	$recent_post = $CI->M_loker->get_loker();
            return $recent_post;
        }
    }