<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model {

	public function login($where) {
		$this->db->where('user_tipe','admin');
		return $this->db->get_where("users_admin", $where);
	}

	public function superadmin($where) {
		return $this->db->get_where("tb_admin", $where);
	}

	public function admin($where) {
		return $this->db->get_where("tb_admin_loker", $where);
	}

	public function admin_daftar($where) {
		return $this->db->get_where("tb_admin_loker", $where);
	}

	public function user($where) {
		$this->db->where('status_aktif','1');
		return $this->db->get_where("tb_pendaftar", $where);
	}

	public function reg_user($data)
	{
		$this->db->insert('tb_pendaftar', $data);
	}

}
