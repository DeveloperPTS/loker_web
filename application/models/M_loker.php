<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_loker extends CI_Model {

	function get_loker_aktif() {
		$now = date('Y-m-d');
		$this->db->select('tb_admin_loker.*, tb_post_loker.*');
		$this->db->join('tb_admin_loker','tb_post_loker.id_admin_loker=tb_admin_loker.id_admin_loker');
		$this->db->order_by('id_post_loker', 'ASC');
		$this->db->where('tanggal_berlaku >=', $now);
		return $this->db->get('tb_post_loker')->num_rows();
	}
	function get_loker() {
		$now = date('Y-m-d');
		$this->db->select('tb_admin_loker.*, tb_post_loker.*');
		$this->db->join('tb_admin_loker','tb_post_loker.id_admin_loker=tb_admin_loker.id_admin_loker');
		$this->db->order_by('id_post_loker', 'ASC');
		$this->db->where('tanggal_berlaku >=', $now);
		return $this->db->get('tb_post_loker')->result();
	}
	public function get_total_loker_aktif() 
    {
		$now = date('Y-m-d');
    	$this->db->select('tb_admin_loker.*, tb_post_loker.*');
		$this->db->join('tb_admin_loker','tb_post_loker.id_admin_loker=tb_admin_loker.id_admin_loker');
		$this->db->order_by('id_post_loker', 'DESC');
		$this->db->where('tanggal_berlaku >=', $now);
        return $this->db->count_all("tb_post_loker");
    }
	public function get_current_page_records_loker($limit, $start) 
    {
    	$now = date('Y-m-d');
		$this->db->select('tb_admin_loker.*, tb_post_loker.*');
		$this->db->join('tb_admin_loker','tb_post_loker.id_admin_loker=tb_admin_loker.id_admin_loker');
		$this->db->order_by('id_post_loker', 'DESC');
		$this->db->where('tanggal_berlaku >=', $now);
        $this->db->limit($limit, $start);
        $query = $this->db->get("tb_post_loker");
 
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
             
            return $data;
        }
 
        return false;
    }
	function get_lamaran($id) {
		$this->db->join('tb_admin_loker', 'tb_admin_loker.id_admin_loker=tb_post_loker.id_admin_loker');
		$this->db->where('id_post_loker', $id);
		return $this->db->get('tb_post_loker');
	}
	function get_post_by_admin($id) {
		$this->db->join('tb_admin_loker', 'tb_admin_loker.id_admin_loker=tb_post_loker.id_admin_loker');
		$this->db->where('tb_admin_loker.id_admin_loker', $id);
		return $this->db->get('tb_post_loker');
	}
	public function get_total_by_admin($id) 
    {
    	$this->db->join('tb_admin_loker', 'tb_admin_loker.id_admin_loker=tb_post_loker.id_admin_loker');
		$this->db->where('tb_admin_loker.id_admin_loker', $id);
        return $this->db->count_all("tb_post_loker");
    }
    public function get_current_page_records($limit, $start,$id) 
    {
    	$this->db->join('tb_admin_loker', 'tb_admin_loker.id_admin_loker=tb_post_loker.id_admin_loker');
    	$this->db->order_by('tanggal_berlaku', 'DESC');
		$this->db->where('tb_admin_loker.id_admin_loker', $id);
        $this->db->limit($limit, $start);
        $query = $this->db->get("tb_post_loker");
 
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
             
            return $data;
        }
 
        return false;
    }
	function lamar_kerja($input)
	{
		$this->db->insert('tb_loker_masuk', $input);
	}

	function filter_data($id)
	{
		$this->db->where('id_jenis_pekerjaan', $id);
		return $this->db->get('tb_jenis_pekerjaan');
	}
	function get_loker_filter($id_jenis) {
		$now = date('Y-m-d');
		$this->db->select('tb_admin_loker.*, tb_post_loker.*');
		$this->db->join('tb_admin_loker','tb_post_loker.id_admin_loker=tb_admin_loker.id_admin_loker');
		$this->db->join('tb_jenis_pekerjaan','tb_jenis_pekerjaan.id_jenis_pekerjaan=tb_post_loker.id_jenis');
		$this->db->where('tanggal_berlaku >=', $now);
		$this->db->where('tb_jenis_pekerjaan.id_jenis_pekerjaan', $id_jenis);
		return $this->db->get('tb_post_loker')->num_rows();
	}

	public function get_current_page_filter($limit, $start,$id_jenis) 
    {
		$now = date('Y-m-d');
    	$this->db->join('tb_admin_loker', 'tb_admin_loker.id_admin_loker=tb_post_loker.id_admin_loker');
		$this->db->join('tb_jenis_pekerjaan','tb_jenis_pekerjaan.id_jenis_pekerjaan=tb_post_loker.id_jenis');
    	$this->db->order_by('tanggal_berlaku', 'DESC');
		$this->db->where('tb_post_loker.tanggal_berlaku >=', $now);
		$this->db->where('tb_post_loker.id_jenis', $id_jenis);
        $this->db->limit($limit, $start);
        $query = $this->db->get("tb_post_loker");
 
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
             
            return $data;
        }
 
        return false;
    }
}