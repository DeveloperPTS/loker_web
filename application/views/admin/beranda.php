<div id="content">
      <div class="panel">
        <div class="panel-body">
          <div class="col-lg-12">
            <h3 class="animated fadeInLeft">Halaman Awal</h3>
          </div>
        </div>                    
      </div>
      <div class="col-lg-12">
        <div class="panel box-v1">
         <div class="panel-body">
           <div class="col-sm-2 text-center">
            <img class="img-circle" src="<?= base_url(); ?>asset/img/avatar.jpg" style="width: 100px;height:100px;">
          </div>
          <div class="col-sm-10">
            <p>SELAMAT DATANG <?= ucwords($this->session->userdata('userdata')->username); ?></p>
            <p>Sistem Admin Loker Tegal digunakan untuk mengelola informasi dan lainnya.<br>Silahkan gunakan dengan bijak</p>
            <small>Terimakasih</small>
          </div>
        </div>
      </div>
    </div>  
  </div>
