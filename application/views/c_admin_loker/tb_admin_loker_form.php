<div id="content">
    <div class="panel">
        <div class="panel-body">
            <div class="col-lg-12">
                <h3 class="animated fadeInLeft"><?php echo $button ?> Data Admin Loker</h3>
            </div>
        </div>                    
    </div>
    <div class="col-lg-12">
        <div class="panel box-v1">
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post">
                   <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="varchar">Nama Perusahaan <?php echo form_error('nama_perusahaan') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="nama_perusahaan" id="nama_perusahaan" placeholder="Nama Perusahaan" value="<?php echo $nama_perusahaan; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="varchar">No Siup <?php echo form_error('no_siup') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="no_siup" id="no_siup" placeholder="No Siup" value="<?php echo $no_siup; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="varchar">Username <?php echo form_error('username') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="varchar">No Telp <?php echo form_error('no_telp') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="No Telp" value="<?php echo $no_telp; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="varchar">Email <?php echo form_error('email') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="varchar">Alamat <?php echo form_error('alamat') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat" value="<?php echo $alamat; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="varchar">Ktp Admin <?php echo form_error('ktp_admin') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="file" class="form-control" name="ktp_admin" id="ktp_admin" placeholder="Ktp Admin" value="<?php echo $ktp_admin; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="varchar">Foto Sk <?php echo form_error('foto_sk') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="file" class="form-control" name="foto_sk" id="foto_sk" placeholder="Foto Sk" value="<?php echo $foto_sk; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="varchar">Foto Perusahaan <?php echo form_error('foto_perusahaan') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="file" class="form-control" name="foto_perusahaan" id="foto_perusahaan" placeholder="Foto Perusahaan" value="<?php echo $foto_perusahaan; ?>" /></div>
                </div>
                <input type="hidden" name="id_admin_loker" value="<?php echo $id_admin_loker; ?>" /> 
                <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                <a href="<?php echo site_url('c_admin_loker') ?>" class="btn btn-default">Cancel</a>
            </form>
            </div>
        </div>
    </div>  
</div>
<script>
    
</script>