<div id="content">
	<div class="panel">
		<div class="panel-body">
			<div class="col-lg-12">
				<h3 class="animated fadeInLeft">Detail Admin Loker</h3>
				<?=  anchor(site_url('c_admin_loker/setujui/'.$id_admin_loker),'<button style="margin-top: 5px; width: 120px" class="btn btn-md btn-warning"><i class="fa fa-pencil"></i> Setujui</button>', 'onclick="javasciprt: return confirm(\'Apakah anda akan menyetujui admin loker ?\')"');  ?>
			</div>
		</div>                    
	</div>
	<div class="col-lg-12">
		<div class="panel box-v1">
			<div class="panel-body">
				<table class="table">
					<tr><td width="200px">Nama Perusahaan</td><td width="20px">:</td><td><?php echo $nama_perusahaan; ?></td></tr>
					<tr><td width="200px">No Siup</td><td width="20px">:</td><td><?php echo $no_siup; ?></td></tr>
					<tr><td width="200px">Username</td><td width="20px">:</td><td><?php echo $username; ?></td></tr>
					<tr><td width="200px">Password</td><td width="20px">:</td><td><?php echo $password; ?></td></tr>
					<tr><td width="200px">No Telp</td><td width="20px">:</td><td><?php echo $no_telp; ?></td></tr>
					<tr><td width="200px">Email</td><td width="20px">:</td><td><?php echo $email; ?></td></tr>
					<tr><td width="200px">Alamat</td><td width="20px">:</td><td><?php echo $alamat; ?></td></tr>
					<tr><td width="200px">Ktp Admin</td><td width="20px">:</td><td><a href="<?= base_url().'upload/'.$ktp_admin; ?>" target="_blank"><img style="width: 300px;" src="<?= base_url().'upload/'.$ktp_admin; ?>"></a></td></tr>
					<tr><td width="200px">Foto Sk</td><td width="20px">:</td><td><a href="<?= base_url().'upload/'.$foto_sk; ?>" target="_blank"><img style="width: 300px;" src="<?= base_url().'upload/'.$foto_sk; ?>"></a></td></tr>
					<tr><td width="200px">Foto Perusahaan</td><td width="20px">:</td><td><a href="<?= base_url().'upload/'.$foto_perusahaan; ?>" target="_blank"><img style="width: 300px;" src="<?= base_url().'upload/'.$foto_perusahaan; ?>"></a></td></tr>
					<tr><td width="200px"></td><td width="20px">:</td><td><a href="<?php echo site_url('c_admin_loker') ?>" class="btn btn-default">Cancel</a></td></tr>
				</table>
			</div>
		</div>
	</div>  
</div>

