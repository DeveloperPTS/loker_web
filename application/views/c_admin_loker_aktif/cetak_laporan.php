<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>laporan data admin</title>

<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
        font-size: 12px;
    }
    @page {
      margin-top: 75px;
      margin-left: 75px;
      margin-right: 75px;
      /*size: landscape*/
    }
    tr.noBorder td {
  border: 0px;
}
    table{
      border-collapse: collapse;
      width: 100%;
    }
    table th{
      border:1px solid #000;
      padding: 3px;
      font-weight: bold;
      text-align: left;
    }
    table td{
      border:1px solid #000;
      padding: 3px;
      vertical-align: top;
    }
</style>

</head>
<body>
  <div>
    <div align="center">
       <p>DATA ADMIN LOKER</p>
     </div>
  <table border="1" width="100%" >
    <thead align="center" valign="middle">
      <tr>
        <td rowspan=""> No</td>
        <td rowspan=""> Nama Perusahaan</td>
        <td colspan=""> Username</td>
        <td colspan=""> No Telpon</td>
        <td colspan=""> No SIUP</td>
        <td colspan=""> Email</td>
        <td colspan=""> Alamat</td>
      </tr>
    </thead>
    <?php $no=1; 
    foreach ($c_admin_loker as $key => $value) { ?>
    	<tr>
	    	<td><?php echo $no++ ?></td>
	    	<td><?php echo $value->nama_perusahaan ?></td>
	        <td><?php echo $value->username ?></td>
			<td><?php echo $value->no_telp ?></td>
			<td><?php echo $value->no_siup ?></td>
			<td><?php echo $value->email ?></td>
			<td><?php echo $value->alamat ?></td>
		</tr>
    <?php } ?>
    <tbody>
    </tbody>
  </table>
</div>

</body>
</html>