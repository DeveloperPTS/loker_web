<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1><?php echo $button ?> Data Admin Loker</h1>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="row">
        <div class="col-md-12">
          <div class="card">
              <div class="card-header">
                  <strong>Admin Loker</strong> Form
              </div>
              <div class="card-body card-block">
                <form action="<?php echo $action; ?>" method="post">
                   <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="varchar">Nama Perusahaan <?php echo form_error('nama_perusahaan') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="nama_perusahaan" id="nama_perusahaan" placeholder="Nama Perusahaan" value="<?php echo $nama_perusahaan; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="varchar">No Siup <?php echo form_error('no_siup') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="no_siup" id="no_siup" placeholder="No Siup" value="<?php echo $no_siup; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="varchar">Username <?php echo form_error('username') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="password">Password <?php echo form_error('password') ?></label></div>
                        <div class="col-12 col-md-9">
                    <textarea class="form-control" rows="3" name="password" id="password" placeholder="Password"><?php echo $password; ?></textarea></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="varchar">No Telp <?php echo form_error('no_telp') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="No Telp" value="<?php echo $no_telp; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="varchar">Email <?php echo form_error('email') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="varchar">Alamat <?php echo form_error('alamat') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat" value="<?php echo $alamat; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="varchar">Ktp Admin <?php echo form_error('ktp_admin') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="ktp_admin" id="ktp_admin" placeholder="Ktp Admin" value="<?php echo $ktp_admin; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="varchar">Foto Sk <?php echo form_error('foto_sk') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="foto_sk" id="foto_sk" placeholder="Foto Sk" value="<?php echo $foto_sk; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="varchar">Foto Perusahaan <?php echo form_error('foto_perusahaan') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="foto_perusahaan" id="foto_perusahaan" placeholder="Foto Perusahaan" value="<?php echo $foto_perusahaan; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="enum">Status Aktif <?php echo form_error('status_aktif') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="status_aktif" id="status_aktif" placeholder="Status Aktif" value="<?php echo $status_aktif; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="datetime">C Date <?php echo form_error('c_date') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="c_date" id="c_date" placeholder="C Date" value="<?php echo $c_date; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="datetime">M Date <?php echo form_error('m_date') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="m_date" id="m_date" placeholder="M Date" value="<?php echo $m_date; ?>" /></div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label"  for="enum">Deleted <?php echo form_error('deleted') ?></label></div>
                        <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="deleted" id="deleted" placeholder="Deleted" value="<?php echo $deleted; ?>" /></div>
                </div>
                <input type="hidden" name="id_admin_loker" value="<?php echo $id_admin_loker; ?>" /> 
                <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                <a href="<?php echo site_url('c_admin_loker') ?>" class="btn btn-default">Cancel</a>
            </form>
        </div>
    </div>
</div>
</div>
</div>