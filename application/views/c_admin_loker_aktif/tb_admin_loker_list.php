<div id="content">
  <div class="panel">
    <div class="panel-body">
      <div class="col-lg-12">
        <h3 class="animated fadeInLeft">Data Admin Loker Aktif</h3>
        <?php echo anchor(site_url('C_admin_loker/cetak_laporan'),'Cetak Laporan', 'class="btn btn-success btn-round pull-right animated fadeInLeft"'); ?>
    </div>
</div>                    
</div>
<div class="col-lg-12">
    <div class="panel box-v1">
     <div class="panel-body">
     <table id="datatable" class="table table-striped table-bordered" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Perusahaan</th>
                    <th>No Siup</th>
                    <th>Username</th>
                    <th>No Telp</th>
                    <th>Email</th>
                    <th>Alamat</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $start = 0;
                foreach ($c_admin_loker_data as $c_admin_loker)
                {
                    ?>
                    <tr>
                     <td width="80px"><?php echo ++$start ?></td>
                     <td><?php echo $c_admin_loker->nama_perusahaan ?></td>
                     <td><?php echo $c_admin_loker->no_siup ?></td>
                     <td><?php echo $c_admin_loker->username ?></td>
                     <td><?php echo $c_admin_loker->no_telp ?></td>
                     <td><?php echo $c_admin_loker->email ?></td>
                     <td><?php echo $c_admin_loker->alamat ?></td>
                     <td style="text-align:center" width="200px">
                        <?php 
                        echo anchor(site_url('c_admin_loker/detail/'.$c_admin_loker->id_admin_loker),'<button style="margin-top: 5px; width: 120px" class="btn btn-sm btn-success"><i class="fa fa-eye"></i> Detail</button>'); 
                        echo '<br>'; 
                        echo anchor(site_url('c_admin_loker/delete/'.$c_admin_loker->id_admin_loker),'<button style="margin-top: 5px; width: 120px" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</button>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>

</div>
</div>
</div>  
</div>
