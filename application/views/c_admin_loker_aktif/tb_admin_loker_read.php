<div id="content">
	<div class="panel">
		<div class="panel-body">
			<div class="col-lg-12">
				<h3 class="animated fadeInLeft">Detail Admin Loker <?php echo ucwords($nama_perusahaan); ?></h3>
			</div>
		</div>                    
	</div>
	<div class="col-lg-12">
		<div class="panel box-v1">
			<div class="panel-body">
				<table class="table">
					<tr><td>Nama Perusahaan</td><td><?php echo $nama_perusahaan; ?></td></tr>
					<tr><td>No Siup</td><td><?php echo $no_siup; ?></td></tr>
					<tr><td>Username</td><td><?php echo $username; ?></td></tr>
					<tr><td>Password</td><td><?php echo $password; ?></td></tr>
					<tr><td>No Telp</td><td><?php echo $no_telp; ?></td></tr>
					<tr><td>Email</td><td><?php echo $email; ?></td></tr>
					<tr><td>Alamat</td><td><?php echo $alamat; ?></td></tr>
					<tr><td>Ktp Admin</td><td><img src="<?php echo site_url('upload/'.$ktp_admin) ?>" class="img-thumbnail text-center" height="150" ></td></tr>
					<tr><td>Foto Sk</td><td> <img src="<?php echo site_url('upload/'.$foto_sk) ?>" class="img-thumbnail text-center" height="150" ></td></tr>
					<tr><td>Foto Perusahaan</td><td><img src="<?php echo site_url('upload/'.$foto_perusahaan) ?>" class="img-thumbnail text-center" height="150" ></td></tr>
					<tr><td></td><td><a href="<?php echo site_url('c_admin_loker/aktif') ?>" class="btn btn-default">Cancel</a></td></tr>
				</table>
			</div>
		</div>
	</div>  
</div>

