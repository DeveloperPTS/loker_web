<div id="content">
    <div class="panel">
        <div class="panel-body">
        <div class="col-lg-12">
            <h3 class="animated fadeInLeft">Data Pelamar Masuk </h3>
        </div>
        </div>                    
    </div>
    <div class="col-lg-12">
    <div class="panel box-v1">
        <div class="panel-body">
        <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
                <th>Id Post Loker</th>
                <th>Nama</th>
                <th>Status</th>
                <!-- <th>Lampiran</th> -->
                <th>Tanggal Masuk</th>
                <th>Action</th>
            </tr>
            <?php
            $start= 0;
            foreach ($data_loker_masuk as $c_loker_masuk)
            {
                ?>
            <tr>
                <td width="80px"><?php echo ++$start ?></td>
                <td><?php echo $c_loker_masuk->id_post_loker ?></td>
                <td><?php echo $c_loker_masuk->nama ?></td>
                <td><?php echo $c_loker_masuk->status ?></td>
                <!-- <td><?php echo $c_loker_masuk->lampiran ?></td> -->
                <td><?php echo $c_loker_masuk->tanggal_masuk ?></td>
                <td style="text-align:center" >
                    <a href="<?= base_url('c_loker_masuk/download/'.$c_loker_masuk->lampiran) ?>" class="btn btn-primary"> <i class="fa fa-download"></i></a>
                    <a href="#" onclick=" update('<?php echo $c_loker_masuk->id_loker_masuk ?>')" class="btn btn-primary"> Update</a>
                   <!--  <?php 
                    echo ' | '; 
                    echo anchor(site_url('c_loker_masuk/delete/'.$c_loker_masuk->id_loker_masuk),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                    ?> -->
                </td>
            </tr>
            <?php
            }
            ?>
        </table>
        </div>
    </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-center">Ubah Status</h3>
            </div>
            <div class="modal-body form">
                <form action="<?php echo base_url('c_loker_masuk/ubah_status')?>" id="form" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="id_loker_masuk" value=""/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama </label>
                            <div class="col-md-9">         
                                <input id="nama_daftar" class="form-control" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Status </label>
                            <div class="col-md-9">
                                <select class="form-control" name="status">
                                    <option value="">--Status--</option>
                                    <option value="terima">Terima</option>
                                    <option value="tidak">Tidak</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        
                            <button type="submit" id="btnSave"  class="btn btn-primary">Save</button>
                        
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    function update(id) {
        $('#form')[0].reset();
        $('#modal_form').modal('show');

         $.ajax({
            url : "<?php echo base_url('c_loker_masuk/get_ajax/')?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data){
                $('#nama_daftar').val(data.nama);
                $('[name="id_loker_masuk"]').val(data.id_loker_masuk);

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        // console.log(id);
        });
     }
</script>
