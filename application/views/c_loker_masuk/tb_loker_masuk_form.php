<div id="content">
    <div class="panel">
        <div class="panel-body">
        <div class="col-lg-12">
            <h3 class="animated fadeInLeft">Data Pelamar Masuk </h3>
        </div>
        </div>                    
    </div>
    <div class="col-lg-12">
    <div class="panel box-v1">
        <div class="panel-body">
        <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
        <form action="<?php echo $action; ?>" method="post">
        <div class="form-group">
            <label for="int">Id Post Loker <?php echo form_error('id_post_loker') ?></label>
            <input type="text" class="form-control" name="id_post_loker" id="id_post_loker" placeholder="Id Post Loker" value="<?php echo $id_post_loker; ?>" />
        </div>
        <div class="form-group">
            <label for="int">Id Pendaftar <?php echo form_error('id_pendaftar') ?></label>
            <input type="text" class="form-control" name="id_pendaftar" id="id_pendaftar" placeholder="Id Pendaftar" value="<?php echo $id_pendaftar; ?>" />
        </div>
        <div class="form-group">
            <label for="enum">Status <?php echo form_error('status') ?></label>
            <select class="form-control" name="status" id="status" placeholder="Status">
                <option> --Status-- </option>
                <option value="terima">Terima</option>
                <option value="tolak">Tolak</option>
            </select>
        </div>
        <div class="form-group">
            <label for="varchar">Lampiran <?php echo form_error('lampiran') ?></label>
            <input type="text" class="form-control" name="lampiran" id="lampiran" placeholder="Lampiran" value="<?php echo $lampiran; ?>" />
        </div>
        <div class="form-group">
            <label for="datetime">Tanggal Masuk <?php echo form_error('tanggal_masuk') ?></label>
            <input type="text" class="form-control" name="tanggal_masuk" id="tanggal_masuk" placeholder="Tanggal Masuk" value="<?php echo $tanggal_masuk; ?>" />
        </div>
        <input type="hidden" name="id_loker_masuk" value="<?php echo $id_loker_masuk; ?>" /> 
        <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
        <a href="<?php echo site_url('c_loker_masuk') ?>" class="btn btn-default">Cancel</a>
    </form>
        </div>
    </div>
    </div>
</div>       