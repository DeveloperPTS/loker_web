<div id="content">
  <div class="panel">
    <div class="panel-body">
      <div class="col-lg-12">
        <h3 class="animated fadeInLeft">Data Pelamar Masuk</h3>
    </div>
</div>                    
</div>
<div class="col-lg-12">
    <div class="panel box-v1">
       <div class="panel-body">
       <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
         <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
                <th>Nama Pelamar</th>
                <th>Status</th>
                <th>Lampiran</th>
                <th>Tanggal Masuk</th>
                <th>Action</th>
                    </tr><?php
                    $start= 0;
                    foreach ($c_post_loker_masuk as $c_loker_masuk)
                    {
                        ?>
                        <tr>
                    <td width="80px"><?php echo ++$start ?></td>
                    <td><?php echo $c_loker_masuk->nama ?></td>
                    <td><?php echo $c_loker_masuk->status ?></td>
                    <td><?php echo $c_loker_masuk->lampiran ?></td>
                    <td><?php echo $c_loker_masuk->tanggal_masuk ?></td>
                    <td style="text-align:center" width="200px">
                        <?php 
                        echo anchor(site_url('c_loker_masuk/read/'.$c_loker_masuk->id_loker_masuk),'<button style="margin-top: 5px; width: 120px" class="btn btn-xs btn-success"><i class="fa fa-eye"></i> Detail</button>'); 
                        echo anchor(site_url('c_loker_masuk/lihat_lamaran_masuk/'.$c_loker_masuk->id_post_loker),'<button style="margin-top: 5px; width: 120px" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i> Update</button>'); 
                        echo anchor(site_url('c_loker_masuk/delete/'.$c_loker_masuk->id_loker_masuk),'<button style="margin-top: 5px; width: 120px" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</button>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
</div>
</div>
</div>  
</div>
