<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>laporan data pelamar</title>

<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
        font-size: 12px;
    }
    @page {
      margin-top: 75px;
      margin-left: 75px;
      margin-right: 75px;
      /*size: landscape*/
    }
    tr.noBorder td {
  border: 0px;
}
    table{
      border-collapse: collapse;
      width: 100%;
    }
    table th{
      border:1px solid #000;
      padding: 3px;
      font-weight: bold;
      text-align: left;
    }
    table td{
      border:1px solid #000;
      padding: 3px;
      vertical-align: top;
    }
</style>

</head>
<body>
  <div>
    <div align="center">
       <p>DATA PELAMAR KERJA</p>
     </div>
  <table border="1" width="100%" >
    <thead align="center" valign="middle">
      <tr>
        <td rowspan=""> No</td>
        <td rowspan=""> Nama </td>
        <td colspan=""> No HP</td>
        <td colspan=""> Alamat</td>
        <td colspan=""> Tempat, Tanggal lahir</td>
        <td colspan=""> Pendidikan</td>
      </tr>
    </thead>
    <?php $no=1; 
    foreach ($c_pendaftar as $key => $value) { ?>
    	<tr>
	    	<td><?php echo $no++ ?></td>
	    	<td><?php echo $value->nama ?></td>
	        <td><?php echo $value->no_telp ?></td>
			<td><?php echo $value->alamat ?></td>
			<td><?php echo $value->tempat_lhr,$value->tanggal_lhr ?></td>
			<td><?php echo $value->pendidikan ?></td>
		</tr>
    <?php } ?>
    <tbody>
    </tbody>
  </table>
</div>

</body>
</html>