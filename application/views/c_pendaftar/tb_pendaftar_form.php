<div id="content">
  <div class="panel">
    <div class="panel-body">
      <div class="col-lg-12">
        <h3 class="animated fadeInLeft"><?php echo $button ?> Data Pencari Kerja</h3>
    </div>
</div>                    
</div>
<div class="col-lg-12">
    <div class="panel box-v1">
     <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post">
            <div class="col-md-6">
               <div class="row form-group">
                <div class="col col-md-3"><label class=" form-control-label" for="varchar">Nama</label></div>
                <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?php echo $nama; ?>" />
                    <?php echo form_error('nama') ?>
                </div>
            </div>
            <div class="row form-group">
                <div class="col col-md-3"><label class=" form-control-label" for="varchar">Username </label></div>
                <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" /><?php echo form_error('username') ?></div>
                </div>
                <!-- <div class="row form-group">
                    <div class="col col-md-3"><label class=" form-control-label" for="password">Password <?php echo form_error('password') ?></label></div>
                            <div class="col-12 col-md-9">
                    <textarea class="form-control" rows="3" name="password" id="password" placeholder="Password"><?php echo $password; ?></textarea></div>
                </div> -->
                <div class="row form-group">
                    <div class="col col-md-3"><label class=" form-control-label" for="varchar">Email </label></div>
                    <div class="col-12 col-md-9">
                        <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" /><?php echo form_error('email') ?></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label" for="varchar">No Telp </label></div>
                        <div class="col-12 col-md-9">
                            <input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="No Telp" value="<?php echo $no_telp; ?>" /><?php echo form_error('no_telp') ?></div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label class=" form-control-label" for="varchar">Alamat </label></div>
                            <div class="col-12 col-md-9">
                                <textarea class="form-control" rows="3" name="alamat" id="alamat" placeholder="Alamat"><?php echo $alamat; ?></textarea>
                                <?php echo form_error('alamat') ?></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row form-group">
                                <div class="col col-md-3"><label class=" form-control-label" for="varchar">Tempat Lhr </label></div>
                                <div class="col-12 col-md-9">
                                    <input type="text" class="form-control" name="tempat_lhr" id="tempat_lhr" placeholder="Tempat Lhr" value="<?php echo $tempat_lhr; ?>" /><?php echo form_error('tempat_lhr') ?></div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3"><label class=" form-control-label" for="date">Tanggal Lahir</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="date" class="form-control" name="tanggal_lhr" id="tanggal_lhr" placeholder="Tanggal Lhr" value="<?php echo $tanggal_lhr; ?>" /><?php echo form_error('tanggal_lhr') ?></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3"><label class=" form-control-label" for="varchar">Pendidikan</label></div>
                                        <div class="col-12 col-md-9">
                                            <select class="form-control" name="pendidikan" id="pendidikan" placeholder="Pendidikan">
                                                <option <?php if($pendidikan==""){ echo "selected='selected'";}?> value="">Pendidikan terakhir</option>
                                                <option <?php if($pendidikan=="SD"){ echo "selected='selected'";}?> value="SD">SD</option>
                                                <option <?php if($pendidikan=="SMP"){ echo "selected='selected'";}?> value="SMP">SMP</option>
                                                <option <?php if($pendidikan=="SMA"){ echo "selected='selected'";}?> value="SMA">SMA/SMK</option>
                                                <option <?php if($pendidikan=="D3"){ echo "selected='selected'";}?> value="D3">D3</option>
                                                <option <?php if($pendidikan=="D4"){ echo "selected='selected'";}?> value="D4">D4</option>
                                                <option <?php if($pendidikan=="S1"){ echo "selected='selected'";}?> value="S1">S1</option>
                                                <option <?php if($pendidikan=="S2"){ echo "selected='selected'";}?> value="S2">S2</option>
                                            </select>
                                            <?php echo form_error('pendidikan') ?></div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label class=" form-control-label" for="varchar">Jurusan </label></div>
                                            <div class="col-12 col-md-9">
                                                <input type="text" class="form-control" name="jurusan" id="jurusan" placeholder="Jurusan" value="<?php echo $jurusan; ?>" /><?php echo form_error('jurusan') ?></div>
                                            </div>
                                            <!-- <div class="row form-group">
                                                <div class="col col-md-3"><label class=" form-control-label" for="varchar">Foto Ktp </label></div>
                                                <div class="col-12 col-md-9">
                                                    <input type="file" class="form-control" name="foto_ktp" id="foto_ktp" placeholder="Foto Ktp" value="<?php echo $foto_ktp; ?>" /><?php echo form_error('foto_ktp') ?></div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3"><label class=" form-control-label" for="varchar">Foto Profil </label></div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="file" class="form-control" name="foto_profil" id="foto_profil" placeholder="Foto Profil" value="<?php echo $foto_profil; ?>" /><?php echo form_error('foto_profil') ?></div>
                                                    </div> -->
                <!-- <div class="row form-group">
                    <div class="col col-md-3"><label class=" form-control-label" for="datetime">C Date <?php echo form_error('c_date') ?></label></div>
                            <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="c_date" id="c_date" placeholder="C Date" value="<?php echo $c_date; ?>" /></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label class=" form-control-label" for="datetime">M Date <?php echo form_error('m_date') ?></label></div>
                            <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="m_date" id="m_date" placeholder="M Date" value="<?php echo $m_date; ?>" /></div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3"><label class=" form-control-label" for="enum">Status Aktif <?php echo form_error('status_aktif') ?></label></div>
                            <div class="col-12 col-md-9">
                    <input type="text" class="form-control" name="status_aktif" id="status_aktif" placeholder="Status Aktif" value="<?php echo $status_aktif; ?>" /></div>
                </div> -->
                <div align="right">
                    <input type="hidden" name="id_pendaftar" value="<?php echo $id_pendaftar; ?>" /> 
                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                    <a href="<?php echo site_url('c_pendaftar') ?>" class="btn btn-default">Batal</a>
                </div>
            </div>
        </form>
    </div>
</div>
</div>  
</div>
