<div id="content">
  <div class="panel">
    <div class="panel-body">
      <div class="col-lg-12">
        <h3 class="animated fadeInLeft">Data Pencari Kerja <?php echo anchor(site_url('c_pendaftar/create'),'Tambah Data', 'class="btn btn-primary"'); ?>
        <?php echo anchor(site_url('c_pendaftar/cetak_laporan'),'Cetak Laporan', 'class="btn btn-success btn-round pull-right"'); ?></h3>
        <!-- <a href="#" class="btn btn-success btn-round pull-right" data-toggle="modal" data-target="#ceksession">
            <span>Cetak Laporan</span>
            <span class="fa fa-print"></span>
        </a> -->
      </div>
      
    </div>                    
  </div>
  <div class="col-lg-12">
    <div class="panel box-v1">
     <div class="panel-body">
       <?php if(!empty($this->session->flashdata('message'))){
        echo $this->session->flashdata('message');
      } ?>
      <table id="datatable" class="table table-striped table-bordered" width="100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Email</th>
            <th>No Telp</th>
            <th>Alamat</th>
            <th>TTL</th>
            <th>Pendidikan Akhir</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $start = 0;
          foreach ($c_pendaftar_data as $c_pendaftar)
          {
            ?>
            <tr>
             <td><?php echo ++$start ?></td>
             <td><?php echo $c_pendaftar->nama ?></td>
             <td><?php echo $c_pendaftar->email ?></td>
             <td><?php echo $c_pendaftar->no_telp ?></td>
             <td><?php echo $c_pendaftar->alamat ?></td>
             <td><?php echo $c_pendaftar->tempat_lhr.", ".$this->M_tgl_indo->indonesian_date($c_pendaftar->tanggal_lhr) ?></td>
             <td><?php echo $c_pendaftar->pendidikan." ".$c_pendaftar->jurusan ?></td>
             <td style="text-align:center">
              <?php 
              echo anchor(site_url('c_pendaftar/read/'.$c_pendaftar->id_pendaftar),'<button style="margin-top: 5px; width: 120px" class="btn btn-xs btn-success"><i class="fa fa-eye"></i> Detail</button>'); 
              echo '<br>'; 
              echo anchor(site_url('c_pendaftar/update/'.$c_pendaftar->id_pendaftar),'<button style="margin-top: 5px; width: 120px" class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i> Ubah</button>'); 
              echo '<br>'; 
              echo anchor(site_url('c_pendaftar/delete/'.$c_pendaftar->id_pendaftar),'<button style="margin-top: 5px; width: 120px" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Hapus</button>','onclick="javasciprt: return confirm(\'Apakah anda akan menghapus data ini ?\')"'); 
              ?>
            </td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
</div>  
</div>
