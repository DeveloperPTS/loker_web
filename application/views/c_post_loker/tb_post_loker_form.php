<div id="content">
  <div class="panel">
    <div class="panel-body">
      <div class="col-lg-12">
        <h3 class="animated fadeInLeft"><?php echo $button ?> Data Loker</h3>
    </div>
</div>                    
</div>
<div class="col-lg-12">
    <div class="panel box-v1">
       <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" class="form-control" name="id_admin_loker" id="id_admin_loker" placeholder="Id Admin Loker" value="<?php echo $this->session->userdata('userdata')->id_admin_loker; ?>" />
        <div class="form-group">
            <label for="varchar">Judul <?php echo form_error('judul') ?></label>
            <input type="text" class="form-control" name="judul" id="judul" placeholder="Judul" value="<?php echo $judul; ?>" />
        </div>
        <div class="form-group">
            <label for="deskripsi">Deskripsi <?php echo form_error('deskripsi') ?></label>
            <textarea class="form-control" rows="3" width="100%" name="deskripsi" id="deskripsi" placeholder="Deskripsi"><?php echo $deskripsi; ?></textarea>
        </div>
        <div class="form-group">
        <label >Jenjang lowongan</label>
               <select name="jenjang" class="form-control">
                   <option value="UMUM">UMUM</option>
                   <option value="SMP">SMP</option>
                   <option value="SMA">SMA</option>
                   <option value="SMK">SMK</option>
                   <option value="D1">D1</option>
                   <option value="D2">D2</option>
                   <option value="D3">D3</option>
                   <option value="D4">D4</option>
                   <option value="S1">S1</option>
                   <option value="S2">S2</option>
               </select>
        </div>
        <div class="form-group">
        <label >Jenis Loker</label>
               <select name="jenis" class="form-control">
                <?php $jenis_loker = $this->db->get('tb_jenis_pekerjaan')->result(); 
                foreach ($jenis_loker as $key => $value) { ?>
                   <option value="<?=$value->id_jenis_pekerjaan?>"><?=$value->jenis_pekerjaan?></option>
                <?php } ?>
               </select>
        </div>
        <div class="form-group">
            <label for="date">Tanggal Berlaku <?php echo form_error('tanggal_berlaku') ?></label>
            <input type="date" class="form-control" name="tanggal_berlaku" id="tanggal_berlaku" placeholder="Tanggal Berlaku" value="<?php echo $tanggal_berlaku; ?>" />
        </div>
        <div class="form-group">
            <label for="varchar">Foto Loker <?php echo form_error('foto_loker') ?></label>
            <input type="file" class="form-control" name="foto_loker" id="foto_loker" placeholder="Foto Loker" />
        </div>
        <input type="hidden" name="id_post_loker" value="<?php echo $id_post_loker; ?>" /> 
        <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
        <a href="<?php echo site_url('c_post_loker') ?>" class="btn btn-default">Cancel</a>
    </form>
</div>
</div>
</div>  
</div>
