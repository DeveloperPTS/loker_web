<div id="content">
  <div class="panel">
    <div class="panel-body">
      <div class="col-lg-12">
        <h3 class="animated fadeInLeft">Data Loker Aktif <?php if($this->session->userdata('usertipe') == 'admin'){ echo anchor(site_url('c_post_loker/create'),'Tambah Loker', 'class="btn btn-primary"'); } ?></h3>
        <div>
        <a href="<?=base_url('C_post_loker/cetak_laporan_aktif')?>" class="btn btn-success btn-round pull-right">
            <span>Cetak Laporan</span>
            <span class="fa fa-print"></span>
        </a>
    </div>
    </div>
</div>                    
</div>
<div class="col-lg-12">
    <div class="panel box-v1">
       <div class="panel-body">
       <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
        <table id="datatable" class="table table-striped table-bordered" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Judul</th>
                    <th>Tanggal Berlaku</th>
                    <th>Foto Loker</th>
                    <th>Tanggal Buat</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $start = 0;
                foreach ($c_post_loker_data as $c_post_loker)
                {
                    ?>
                    <tr>
                       <td width="80px"><?php echo ++$start ?></td>
                       <td><?php echo $c_post_loker->judul ?></td>
                       <td><?php echo $c_post_loker->tanggal_berlaku ?></td>
                       <td><?php echo $c_post_loker->foto_loker ?></td>
                       <td><?php echo $c_post_loker->tanggal_buat ?></td>
                       <td style="text-align:center" width="200px">
                        <?php 
                        echo anchor(site_url('c_post_loker/read/'.$c_post_loker->id_post_loker),'<button style="margin-top: 5px; width: 120px" class="btn btn-xs btn-success"><i class="fa fa-eye"></i> Detail</button>'); 
                        echo ' <br/> '; 
                        echo anchor(site_url('c_post_loker/update/'.$c_post_loker->id_post_loker),'<button style="margin-top: 5px; width: 120px" class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i> Ubah</button>'); 
                        echo ' <br/> '; 
                        echo anchor(site_url('c_post_loker/delete/'.$c_post_loker->id_post_loker),'<button style="margin-top: 5px; width: 120px" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Hapus</button>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
</div>
</div>
</div>  
</div>
