<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Laporan Loker Tutup</title>

<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
        font-size: 12px;
    }
    @page {
      margin-top: 75px;
      margin-left: 75px;
      margin-right: 75px;
      /*size: landscape*/
    }
    tr.noBorder td {
  border: 0px;
}
    table{
      border-collapse: collapse;
      width: 100%;
    }
    table th{
      border:1px solid #000;
      padding: 3px;
      font-weight: bold;
      text-align: left;
    }
    table td{
      border:1px solid #000;
      padding: 3px;
      vertical-align: top;
    }
</style>

</head>
<body>
  <div>
    <div align="center">
       <p>DATA LOWONGAN PEKERJAAN TUTUP</p>
       <!-- <p>BULAN <b></b></p> -->
       <br>
       <br>
     </div>
  <table border="1" width="100%" >
    <thead align="center" valign="middle">
      <tr>
        <td rowspan=""> No</td>
        <td rowspan=""> Nama Perusahaan</td>
        <td colspan=""> Judul</td>
        <td colspan=""> Jenjang</td>
        <td colspan=""> Tanggal Posting</td>
        <td colspan=""> Tanggal Selesai</td>
      </tr>
    </thead>
    <?php $no=1; 
    foreach ($c_post_loker_data as $key => $value) { ?>
    	<tr>
	    	<td><?php echo $no++ ?></td>
	    	<td><?php echo $value->nama_perusahaan ?></td>
        <td><?php echo $value->judul ?></td>
  			<td><?php echo $value->jenjang ?></td>
  			<td><?php echo $value->tanggal_buat ?></td>
  			<td><?php echo $value->tanggal_berlaku ?></td>
		</tr>
    <?php } ?>
    <tbody>
    </tbody>
  </table>
</div>

</body>
</html>