<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Tb_post_loker <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Id Admin Loker <?php echo form_error('id_admin_loker') ?></label>
            <input type="text" class="form-control" name="id_admin_loker" id="id_admin_loker" placeholder="Id Admin Loker" value="<?php echo $id_admin_loker; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Judul <?php echo form_error('judul') ?></label>
            <input type="text" class="form-control" name="judul" id="judul" placeholder="Judul" value="<?php echo $judul; ?>" />
        </div>
	    <div class="form-group">
            <label for="deskripsi">Deskripsi <?php echo form_error('deskripsi') ?></label>
            <textarea class="form-control" rows="3" name="deskripsi" id="deskripsi" placeholder="Deskripsi"><?php echo $deskripsi; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Jenjang <?php echo form_error('jenjang') ?></label>
            <input type="text" class="form-control" name="jenjang" id="jenjang" placeholder="Jenjang" value="<?php echo $jenjang; ?>" />
        </div>
	    <div class="form-group">
            <label for="date">Tanggal Berlaku <?php echo form_error('tanggal_berlaku') ?></label>
            <input type="text" class="form-control" name="tanggal_berlaku" id="tanggal_berlaku" placeholder="Tanggal Berlaku" value="<?php echo $tanggal_berlaku; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Foto Loker <?php echo form_error('foto_loker') ?></label>
            <input type="text" class="form-control" name="foto_loker" id="foto_loker" placeholder="Foto Loker" value="<?php echo $foto_loker; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Tanggal Buat <?php echo form_error('tanggal_buat') ?></label>
            <input type="text" class="form-control" name="tanggal_buat" id="tanggal_buat" placeholder="Tanggal Buat" value="<?php echo $tanggal_buat; ?>" />
        </div>
	    <input type="hidden" name="id_post_loker" value="<?php echo $id_post_loker; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('c_post_loker') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>