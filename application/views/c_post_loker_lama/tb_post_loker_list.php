<div id="content">
  <div class="panel">
    <div class="panel-body">
      <div class="col-lg-12">
      <h3 class="animated fadeInLeft">Data Loker Tutup</h3>
      <div>
        <a href="#" class="btn btn-success btn-round pull-right" data-toggle="modal" data-target="#ceksession">
            <span>Cetak Laporan</span>
            <span class="fa fa-print"></span>
        </a>
    </div>
    </div>
</div>                    
</div>
<div class="col-lg-12">
    <div class="panel box-v1">
       <div class="panel-body">
        <table id="datatable" class="table table-striped table-bordered" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Id Admin Loker</th>
                    <th>Judul</th>
                    <th>Deskripsi</th>
                    <th>Jenjang</th>
                    <th>Tanggal Berlaku</th>
                    <th>Foto Loker</th>
                    <th>Tanggal Buat</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $start = 0;
                foreach ($c_post_loker_data as $c_post_loker)
                {
                    ?>
                    <tr>
                     <td width="80px"><?php echo ++$start ?></td>
                     <td><?php echo $c_post_loker->id_admin_loker ?></td>
                     <td><?php echo $c_post_loker->judul ?></td>
                     <td><?php if(strlen($c_post_loker->deskripsi) > 100 ){
                      echo substr($c_post_loker->deskripsi,0,strpos($c_post_loker->deskripsi,' ',100));
                    } else {
                        echo $c_post_loker->deskripsi;
                      } ?></td>
                     <td><?php echo $c_post_loker->jenjang ?></td>
                     <td><?php echo $c_post_loker->tanggal_berlaku ?></td>
                     <td><?php echo $c_post_loker->foto_loker ?></td>
                     <td><?php echo $c_post_loker->tanggal_buat ?></td>
                     <td style="text-align:center" width="200px">
                        <?php 
                        echo anchor(site_url('c_post_loker/readtutup/'.$c_post_loker->id_post_loker),'<button style="margin-top: 5px; width: 120px" class="btn btn-xs btn-success"><i class="fa fa-eye"></i> Detail</button>'); 
                        echo ' <br/> '; 
                        echo anchor(site_url('c_loker_masuk/lihat_lamaran_masuk/'.$c_post_loker->id_post_loker),'<button style="margin-top: 5px; width: 120px" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> Lihat Pelamar</button>'); 
                        echo ' <br/> '; 
                        echo anchor(site_url('c_post_loker/delete/'.$c_post_loker->id_post_loker),'<button style="margin-top: 5px; width: 120px" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Hapus</button>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                        ?>
                    </td
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <!-- <div>
      <button class="btn btn-success">Cetak Laporan</button>
    </div> -->
</div>

</div>
</div>  
</div>
<div class="modal fade" id="ceksession" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cetak Laporan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?=base_url('C_post_loker/cetak_laporan')?>" method="post" >
         <div class="form-group">
           <label>Bulan</label>
           <select class="form-control" name="bulan">
              <option>Pilih Bulan</option>
              <option value="01">January</option>
              <option value="02">February</option>
              <option value="03">March</option>
              <option value="04">April</option>
              <option value="05">May</option>
              <option value="06">June</option>
              <option value="07">July</option>
              <option value="08">August</option>
              <option value="09">September</option>
              <option value="10">October</option>
              <option value="11">November</option>
              <option value="12">December</option>
           </select>
         </div>
         <div>
           <button class="btn btn-success">Cetak</button>
         </div>
       </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
