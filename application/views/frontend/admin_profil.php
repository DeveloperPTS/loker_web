<div class="col-md-12 col-sm-12 profile-v1-body">
  <div class="col-md-7">
<?php foreach ($results  as $key => $value) { ?> 
  <div class="panel box-v7">
    <div class="panel-body">
      <div class="col-md-12 padding-0 box-v7-header">
          <div class="col-md-12 padding-0">
              <div class="col-md-10 padding-0">
              <img src="<?=base_url()?>asset/img/avatar.jpg" class="box-v7-avatar pull-left">
              <h4><a href="<?= base_url('web/detail/'.$value->id_admin_loker)?>"><?php echo $value->nama_perusahaan ?></a></h4>
              <p>Today 21:10 Am - 14.07.1997</p>
              </div>
          </div>
      </div>
     <div class="col-md-12 padding-0 box-v7-body">
      <img src="<?php echo site_url('upload/'.$value->foto_loker) ?>"  class="img-thumbnail text-center">
      <h2><?php echo $value->judul ?></h2>
        <p><?php if(strlen($value->deskripsi) > 100 )
{
     echo substr($value->deskripsi,0,strpos($value->deskripsi,' ',100));             //strpos to find ‘ ‘ after 30 characters.
}
else {
     echo $value->deskripsi;
} ?></p>
      </div>
      <div class="col-md-12 top-20">
          <?php 
           $now = date('Y-m-d');
           if ($value->tanggal_berlaku >= $now) {
           if(!empty($login)) { ?>
            <a href="<?php echo base_url('web/lamar/'.$value->id_post_loker) ?>" class="btn btn-success btn-round pull-right" >
              <span>KIRIM LAMARAN</span>
              <span class="icon-arrow-right icons"></span>
            </a>
          <?php } else { ?>
            <a href="#" class="btn btn-success btn-round pull-right" data-toggle="modal" data-target="#ceksession">
              <span>KIRIM LAMARAN</span>
              <span class="icon-arrow-right icons"></span>
            </a>
            <a href="<?= base_url('web/detail/'.$value->id_post_loker)?>" class="btn btn-primary btn-round pull-right">
            <span> Detail</span>
            <span class="icon-eye icons"></span>
          </a>
        <?php }
          } else { ?>
            <a href="#" class="btn btn-danger btn-round pull-right">
            <span> Tutup</span>
            <span class="icon-eye icons"></span>
          </a>
          <?php } ?>          
        </div>
    </div>
  </div>
<?php } ?>
<center>
  <?php if (isset($links)) { ?>
      <?php echo $links ?>
  <?php } ?>
</center>
</div>
  <div class="col-md-5">
    <div class="panel box-v3">
      <div class="panel-heading padding-0">
        <div class="box-v2-detail">
          <img src="<?php echo site_url('upload/'.$foto_perusahaan) ?>" class="img-responsive">
          <h4><?=$perusahaan?></h4>
        </div>
      </div>
  </div>
  <!-- <div class="panel box-v3">
    <div class="panel-heading bg-white border-none">
      <h4>Report</h4>
    </div>
    <div class="panel-body">
        
      <div class="media">
        <div class="media-left">
            <span class="icon-folder icons" style="font-size:2em;"></span>
        </div>
        <div class="media-body">
          <h5 class="media-heading">Document Handling</h5>
            <div class="progress progress-mini">
              <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%;">
                <span class="sr-only">60% Complete</span>
              </div>
            </div>
        </div>
      </div>

      <div class="media">
        <div class="media-left">
            <span class="icon-pie-chart icons" style="font-size:2em;"></span>
        </div>
        <div class="media-body">
          <h5 class="media-heading">UI/UX Development</h5>
            <div class="progress progress-mini">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="19" aria-valuemin="0" aria-valuemax="100" style="width: 19%;">
                <span class="sr-only">60% Complete</span>
              </div>
            </div>
        </div>
      </div>

      <div class="media">
        <div class="media-left">
            <span class="icon-energy icons" style="font-size:2em;"></span>
        </div>
        <div class="media-body">
          <h5 class="media-heading">Server Optimation</h5>
            <div class="progress progress-mini">
              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100" style="width: 55%;">
                <span class="sr-only">60% Complete</span>
              </div>
            </div>
        </div>
      </div>

      <div class="media">
        <div class="media-left">
            <span class="icon-user icons" style="font-size:2em;"></span>
        </div>
        <div class="media-body">
          <h5 class="media-heading">User Status</h5>
            <div class="progress progress-mini">
              <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:20%;">
                <span class="sr-only">60% Complete</span>
              </div>
            </div>
        </div>
      </div>

       <div class="media">
        <div class="media-left">
            <span class="icon-fire icons" style="font-size:2em;"></span>
        </div>
        <div class="media-body">
          <h5 class="media-heading">Firewall Status</h5>
            <div class="progress progress-mini">
              <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
                <span class="sr-only">60% Complete</span>
              </div>
            </div>
        </div>
      </div>
    </div>
    <div class="panel-footer bg-white border-none">
        <center>
          <input type="button" value="download as pdf" class="btn btn-danger box-shadow-none">
        </center>
    </div>
  </div>
  </div> -->
</div>