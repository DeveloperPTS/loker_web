<div class="col-md-7">
  <div class="panel box-v7">
    <div class="panel-body">
      <div class="col-md-12 padding-0 box-v7-header">
          <div class="col-md-12 padding-0">
              <div class="col-md-10 padding-0">
              <img src="<?=base_url()?>asset/img/avatar.jpg" class="box-v7-avatar pull-left">
              <h4><?php echo $isi->nama_perusahaan ?></h4>
              <p>Today 21:10 Am - 14.07.1997</p>
              </div>
          </div>
      </div>
     <div class="col-md-12 padding-0 box-v7-body">
      <img src="<?php echo site_url('upload/'.$isi->foto_loker) ?>"  class="img-thumbnail text-center">
      <h2><?php echo $isi->judul ?></h2>
        <p><?php echo $isi->deskripsi ?></p>
      </div>
      <div class="col-md-12 top-20">
           <?php if($this->session->userdata('status_login') == true) { ?>
             <a href="<?php echo base_url('web/lamar/'.$isi->id_post_loker) ?>" class="btn btn-success btn-round pull-right" >
            <span>KIRIM LAMARAN</span>
            <span class="icon-arrow-right icons"></span>
          </a>

          <?php } else { ?>
          <a href="#" class="btn btn-success btn-round pull-right" data-toggle="modal" data-target="#ceksession">
            <span>KIRIM LAMARAN</span>
            <span class="icon-arrow-right icons"></span>
          </a>
        <?php } ?>
        </div>
    </div>
  </div>
</div>