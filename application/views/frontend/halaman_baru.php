<div class="col-md-7">
  <div class="panel box-v7">
    <div class="panel-body">
      <div class="col-md-12 padding-0 box-v7-header">
        <h2><strong>Buku Tamu</strong></h2>
      </div>
      <div class="col-md-12 padding-0 box-v7-body">
        <div class="col-md-12">
          <div id="container">
            <h1>Buku Tamu</h1>
            <p>Silahkan isi buku tamu di bawah ini untuk meninggalkan pesan Anda!</p>
            <form action="<?=base_url('web/send_pesan')?>" method="post" id="form_pesan">
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="nama">Nama Lengkap</label>
                  <input type="text" class="form-control" id="nama" name="nama">
                  <span class="text-danger"> <?php echo form_error('nama'); ?></span>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" name="email">
                  <span class="text-danger"> <?php echo form_error('email'); ?></span>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-12">
                  <label for="pesan">Pesan</label>
                  <textarea class="form-control" style="height: 70px" name="pesan" id="pesan"></textarea>
                  <span class="text-danger"> <?php echo form_error('pesan'); ?></span>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <input type="submit" name="save" value="Kirim" class="btn btn-primary">
                  <input type="button" onclick="hapus()" name="reset" value="Hapus" class="btn btn-danger">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-md-12 padding-0 box-v7-comment">
          <?php 
          if ($pesan->num_rows() >0 ) {
            foreach ($pesan->result() as $key => $val) {
            $date = new DateTime($val->date_time);
            $result = $date->format('d M Y H:i');
         ?>
          <div class="media">
          <div class="media-left">
            <a href="#">
              <img src="<?=base_url()?>/asset/img/avatar2.png" class="media-object box-v7-avatar">
            </a>
          </div>
          <div class="media-body">
            <h4 class="media-heading"><?=$val->nama?></h4>
             <p><?=$val->pesan?></p>
          </div>
        </div>
        <div class="media-footer">
          <?=$result?>
        </div>
          
        <hr>
          <?php   
            }
          }else {
            echo "Blum Ada Pesan";
          } ?>
          
      </div>
    </div>
  </div>
</div>
<script>
  function hapus() {
    $('#nama').val('');
    $('#email').val('');
    $('#pesan').val('');
  }
</script>