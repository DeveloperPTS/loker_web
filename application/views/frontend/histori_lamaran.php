<div class="col-md-12">
  <div class="panel box-v7">
    <div class="panel-body">
      <div class="col-md-12 padding-0 box-v7-header">
        <h2><strong>Riwayat Lamaran</strong></h2>
      </div>
      <div class="col-md-12 padding-0 box-v7-body">
        <table class="table table-bordered" style="margin-bottom: 10px">
          <thead>
            <th>No</th>
            <th>Nama Pekerjaan</th>
            <th>Nama Perusahaan</th>
            <th>Tanggal Lamaran</th>
            <th>Status</th>
          </thead>
          <tbody>
            <?php if(!empty($lamaran)) : foreach ($lamaran as $key => $value) { 
              $perusahaan = $this->db->get_where('tb_admin_loker', array('id_admin_loker' =>$value->id_admin_loker))->row();?>
            <tr>
              <td>1</td>
              <td><?=$value->judul?></td>
              <td><?=$perusahaan->nama_perusahaan?></td>
              <td><?=$value->tanggal_masuk?></td>
              <?php if($value->status == 'terima') :?>
              <td><span class="badge badge-success">Diterima</span></td>
              <?php  elseif($value->status == 'tidak'): ?>
              <td><span class="badge badge-danger">Ditolak</span></td>
              <?php  else: ?>
              <td><span class="badge badge-primary">Belum Diverivikasi</span></td>
              <?php endif ?>
            </tr>
          <?php } ?>
          <?php endif ?>
          </tbody>
        </table>
        
      </div>
    </div>
  </div>
</div>