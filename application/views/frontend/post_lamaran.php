<div class="col-md-7">
  <div class="panel box-v7">
    <div class="panel-body">
      <div class="col-md-12 padding-0 box-v7-header">
        <div class="col-md-12 padding-0">
          <div class="col-md-12 padding-0">
            <h4>Kirim Lamaran</h4>
          </div>
        </div>
      </div>
      <div class="col-md-12 padding-0 box-v7-body">
        <?php if (!empty($error)) { ?>
           <div class="alert">
          <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
          <?php echo $error; ?>        
        </div>
       <?php } ?>
        <form action="<?php echo base_url('web/proses_lamar') ?>" method="post" enctype="multipart/form-data">
          <div class="form-group form-animate-text" style="margin-top:40px !important;">
            <input type="File" class="formtext-" id="berkas" name="berkas" required="" aria-required="true">
            <input type="hidden" name="id_post_loker" value="<?= $id_post_loker?>">
            <input type="hidden" name="id_pendaftar" value="<?= $id_pendaftar?>">
          </div>
          <div class="form-group">
            <button type="submit" class="submit btn btn-primary">
                     Lamar
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>