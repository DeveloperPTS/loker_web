<div class="col-md-7 posting">
<?php if(!empty($results)) : ?> 
<?php foreach ($results as $key => $value) { ?> 
  <div class="panel box-v7">
    <div class="panel-body">
      <div class="col-md-12 padding-0 box-v7-header">
          <div class="col-md-12 padding-0">
              <div class="col-md-10 padding-0">
              <img src="<?=base_url()?>asset/img/avatar.jpg" class="box-v7-avatar pull-left">
              <h4><a href="<?= base_url('web/profile/'.$value->id_admin_loker)?>"><?php echo $value->nama_perusahaan ?></a></h4>
              <p><?php echo date("d M Y", strtotime($value->tanggal_buat))?></p>
              </div>
          </div>
      </div>
     <div class="col-md-12 padding-0 box-v7-body">
      <img src="<?php echo site_url('upload/'.$value->foto_loker) ?>" class="img-thumbnail text-center" height="150" >
      <h2><?php echo $value->judul ?></h2>
        <p><?php if(strlen($value->deskripsi) > 100 )
{
     echo substr($value->deskripsi,0,strpos($value->deskripsi,' ',100));             //strpos to find ‘ ‘ after 30 characters.
}
else {
     echo $value->deskripsi;
} ?></p>
      </div>
      <div class="col-md-12 top-20">
           <?php if($this->session->userdata('status_login') == true) { ?>
             <a href="<?php echo base_url('web/lamar/'.$value->id_post_loker) ?>" class="btn btn-success btn-round pull-right" >
            <span>KIRIM LAMARAN</span>
            <span class="icon-arrow-right icons"></span>
          </a>

          <?php } else { ?>
          <a href="#" class="btn btn-success btn-round pull-right" data-toggle="modal" data-target="#ceksession">
            <span>KIRIM LAMARAN</span>
            <span class="icon-arrow-right icons"></span>
          </a>
        <?php } ?> 
        <a href="<?= base_url('web/detail/'.$value->id_post_loker)?>" class="btn btn-primary btn-round pull-right">
            <span> Detail</span>
            <span class="icon-eye icons"></span>
          </a>
        </div>
    </div>
  </div>
<?php } ?>
<?php  else: ?>
<div class="alert alert-outline alert-danger col-md-12 alert-dismissible" role="alert">
  <div class="col-md-12 col-md-12">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    <p> Tidak ada lowongan untuk jenjang.<strong id="stat"></strong></p>
  </div>
</div>
<?php endif ?>
<center>
  <?php if (isset($links)) { ?>
      <?php echo $links ?>
  <?php } ?>
</center>
</div>
