<div class="col-md-5">
  <div class="panel">
    <div class="panel-heading">
      <h3>Loker Hampir Tutup</h3>
    </div>
     <div class="panel-body">                 
      <div class="col-md-12 list-timeline">
        <div class="col-md-12 list-timeline-section bg-light">
          <?php foreach (recent_post() as $key => $post) { ?>
          <div class="col-md-12 list-timeline-detail">
            <h4>
              <a href="<?=base_url('web/detail/'.$post->id_post_loker.'')?>"><?=$post->judul?></a>
            </h4>
            <p>
             <?php if(strlen($post->deskripsi) > 30 )
              {
              echo substr($post->deskripsi,0,strpos($post->deskripsi,' ',30));
              }
              else {
              echo $post->deskripsi;
              } ?>
            </p>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>