<div id="content">
  <div class="panel">
    <div class="panel-body">
      <div class="col-lg-12">
        <h3 class="animated fadeInLeft"><?php echo $button ?> Data Jenis Loker</h3>
    </div>
</div>                    
</div>
<div class="col-lg-12">
    <div class="panel box-v1">
     <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post">
            <div class="col-md-12">
              <div class="row form-group">
                <div class="col col-md-3">
                  <label class=" form-control-label" for="varchar">Jenis Pekerjaan </label>
                </div>
                <div class="col-12 col-md-9">
                  <input type="text" class="form-control" name="jenis_kerjaan" id="jenis_kerjaan" placeholder="Jenis Kerjaan" value="<?php echo $jenis_kerjaan; ?>" /><?php echo form_error('jenis_kerjaan') ?>
                </div>
              </div>
             <!--  <div class="row form-group">
                <div class="col col-md-3">
                  <label class=" form-control-label" for="keterangan">Keterangan </label>
                </div>
                <div class="col-12 col-md-9">
                  <textarea class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan" value="<?php echo $keterangan; ?>" ><?php echo form_error('keterangan') ?></textarea>
                </div>
              </div>      -->    
              <div align="right">
                  <input type="hidden" name="id_jenis_pekerjaan" value="<?php echo $id_jenis_pekerjaan; ?>" /> 
                  <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                  <a href="<?php echo site_url('admin/jenis_loker') ?>" class="btn btn-default">Batal</a>
              </div>
            </div>
        </form>
    </div>
</div>
</div>  
</div>
