<div id="content">
  <div class="panel">
    <div class="panel-body">
      <div class="col-lg-12">
      <h3 class="animated fadeInLeft">Data Jenis Loker <?php echo anchor(site_url('Admin/jenis_loker_add'),'Tambah Data', 'class="btn btn-primary"'); ?>
      </h3>
      <div>
    </div>
    </div>
</div>                    
</div>
<div class="col-lg-12">
    <div class="panel box-v1">
       <div class="panel-body">
        <table id="datatable" class="table table-striped table-bordered" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Jenis Pekerjaan</th>
                    <!-- <th>Keterangan</th> -->
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $start = 0;
                foreach ($jenis_kerjaan as $value)
                {
                    ?>
                    <tr>
                     <td width="80px"><?php echo ++$start ?></td>
                     <td><?php echo $value->jenis_pekerjaan ?></td>
                     <!-- <td><?php echo $value->keterangan ?></td> -->
                     <td style="text-align:center" width="200px">
                        <?php 
                        echo anchor(site_url('admin/update_jenis/'.$value->id_jenis_pekerjaan),'<button style="margin-top: 5px; width: 120px" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i> Edit Jenis</button>'); 
                        echo ' <br/> '; 
                        echo anchor(site_url('admin/delete_jenis/'.$value->id_jenis_pekerjaan),'<button style="margin-top: 5px; width: 120px" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Hapus</button>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                        ?>
                    </td
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <!-- <div>
      <button class="btn btn-success">Cetak Laporan</button>
    </div> -->
</div>

</div>
</div>  
</div>
<div class="modal fade" id="ceksession" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cetak Laporan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?=base_url('C_post_loker/cetak_laporan')?>" method="post" >
         <div class="form-group">
           <label>Bulan</label>
           <select class="form-control" name="bulan">
              <option>Pilih Bulan</option>
              <option value="01">January</option>
              <option value="02">February</option>
              <option value="03">March</option>
              <option value="04">April</option>
              <option value="05">May</option>
              <option value="06">June</option>
              <option value="07">July</option>
              <option value="08">August</option>
              <option value="09">September</option>
              <option value="10">October</option>
              <option value="11">November</option>
              <option value="12">December</option>
           </select>
         </div>
         <div>
           <button class="btn btn-success">Cetak</button>
         </div>
       </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
