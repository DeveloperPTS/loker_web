<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="description" content="Miminium Admin Template v.1">
  <meta name="author" content="Isna Nur Azis">
  <meta name="keyword" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Admin Loker</title>

  <!-- start: Css -->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/bootstrap.min.css">

  <!-- plugins -->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/simple-line-icons.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/animate.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/icheck/skins/flat/aero.css"/>
  <link href="<?= base_url(); ?>asset/css/style.css" rel="stylesheet">
  <!-- end: Css -->

  <link rel="shortcut icon" href="<?= base_url(); ?>asset/img/logomi.png">
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
  </head>

  <body id="mimin" class="dashboard form-signin-wrapper">

      <div class="container">

        <form class="form-signin" method="post" action="<?= base_url()?>Admin/daftar_admin" enctype="multipart/form-data">
          <div class="panel periodic-login">
            <div class="panel-body text-center">
              <h1 class="atomic-symbol">LT</h1>
              <p class="atomic-mass">Daftar sebagai admin lowongan pekerjaan</p>
              <?php if(!empty($this->session->flashdata('message'))){
                echo $this->session->flashdata('message');
              } ?>
                <div class="form-group form-animate-text">
                    <input autocomplete="off"  type="text" class="form-text" name="email" required>
                    <span class="bar"></span>
                    <label>Email</label>
                </div>
                <div class="form-group form-animate-text">
                    <input autocomplete="off"  type="text" class="form-text" name="username" required>
                    <span class="bar"></span>
                    <label>Nama Pengguna</label>
                </div>
                <div class="form-group form-animate-text">
                    <input autocomplete="off"  type="text" class="form-text" name="no_siup" required>
                    <span class="bar"></span>
                    <label>Nomor Siup (Optional)</label>
                </div>
                <div class="form-group form-animate-text">
                    <input autocomplete="off"  type="text" class="form-text" name="nama_perusahaan" required>
                    <span class="bar"></span>
                    <label>Nama Perusahaan</label>
                </div>
                <div class="form-group form-animate-text">
                    <input autocomplete="off"  type="text" class="form-text" name="no_telp" required>
                    <span class="bar"></span>
                    <label>Nomor Telepon</label>
                </div>
                <div class="form-group form-animate-text">
                    <input autocomplete="off"  autocomplete="off" type="text" class="form-text" name="alamat" required>
                    <span class="bar"></span>
                    <label>Alamat Perusahaan</label>
                </div>
                <div class="form-group">
                    <label class="control-label">Lampiran foto Ktp</label>
                    <input autocomplete="off"  type="file" class="form-control" name="ktp" required>
                </div>
                <div class="form-group">
                    <label class="control-label">Lampiran SK Pimpinan Perusahaan</label>
                    <input autocomplete="off"  type="file" class="form-control" name="sk" required>
                </div>
               <div class="form-group">
                    <label class="control-label">Lampiran foto Perusahaan</label>
                    <input autocomplete="off"  type="file" class="form-control" name="perusahaan" required>
                </div>
                <input type="submit" class="btn col-md-12" value="Daftar"/>
                <a href="<?= base_url()?>Admin/pilih_reg" class="button btn btn-success">Kembali</a>
            </div>

            <div class="text-center" style="padding:5px;">
              <!-- <a href="forgotpass.html">Lupa Kata Sandi </a> -->
              Belum punya akun ?
              <a href="<?= base_url()?>Admin/pilih_reg">Daftar</a>
          </div>
      </div>
  </form>

</div>

<!-- end: Content -->
<!-- start: Javascript -->
<script src="<?= base_url(); ?>asset/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>asset/js/jquery.ui.min.js"></script>
<script src="<?= base_url(); ?>asset/js/bootstrap.min.js"></script>

<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/icheck.min.js"></script>

<!-- custom -->
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script type="text/javascript">
 $(document).ready(function(){
   $('input').iCheck({
      checkboxClass: 'icheckbox_flat-aero',
      radioClass: 'iradio_flat-aero'
  });
});
</script>
<!-- end: Javascript -->
</body>
</html>