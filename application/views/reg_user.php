<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="description" content="Miminium Admin Template v.1">
  <meta name="author" content="Isna Nur Azis">
  <meta name="keyword" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Admin Loker</title>

  <!-- start: Css -->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/bootstrap.min.css">
  <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/css/ripples.min.css"/>
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/css/bootstrap-material-design.min.css"/>
<link rel="stylesheet" href="<?= base_url(); ?>asset/css/bootstrap-material-datetimepicker.css" />

  <!-- plugins -->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/simple-line-icons.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/animate.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/icheck/skins/flat/aero.css"/>
  <link href="<?= base_url(); ?>asset/css/style.css" rel="stylesheet">
  <!-- end: Css -->

  <link rel="shortcut icon" href="<?= base_url(); ?>asset/img/logomi.png">
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
  </head>

  <body id="mimin" class="dashboard form-signin-wrapper">

      <div class="container">

        <form class="form-signin" method="post" action="<?= base_url()?>Admin/proses_reguser">
          <div class="panel periodic-login">
            <div class="panel-body text-center">
              <h1 class="atomic-symbol">LT</h1>
              <p class="atomic-mass">Daftar sebagai pencari kerja</p>
               <?php if(!empty($this->session->flashdata('regist'))){ ?>
                <div class="alert alert-success"><button">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('regist'); ?>
                </div>
                <?php } ?>
              <?php if(!empty($this->session->flashdata('error'))){ ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php } ?>
                <div class="form-group form-animate-text">
                    <input type="email" class="form-text" name="email" required>
                    <span class="bar"></span>
                    <label>Email</label>
                </div>
                 <div class="form-group form-animate-text">
                    <input type="text" class="form-text" name="username" required>
                    <span class="bar"></span>
                    <label>Username</label>
                </div>
                <div class="form-group form-animate-text">
                    <input type="text" class="form-text" name="nama" id="nama" required>
                    <span class="bar"></span>
                    <label>Nama Pengguna</label>
                </div>
                <div class="form-group form-animate-text">
                    <input type="text" class="form-text" name="alamat" required>
                    <span class="bar"></span>
                    <label>Alamat</label>
                </div>
                <div class="form-group form-animate-text">
                    <input type="text" class="form-text" name="tempat_lhr" required>
                    <span class="bar"></span>
                    <label>Tempat Lahir</label>
                </div>
                <div class="form-group form-animate-text">
                <!-- <label>Pendidikan</label> -->
                  <select class="form-control form-text" name="pendidikan">
                    <option></option>
                    <option value="S2">S2</option>
                    <option value="S1">S1</option>
                    <option value="D3">D IV</option>
                    <option value="D3">D III</option>
                    <option value="SMA">SMA/SMK</option>
                    <option value="SMP">SMP</option>
                    <option value="SD">SD</option>
                  </select>
                    <span class="bar"></span>
                    <label>Pendidikan</label>                    
                </div>
                <div class="form-group form-animate-text">
                    <input type="text" class="form-text date" id="date" name="tanggal_lhr" required>
                    <span class="bar"></span>
                    <label>Tanggal Lahir</label>
                </div>
                <div class="form-group form-animate-text">
                    <input type="text" class="form-text mask-phone" name="no_telp" required>
                    <span class="bar"></span>
                    <label>Telepon</label>
                </div>
                <div class="form-group form-animate-text">
                    <input type="password" class="form-text" name="password" required>
                    <span class="bar"></span>
                    <label>Kata Sandi</label>
                </div>
<!-- <div class="form-group form-animate-text">
                    <input type="file" class="form-control form-text" name="ktp" required>
                    <span class="bar"></span>
                    <label>Foto KTP</label>
                </div> -->
                <!-- <div class="row col-md-12">
                          <div class="form-group input-group fileupload-v1">
                            <input type="file" name="manualfile" class="fileupload-v1-file hidden"/>
                            <input type="text" class="form-control fileupload-v1-path" placeholder="File Path..." disabled>
                            <span class="input-group-btn">
                              <button class="btn fileupload-v1-btn" type="button"> Choose File</button>
                            </span>
                          </div>
                        </div> -->
                <input type="submit" class="btn col-md-12" onclick="validHuruf()" value="Daftar"/>
                <a href="<?= base_url()?>Admin/pilih_reg" class="button btn btn-success">Kembali</a>
            </div>
      </div>
  </form>

</div>

<!-- end: Content -->
<!-- start: Javascript -->
<script src="<?= base_url(); ?>asset/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>asset/js/jquery.ui.min.js"></script>
<script src="<?= base_url(); ?>asset/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/ripples.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/material.min.js"></script>

<script type="text/javascript" src="<?= base_url(); ?>asset/js/bootstrap-material-datetimepicker.js"></script>


<script src="<?= base_url(); ?>asset/js/plugins/jquery.mask.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/icheck.min.js"></script>

<!-- custom -->
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<script type="text/javascript">
 $(document).ready(function(){
  $('#date').bootstrapMaterialDatePicker
      ({
        time: false,
        clearButton: true
      });
  $('.mask-date').mask('00-00-0000');
  $('.mask-phone').mask('000000000000');
  $('input').iCheck({
      checkboxClass: 'icheckbox_flat-aero',
      radioClass: 'iradio_flat-aero'
  });
  

});
 function validHuruf()
{
  var validasiHuruf = /^[a-zA-Z ]+$/;
  var namaKota = document.getElementById("nama");
  if(namaKota.value.match(validasiHuruf))
{
  return true;
  
} else {
  alert("Nama Pengguna Wajib Huruf!");
  return false;
}
}
</script>
<!-- end: Javascript -->
</body>
</html>


