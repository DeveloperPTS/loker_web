<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta charset="utf-8">
    <meta name="description" content="Miminium Admin Template v.1">
    <meta name="keyword" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Admin Loker</title>

  <!-- start: Css -->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/datatables.bootstrap.min.css"/>
  <!-- wysihtml -->
  <!-- <script src="bower_components/wysihtml/dist/wysihtml-toolbar.min.js"></script> -->

  <!--  -->
  <link rel="stylesheet" href="<?php echo base_url('asset/')?>bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- plugins -->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/simple-line-icons.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/animate.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/fullcalendar.min.css"/>
  <link href="<?= base_url(); ?>asset/css/style.css" rel="stylesheet">
  <style type="text/css">
    .nav>li>a {
      position: relative;
      display: block;
      padding: 0px 15px;
    </style>
    <!-- end: Css -->

    <link rel="shortcut icon" href="<?= base_url(); ?>asset/img/logomi.png">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>

    <body id="mimin" class="dashboard">
      <!-- start: Header -->
      <nav class="navbar navbar-default header navbar-fixed-top">
        <div class="col-md-12 nav-wrapper">
          <div class="navbar-header" style="width:100%;">
            <div class="opener-left-menu is-open">
              <span class="top"></span>
              <span class="middle"></span>
              <span class="bottom"></span>
            </div>
            <a href="<?=base_url()?>" class="navbar-brand"> 
             <b>Loker Tegal</b>
           </a>


           <ul class="nav navbar-nav navbar-right user-nav">
           <?php if($this->session->userdata('usertipe') == "admin"){?>
            <?php if($this->session->userdata('userdata')->nama_perusahaan == "") {?>
                <li class="user-name"><span>[Nama perusahaan belum diatur]</span></li>
                <?php } else {?>
                  <li class="user-name"><span><?php echo $this->session->userdata('userdata')->nama_perusahaan; ?></span></li>
                  <?php } ?>  
            <?php } else{?>
            <li class="user-name"><span><?php echo $this->session->userdata('userdata')->nama; ?></span></li>
            <?php } ?>
            <li class="dropdown avatar-dropdown">
             <img src="<?= base_url(); ?>asset/img/avatar.jpg" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
             <ul class="dropdown-menu user-dropdown">
             <?php if($this->session->userdata('usertipe')== "admin"){?>
               <li><a href="<?= base_url('Admin/profil/'.$this->session->userdata('userdata')->id_admin_loker); ?>"><span class="fa fa-user"></span> Profil Saya</a></li>
               <?php } else {?>
                <li><a href="#"><span class="fa fa-user"></span> Profil Saya</a></li>
                <?php } ?>
               <li><a href="<?= base_url()?>admin/logout"><span class="fa fa-power-off"></span> Keluar</a></li>

             </ul>
           </li>
         </ul>
       </div>
     </div>
   </nav>
   <!-- end: Header -->

   <div class="container-fluid mimin-wrapper">

    <!-- start:Left Menu -->
    <div id="left-menu">
      <div class="sub-left-menu scroll">
        <ul class="nav nav-list">
          <li><div class="left-bg"></div></li>
          <li class="time">
            <h1 class="animated fadeInLeft">21:00</h1>
            <p class="animated fadeInRight">Sat,October 1st 2029</p>
          </li>
          <?php if($this->session->userdata('usertipe') == "superadmin"){?>
            <li><a href="<?= base_url()?>Admin/beranda">Halaman Awal</a></li>
            <li><a href="<?= base_url()?>C_pendaftar">Data Pencari Kerja </a></li>
            <li><a href="<?= base_url()?>C_admin_loker">Admin Loker Baru</a></li>
            <li><a href="<?= base_url()?>C_admin_loker/aktif">Admin Loker Aktif</a></li>
            <li><a href="<?= base_url()?>C_post_loker">Loker Aktif</a></li>
            <li><a href="<?= base_url()?>C_post_loker/tutup">Loker Tutup</a></li>
            <li><a href="<?= base_url()?>Admin/jenis_loker">Jenis Loker</a></li>
            <li><a href="<?= base_url(); ?>">Lihat Halaman Web</a></li>
            <?php }else{ ?>
              <li><a href="<?= base_url()?>Admin/beranda">Halaman Awal</a></li>
              <li><a href="<?= base_url()?>C_post_loker">Loker Aktif</a></li>
              <li><a href="<?= base_url()?>C_post_loker/tutup">Loker Tutup</a></li>
              <li><a href="<?= base_url()?>C_loker_masuk">Lamaran Masuk</a></li>
              <li><a href="<?= base_url(); ?>">Lihat Halaman Web</a></li>
              <?php }?>
            </ul>
      </div>
    </div>
    <!-- end: Left Menu -->


    <!-- start: content -->
    <?php echo $contents; ?>
  <!-- start: Mobile -->
  <!-- <div id="mimin-mobile" class="reverse">
    <div class="mimin-mobile-menu-list">
      <div class="col-md-12 sub-mimin-mobile-menu-list animated fadeInLeft">
        <ul class="nav nav-list">
          <li class="ripple">
            <a href="calendar.html">
             <span class="fa fa-calendar-o"></span>Calendar
           </a>
         </li>
       </ul>
     </div>
   </div>       
 </div>
 <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
  <span class="fa fa-bars"></span>
</button> -->
<!-- end: Mobile -->

<!-- start: Javascript -->
<script src="<?= base_url(); ?>asset/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>asset/js/jquery.ui.min.js"></script>
<script src="<?= base_url(); ?>asset/js/bootstrap.min.js"></script>


<!-- plugins -->
<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/fullcalendar.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.vmap.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/maps/jquery.vmap.world.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.vmap.sampledata.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/chart.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.datatables.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/datatables.bootstrap.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>
<!--  -->
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('asset/')?>/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

<!-- <script src="bower_components/wysihtml/parser_rules/advanced_and_extended.js"></script> -->

<!-- custom -->
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#datatable').DataTable();
  });
</script>
<script type="text/javascript">
  $('textarea').wysihtml5();
</script>

<!-- end: Javascript -->
</body>
</html>