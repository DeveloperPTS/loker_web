<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="utf-8">
	<meta name="description" content="Miminium Admin Template v.1">
	<meta name="author" content="">
	<meta name="keyword" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Loker Tegal</title>

  <!-- start: Css -->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/bootstrap.min.css">
  <script src="<?= base_url(); ?>asset/css/pace.js"></script>
  <link href="<?= base_url(); ?>asset/css/pace.css" rel="stylesheet" />
  <!-- plugins -->
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/simple-line-icons.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/mediaelementplayer.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/plugins/animate.min.css"/>
  <link href="<?= base_url(); ?>asset/css/style.css" rel="stylesheet">
  <!-- end: Css -->

  <link rel="shortcut icon" href="<?= base_url(); ?>asset/img/logomi.png">
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>

    <body id="mimin" class="dashboard topnav">
      <!-- start: Header -->
      <nav class="navbar navbar-default header navbar-fixed-top">
        <div class="col-md-12 nav-wrapper">
          <div class="navbar-header" style="width:100%;">
            <a href="<?=base_url()?>" class="navbar-brand"> 
             <b>LOKER TEGAL</b>
           </a>

           <ul class="nav navbar-nav search-nav">
            <li><a href="<?= base_url()?>">BERANDA</a></li>
            <li class="dropdown" id="jenjang">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">JENIS <span class="caret"></span></a>
              <ul class="dropdown-menu jenjang">
                <?php $jenis = $this->db->get('tb_jenis_pekerjaan')->result();
                foreach ($jenis as $key => $value) { ?>
                <li info="<?=$value->id_jenis_pekerjaan?>"><a href="#" ><?=$value->jenis_pekerjaan?></a></li>
              <?php } ?>
                <!-- <li role="separator" class="divider"></li>
                <li><a href="#">Separated link</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">One more separated link</a></li> -->
              </ul> 
            </li>
            <?php if($this->session->userdata('usertipe')=='user')
            {?>
              <li><a href="<?=base_url('web/lamaran_saya')?>">LAMARAN SAYA</a></li>
            <?php } ?>
            <!-- <li><a href="#">TENTANG KAMI</a></li> -->
            <li><a href="<?=base_url('web/contac_us')?>">KONTAK KAMI</a></li>
            <li><a href="<?=base_url('web/halaman_baru')?>">HALAMAN BARU</a></li>
            <?php if($this->session->userdata('usertipe')=='superadmin' || $this->session->userdata('usertipe')=='admin')
            {?>
            <li><a href="<?= base_url()?>Admin/beranda">HALAMAN ADMIN</a></li>
            <?php } ?>
          </ul>

          <ul class="nav navbar-nav navbar-right user-nav">
            <?php if(!$this->session->userdata('status_login') == true){?>
            <li><a href="<?= base_url()?>admin/logout" >MASUK AKUN</a></li>
            <?php }else{?>
            <?php if($this->session->userdata('usertipe') == "admin"){?>
              <?php if($this->session->userdata('userdata')->nama_perusahaan == "") {?>
                <li class="user-name"><span>[Nama perusahaan belum diatur]</span></li>
                <?php } else {?>
                  <li class="user-name"><span><?php echo $this->session->userdata('userdata')->nama_perusahaan; ?></span></li>
                  <?php } ?>  
            <?php } else{?>
            <li class="user-name"><span><?php echo $this->session->userdata('userdata')->nama; ?></span></li>
            <?php } ?>
            <li class="dropdown avatar-dropdown">
             <img src="<?= base_url(); ?>asset/img/avatar.jpg" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
             <ul class="dropdown-menu user-dropdown">
             <li><a href="#"><span class="fa fa-user"></span> Profil Saya</a></li>
               <li><a href="<?= base_url()?>web/logout"><span class="fa fa-power-off"></span> Keluar</a></li>
             </ul>
           </li>
           <?php } ?>
         </ul>
      </div>
    </div>
  </nav>
  <!-- end: Header -->
  <div id="content" class="profile-v1">
   <div class="col-md-12 col-sm-12 profile-v1-wrapper">
     <div class="col-md-12 profile-v1-cover-wrap" style="padding-right:0px;">
      <!-- <div class="profile-v1-pp">
        <img src="<?= base_url(); ?>asset/img/avatar.jpg"/>
        <h2>Akihiko Avaron</h2>
        <input type="button" class="btn btn-danger" value="follow" />
      </div> -->
      <div class="col-md-12 profile-v1-cover">
        <img src="<?= base_url(); ?>asset/img/bg1.jpg" class="img-responsive">
      </div>
    </div>
  </div>
</div>
<div class="col-md-12 col-sm-12 profile-v1-body">
  <?php if (!empty($this->session->flashdata('lamaran'))): ?>
  <div class="alert alert-info alert-dismissible">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Berhasil</strong> <?php echo $this->session->flashdata('lamaran')?>
</div>
<?php endif ?>
  <?php $this->load->view($post) ?>

  <?php if(!empty($right_bar))$this->load->view($right_bar) ?>
</div>

</div>
<!-- end: content -->





<!-- start: Mobile -->
<div id="mimin-mobile" class="reverse">
  <div class="mimin-mobile-menu-list">
    <div class="col-md-12 sub-mimin-mobile-menu-list animated fadeInLeft">
      <ul class="nav nav-list">
        <li class="active ripple">
          <a class="tree-toggle nav-header">
            <span class="fa-home fa"></span>Dashboard 
            <span class="fa-angle-right fa right-arrow text-right"></span>
          </a>
        </li>
        <li class="ripple">
          <a class="tree-toggle nav-header">
            <span class="fa-diamond fa"></span>Layout
            <span class="fa-angle-right fa right-arrow text-right"></span>
          </a>
          <ul class="nav nav-list tree">
            <li><a href="topnav.html">Top Navigation</a></li>
            <li><a href="boxed.html">Boxed</a></li>
          </ul>
        </li>
        <li class="ripple">
          <a class="tree-toggle nav-header">
            <span class="fa-area-chart fa"></span>Charts
            <span class="fa-angle-right fa right-arrow text-right"></span>
          </a>
          <ul class="nav nav-list tree">
            <li><a href="chartjs.html">ChartJs</a></li>
            <li><a href="morris.html">Morris</a></li>
            <li><a href="flot.html">Flot</a></li>
            <li><a href="sparkline.html">SparkLine</a></li>
          </ul>
        </li>
        <li class="ripple">
          <a class="tree-toggle nav-header">
            <span class="fa fa-pencil-square"></span>Ui Elements
            <span class="fa-angle-right fa right-arrow text-right"></span>
          </a>
          <ul class="nav nav-list tree">
            <li><a href="color.html">Color</a></li>
            <li><a href="weather.html">Weather</a></li>
            <li><a href="typography.html">Typography</a></li>
            <li><a href="icons.html">Icons</a></li>
            <li><a href="buttons.html">Buttons</a></li>
            <li><a href="media.html">Media</a></li>
            <li><a href="panels.html">Panels & Tabs</a></li>
            <li><a href="notifications.html">Notifications & Tooltip</a></li>
            <li><a href="badges.html">Badges & Label</a></li>
            <li><a href="progress.html">Progress</a></li>
            <li><a href="sliders.html">Sliders</a></li>
            <li><a href="timeline.html">Timeline</a></li>
            <li><a href="modal.html">Modals</a></li>
          </ul>
        </li>
        <li class="ripple">
          <a class="tree-toggle nav-header">
           <span class="fa fa-check-square-o"></span>Forms
           <span class="fa-angle-right fa right-arrow text-right"></span>
         </a>
         <ul class="nav nav-list tree">
          <li><a href="formelement.html">Form Element</a></li>
          <li><a href="#">Wizard</a></li>
          <li><a href="#">File Upload</a></li>
          <li><a href="#">Text Editor</a></li>
        </ul>
      </li>
      <li class="ripple">
        <a class="tree-toggle nav-header">
          <span class="fa fa-table"></span>Tables
          <span class="fa-angle-right fa right-arrow text-right"></span>
        </a>
        <ul class="nav nav-list tree">
          <li><a href="datatables.html">Data Tables</a></li>
          <li><a href="handsontable.html">handsontable</a></li>
          <li><a href="tablestatic.html">Static</a></li>
        </ul>
      </li>
      <li class="ripple">
        <a href="calendar.html">
         <span class="fa fa-calendar-o"></span>Calendar
       </a>
     </li>
     <li class="ripple">
      <a class="tree-toggle nav-header">
        <span class="fa fa-envelope-o"></span>Mail
        <span class="fa-angle-right fa right-arrow text-right"></span>
      </a>
      <ul class="nav nav-list tree">
        <li><a href="mail-box.html">Inbox</a></li>
        <li><a href="compose-mail.html">Compose Mail</a></li>
        <li><a href="view-mail.html">View Mail</a></li>
      </ul>
    </li>
    <li class="ripple">
      <a class="tree-toggle nav-header">
        <span class="fa fa-file-code-o"></span>Pages
        <span class="fa-angle-right fa right-arrow text-right"></span>
      </a>
      <ul class="nav nav-list tree">
        <li><a href="forgotpass.html">Forgot Password</a></li>
        <li><a href="login.html">SignIn</a></li>
        <li><a href="reg.html">SignUp</a></li>
        <li><a href="article-v1.html">Article v1</a></li>
        <li><a href="search-v1.html">Search Result v1</a></li>
        <li><a href="productgrid.html">Product Grid</a></li>
        <li><a href="profile-v1.html">Profile v1</a></li>
        <li><a href="invoice-v1.html">Invoice v1</a></li>
      </ul>
    </li>
    <li class="ripple"><a class="tree-toggle nav-header"><span class="fa "></span> MultiLevel  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
      <ul class="nav nav-list tree">
        <li><a href="view-mail.html">Level 1</a></li>
        <li><a href="view-mail.html">Level 1</a></li>
        <li class="ripple">
          <a class="sub-tree-toggle nav-header">
            <span class="fa fa-envelope-o"></span> Level 1
            <span class="fa-angle-right fa right-arrow text-right"></span>
          </a>
          <ul class="nav nav-list sub-tree">
            <li><a href="mail-box.html">Level 2</a></li>
            <li><a href="compose-mail.html">Level 2</a></li>
            <li><a href="view-mail.html">Level 2</a></li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="credits.html">Credits</a></li>
  </ul>
</div>
</div>       
</div>
<button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
  <span class="fa fa-bars"></span>
</button>
<div class="modal fade" id="ceksession" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Silahkan Login Untuk Melanjutkan Lamaran
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="<?php echo base_url('admin') ?>" type="button" class="btn btn-primary">Login</a>
      </div>
    </div>
  </div>
<!-- end: Mobile -->

<!-- start: Javascript -->
<script src="<?= base_url(); ?>asset/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>asset/js/jquery.ui.min.js"></script>
<script src="<?= base_url(); ?>asset/js/bootstrap.min.js"></script>



<!-- plugins -->
<script src="<?= base_url(); ?>asset/js/plugins/holder.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/moment.min.js"></script>
<script src="<?= base_url(); ?>asset/js/plugins/jquery.nicescroll.js"></script>


<!-- custom -->
<script src="<?= base_url(); ?>asset/js/main.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('ul.jenjang li').click(function(e) {
        var d = $(this).attr('info');
        var f = $(this).text();

        // $('#posting').hide();
        $.ajax({
        url: "<?php echo base_url('web/filter')?>/" + d,
        type : "GET",
        // dataType : "JSON",
        success : function (data) {
          $('.posting').html(data);
          $('#stat').text(f);
        }
    });
        
        console.log(d);
    })
  });

</script>
<!-- end: Javascript -->
</body>
</html>