-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 18 Jun 2019 pada 15.05
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loker_web`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `nama` varchar(25) DEFAULT NULL,
  `password` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `username`, `nama`, `password`, `created_at`) VALUES
(1, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', '2018-09-06 00:39:20'),
(2, 'kino', 'Budi kosim', 'e10adc3949ba59abbe56e057f20f883e', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin_loker`
--

CREATE TABLE `tb_admin_loker` (
  `id_admin_loker` int(11) NOT NULL,
  `nama_perusahaan` varchar(50) NOT NULL,
  `no_siup` varchar(30) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` text NOT NULL,
  `no_telp` varchar(14) NOT NULL,
  `email` varchar(30) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `ktp_admin` varchar(30) NOT NULL,
  `foto_sk` varchar(30) NOT NULL,
  `foto_perusahaan` varchar(30) NOT NULL,
  `status_aktif` enum('0','1') NOT NULL,
  `c_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `m_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_admin_loker`
--

INSERT INTO `tb_admin_loker` (`id_admin_loker`, `nama_perusahaan`, `no_siup`, `username`, `password`, `no_telp`, `email`, `alamat`, `ktp_admin`, `foto_sk`, `foto_perusahaan`, `status_aktif`, `c_date`, `m_date`, `deleted`) VALUES
(1, 'PT. HINO TEGAL', '10090076754', 'hino', 'b56adba4c9052afc1b0afb17e67f4080', '0283446766', 'hino_tegal@hino.com', 'Kramat', '', '', '15362917673252.jpg', '1', '0000-00-00 00:00:00', '2019-02-20 12:35:58', ''),
(27, 'Pt.indo', '10089389767', 'nazzul', 'b56adba4c9052afc1b0afb17e67f4080', '08777897891', 'nazzul13@gmail.com', 'Tegal', '1536291767325.jpg', '15362917673251.jpg', '15362917673252.jpg', '1', '2018-09-07 10:42:47', '2019-02-20 12:37:54', '0'),
(28, 'Flux Cafe', '10090190983', 'Flux Cafe', 'b271e110d043cc439dc6bf6b232bab6e', '08978000559', 'ardifx68@gmail.com', 'Jl. Halmahera no.23 Kota Tegal', '1547504418823.jpg', '15475044188231.jpg', '15475044188232.jpg', '1', '2019-01-15 06:20:18', '2019-02-20 12:36:36', '0'),
(34, 'Vista Komputer', '1009003763', 'Vista Komputer', '2b761a2dc1646fe97633d4f30b1d0bc8', '085788909123', 'perusahan02@gmail.com', 'Jl. ar hakim no.26 Tegal', '1550550770719.jpg', '15505507707191.jpg', '15505507707192.jpg', '1', '2019-02-19 11:32:51', '2019-02-20 12:36:42', '0'),
(35, 'Dewi Farma Apotek', '10090010865', 'Dewi Farma Apotek', 'b6672cd70297b8e8de0c25a185da896a', '087700089754', 'double68ki@gmail.com', 'Jl. asemtiga no.40 rt.09/04 keraton kota tegal', '1550551912356.jpg', '15505519123561.jpg', '15505519123562.jpg', '1', '2019-02-19 11:51:52', '2019-02-19 12:00:09', '0'),
(36, 'Trimo Bengkel Mobil', '10090045630', 'Trimo Bengkel Mobil', '64afb062f847ba9b28f813bd292ae729', '0877635545612', 'mantan.kamu69@gmail.com', 'Jl.brawijawa no.39 Tegal', '1550552206022.jpg', '15505522060221.jpg', '15505522060222.jpg', '1', '2019-02-19 11:56:46', '2019-02-19 12:00:03', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jenis_pekerjaan`
--

CREATE TABLE `tb_jenis_pekerjaan` (
  `id_jenis_pekerjaan` int(11) NOT NULL,
  `jenis_pekerjaan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jenis_pekerjaan`
--

INSERT INTO `tb_jenis_pekerjaan` (`id_jenis_pekerjaan`, `jenis_pekerjaan`) VALUES
(2, 'Administrasi'),
(3, 'Analisis Kredit'),
(4, 'Akutansi'),
(6, 'Manufactur'),
(10, 'Driver'),
(12, 'Bagian Gudang'),
(13, 'Guru'),
(15, 'Kasir'),
(16, 'Karyawan'),
(17, 'Supervisor'),
(18, 'Kolektor'),
(19, 'Marketing'),
(20, 'Trainer'),
(21, 'Operator'),
(22, 'Pramuniaga'),
(23, 'Sales'),
(24, 'Manager'),
(26, 'Teknisi'),
(27, 'Staff HRD'),
(31, 'Teller');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_loker_masuk`
--

CREATE TABLE `tb_loker_masuk` (
  `id_loker_masuk` int(11) NOT NULL,
  `id_post_loker` int(11) NOT NULL,
  `id_pendaftar` int(11) NOT NULL,
  `status` enum('terima','tidak') DEFAULT NULL,
  `lampiran` varchar(30) NOT NULL,
  `tanggal_masuk` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_loker_masuk`
--

INSERT INTO `tb_loker_masuk` (`id_loker_masuk`, `id_post_loker`, `id_pendaftar`, `status`, `lampiran`, `tanggal_masuk`) VALUES
(1, 4, 4, 'terima', '4_4.zip', '0000-00-00 00:00:00'),
(2, 3, 4, 'terima', '4_41.zip', '2018-09-23 07:15:53'),
(3, 4, 3, NULL, '4_42.zip', '2018-09-25 16:12:22'),
(4, 5, 4, NULL, '4_43.zip', '2018-09-26 02:06:33'),
(5, 6, 4, NULL, '6_4.zip', '2018-11-19 11:18:54'),
(6, 9, 12, 'terima', '9_12.zip', '2019-01-29 18:02:33'),
(7, 8, 4, 'terima', '8_42.zip', '2019-01-30 13:08:53'),
(8, 14, 28, NULL, '14_28.zip', '2019-02-22 11:13:04'),
(9, 13, 4, 'terima', '13_4.zip', '2019-02-22 16:48:28'),
(10, 12, 4, 'terima', '12_4.zip', '2019-02-22 16:52:12'),
(11, 12, 4, 'terima', '12_41.zip', '2019-02-22 16:58:13'),
(12, 11, 4, NULL, '11_4.zip', '2019-02-22 17:02:14'),
(13, 11, 4, NULL, '11_41.zip', '2019-02-22 17:02:39'),
(14, 11, 4, NULL, '11_42.zip', '2019-02-22 17:03:33'),
(15, 11, 4, NULL, '11_43.zip', '2019-02-22 17:04:29'),
(16, 11, 4, NULL, '11_44.zip', '2019-02-22 17:14:17'),
(17, 12, 4, 'terima', '12_42.zip', '2019-02-25 18:06:25'),
(18, 10, 4, NULL, '10_4.zip', '2019-02-25 18:09:22'),
(19, 14, 4, NULL, '14_4.zip', '2019-02-25 18:10:29'),
(20, 14, 4, NULL, '14_41.zip', '2019-02-25 18:12:46'),
(21, 12, 4, 'terima', '12_43.zip', '2019-02-25 18:14:21'),
(22, 12, 4, 'terima', '12_44.zip', '2019-02-26 16:51:14'),
(23, 10, 4, NULL, '10_41.zip', '2019-02-26 16:52:30'),
(24, 13, 4, 'terima', '13_41.zip', '2019-02-26 16:54:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pendaftar`
--

CREATE TABLE `tb_pendaftar` (
  `id_pendaftar` int(11) NOT NULL,
  `nama` char(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `no_telp` varchar(14) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `tempat_lhr` varchar(20) NOT NULL,
  `tanggal_lhr` date NOT NULL,
  `pendidikan` varchar(20) NOT NULL,
  `jurusan` varchar(30) NOT NULL,
  `foto_ktp` varchar(30) NOT NULL,
  `foto_profil` varchar(30) NOT NULL,
  `c_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `m_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_aktif` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pendaftar`
--

INSERT INTO `tb_pendaftar` (`id_pendaftar`, `nama`, `username`, `password`, `email`, `no_telp`, `alamat`, `tempat_lhr`, `tanggal_lhr`, `pendidikan`, `jurusan`, `foto_ktp`, `foto_profil`, `c_date`, `m_date`, `status_aktif`) VALUES
(2, 'Adi Rangga', 'adibanyak', 'b69cb5ee6af2b11cb48cbdda620944f3', 'adi_rangga@gmail.com', '0877889888', '<p>Ds.losari rt.03/rw.05 no.98 kab.brebes</p>', 'Brebes', '1996-05-12', 'D4', 'Teknik Informatika', '', '', '2018-09-01 08:12:23', '2019-02-13 11:59:31', '1'),
(4, 'Ardi Fitra', 'ardi', '0264391c340e4d3cbba430cee7836eaf', 'ardifx68@gmail.com', '089667053375', '<p>Ds.sidaharja rt.02/rw.02 no.40 kec.suradadi kab.tegal</p>', 'Tegal', '1996-01-10', 'D4', 'Informatika', '', '', '2018-09-01 08:12:23', '2019-02-13 11:57:22', '1'),
(5, 'Ibnu Bachtiar Ulum', 'ibnu', '195ace8d50de761419faf08845304398', 'ibnu_bachtiar@gmail.com', '08778896786', 'Ds.larangan rt.07/rw.02 no.45 kab.brebes', 'Tegal', '1995-03-18', 'D4', 'Teknik Informatika', '', '', '2018-09-06 18:19:26', '2019-02-13 12:01:12', '1'),
(8, 'Tesar sandy', 'ecang', '25d55ad283aa400af464c76d713c07ad', 'sandy@gmail.com', '085200532191', '<p>Jatibarang rt.03/rw.04 kab.brebes</p>', 'Tegal', '1994-12-08', 'SMK', 'Komputer jaringan', '', '', '2018-12-01 02:10:02', '2019-02-19 09:55:44', '1'),
(9, 'Anisa putri', 'sandy', '28b662d883b6d76fd96e4ddc5e9ba780', 'tes@gmail,co', '089667053705', '<p>Jl.merpati no.76 rt.04/rw.2 randugunting tegal</p>', 'Tegal', '1999-12-13', 'SMA', 'IPA', '', '', '2018-12-01 02:12:30', '2019-02-19 09:58:32', '0'),
(11, 'Rian gunawan', 'rian68gunawan', '6ec08f41d4c1f51a41e6a9b2f9a3b6d4', 'double68ki@gmail.com', '089667053375', 'jl. raya brebes no.14 rt.05/rw.01, brebes', 'brebes', '1996-12-08', 'S1', 'Teknik sipil', '', '', '2018-12-08 18:25:22', '2019-02-19 10:02:28', '0'),
(14, 'Dika Pangestu', 'Dika97', '78426e755eedc372b85f8c9c76de3452', 'Dika1997@gmail.com', '0897750377234', 'Jl.Serayu no.45 Kota Tegal', 'Tegal', '1998-02-13', 'SMK', 'Otomotif', '', '', '2019-02-13 10:32:03', '2019-02-13 11:31:53', '1'),
(15, 'Dwi sartika', 'Dwi S', '4e4a8579d8f3016b6478c6e2670315f1', 'dwi_sartika@gmail.com', '087765678982', 'Kel. kerandon rt.02 rw.45 kec.margadana', 'Tegal', '2000-04-06', 'SMA', 'IPS', '', '', '2019-02-13 10:38:39', '2019-02-13 11:39:20', '1'),
(16, 'Windya lestari', 'Windya L', '678b224a53772fdb898c077847436e66', 'Windya_lestari@gmail.com', '085237746123', 'Jl. Merpati no.143 rt.02/rw.06 kel.randugunting', 'Tegal', '2000-08-03', 'SMK', 'Tatabusana', '', '', '2019-02-13 10:42:18', '2019-02-13 11:53:08', '1'),
(17, 'Sukron ady', 'Sukron A', 'e3c260566c11ce4a306311f00f1ab957', 'Sukron_ady@gmail.com', '087789867543', 'Kel. debong tengah rt.02/rw.03 no.67', 'Tegal', '1999-11-03', 'SMP', '-', '', '', '2019-02-13 10:45:18', '2019-02-13 11:54:45', '1'),
(18, 'Muhammad F', 'Muhammad F', 'ca531e80d71fd5173cc0683910f994fb', 'Muhammad_2000@gmail.com', '085266745621', 'Kel. debong kidul rt.01/rw.02 no.32', 'Tegal', '1995-04-15', 'SMK', 'mesin idustri', '', '', '2019-02-13 10:49:13', '2019-02-19 10:03:44', '1'),
(19, 'Maulana Bahtiar', 'Maulana B', '033825c0b63234d7229a2a2f170b6752', 'Maulana _bahtiar@gmail.com', '081667675432', 'Jl. sawo rt.06/rw.05 no.28 kel.tegalsari', 'Brebes', '2000-11-09', 'SMK', 'otomotif', '', '', '2019-02-13 10:57:33', '2019-02-19 10:04:49', '1'),
(20, 'Riza fitriani', 'Riza F', 'd4fa002e87b1a6b94126d169ac06acaa', 'Riza_fitriani@gmail.com', '0897765674321', 'Jl. Merpati no.67 rt.02/rw.05 kel.randugunting', 'Tegal', '1998-01-29', 'D3', 'akutansi', '', '', '2019-02-13 11:06:28', '2019-02-19 10:06:05', '1'),
(21, 'Nur azizah', 'Nur azizah', 'adb236a2a90e5487ac1535fc80cdf382', 'Nur_azizah@gmail.com', '085276789490', 'Jl.jatisari no.98 rt.04/rw.02 kel.debong tengah', 'Tegal', '1992-11-16', 'SMA', 'IPS', '', '', '2019-02-13 11:09:19', '2019-02-19 10:08:42', '1'),
(22, 'Irma septia', 'Irma septia', 'cb3782a73b688769a28f6e9a9bd372a8', 'Irma_septia@gmail.com', '085277665878', 'kel.kalinyamat kulon rt.02/rw.02', 'Tegal', '1997-09-01', 'D3', 'farmasi', '', '', '2019-02-13 11:12:20', '2019-02-19 10:09:38', '1'),
(23, 'Renita', 'Renita', '3634afdd22d123d21875fb2f734a3323', 'Renita@gmail.com', '085244567685', 'Jl.arjuna no.3 rt.05/rw.2 kel.selerok', 'Tegal', '1997-02-15', 'SMK', 'IPS', '', '', '2019-02-13 11:16:21', '2019-02-19 10:11:39', '1'),
(24, 'Teguh arizal', 'Teguh A', 'f85df46fa882ee675156dd9b88f9c0b8', 'Teguh_arizal@gmail.com', '089776787123', 'Jl.kapuas rt.4/rw.05 kel.panggung', 'Tegal', '1987-06-24', 'SMK', 'teknik listrik', '', '', '2019-02-13 11:18:58', '2019-02-19 10:14:31', '1'),
(25, 'Utami maulina', 'Utami M', '63e887c22371e950989220b8305913b8', 'Utami_maulina@gmail.com', '087753467583', 'Jl.kol sugiarto rt.01/rw/04 kel.panggung', 'Tegal', '1994-04-19', 'S1', 'ekonomi', '', '', '2019-02-13 11:23:08', '2019-02-19 10:15:22', '1'),
(26, 'Wahyudin', 'Wahyudin', '7be947cf38aee00857f571b5dd5ef2a2', 'Wahyudin@gmail.com', '081556547800', 'Kel.tunon rt.01/rw.02', 'Tegal', '1998-01-15', 'SMK', 'otomotif', '', '', '2019-02-13 11:25:18', '2019-02-19 10:16:22', '1'),
(27, 'Yuda Triyono', 'Yuda T', 'f0fbdc8a496c48befc535c80d66528b7', 'Yuda_Triyono@gmial.com', '085245676112', 'Jl.brawijaya no.84D rt.01/rw.02 kel.muarareja', 'Tegal', '1999-10-17', 'SMK', 'teknik mesin industri', '', '', '2019-02-13 11:27:57', '2019-02-22 16:52:51', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pesan`
--

CREATE TABLE `tb_pesan` (
  `id_pesan` int(4) NOT NULL,
  `email` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `pesan` text NOT NULL,
  `date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tb_pesan`
--

INSERT INTO `tb_pesan` (`id_pesan`, `email`, `nama`, `pesan`, `date_time`) VALUES
(1, 'sandytesar@gmail.com', 'ss', 'asdsad', '2019-03-03 16:02:08'),
(2, 'sandytesar@gmail.com', 'Tesar', 'Terimakasih Telah membatu saya menemukan pekerjaan', '2019-03-03 16:03:09'),
(3, 'sandytesar@gmail.com', 'Tesar Sa', 'Terimakasih...', '2019-03-03 16:44:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_post_loker`
--

CREATE TABLE `tb_post_loker` (
  `id_post_loker` int(11) NOT NULL,
  `id_admin_loker` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `jenjang` enum('UMUM','SD','SMP','SMA','D1','D2','D3','D4','S1','S2') NOT NULL,
  `tanggal_berlaku` date NOT NULL,
  `foto_loker` varchar(30) NOT NULL,
  `tanggal_buat` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_post_loker`
--

INSERT INTO `tb_post_loker` (`id_post_loker`, `id_admin_loker`, `id_jenis`, `judul`, `deskripsi`, `jenjang`, `tanggal_berlaku`, `foto_loker`, `tanggal_buat`) VALUES
(3, 1, 12, 'Lowongan Kerja Bagian Gudang PT.Hino Tegal', '<p>Dibutuhkan karyawan bagian gudang PT.HINO TEGAL</p><p>Jl. Raya Keramat No.56, 445067 <br></p><p>Persaratan :</p><p>- Pria usia max.30 tahun</p><p>- Lulusan SMK</p><p>- Sehat dan tidak memiliki riwayat penyakit </p><p>- Memiliki keterampilan di bidang otomotif</p><p>Lampiran :</p><p>- Foto ijazah</p><p>- Foto SKCK</p><p>- Foto surat keterangan sehat</p><p>- Foto sertifikat keahlian yang dimiliki</p><p>- Foto KTP</p><p>- Foto Diri 4x6</p>', 'SMA', '2019-02-15', '1550640806834.jpg', '2018-12-05'),
(8, 28, 22, 'Lowongan kerja karyawan Flux Cafe', '<p>Dibutuhkan karyawan FLUX CAFE Tegal</p><p>Jl.Halmahera no.56 Kota Tegal</p><p>Persaratan : </p><p>- Pria/Wanita usia Max.24 tahun (Belum menikah)</p><p>- Bepenampilan menarik</p><p>- Dapat mengendarai kendaraan  bermotor dan Mempunyai SIM C</p><p>Lampiran : </p><p>- Foto ijazah terakhir</p><p>- Foto SKCK</p><p>- Foto KTP</p><p>- Foto SIM C</p><p>- FOTO Diri 4x6</p>', 'UMUM', '2019-02-27', '1550635564708.jpg', '2019-02-01'),
(9, 1, 2, 'Lowongan Kerja Karyawan Manufacturing PT.Hino Tegal', '<p>Dibutuhkan karyawan mekanik kendaraan besar PT.HINO TEGAL</p><p>\r\n\r\nJl. Raya Keramat No.56, 445067\r\n\r\n<br></p><p>Persaratan :</p><p>- Pria usia max.30 tahun</p><p>- Lulusan SMK</p><p>- Sehat dan tidak memiliki riwayat penyakit </p><p>- Memiliki keterampilan di bidang otomotif</p><p>Lampiran :</p><p>- Foto ijazah</p><p>- Foto SKCK</p><p>- Foto surat keterangan sehat</p><p>- Foto sertifikat keahlian yang dimiliki</p><p>- Foto KTP</p><p>- Foto Diri 4x6</p>', 'SMA', '2019-03-15', '1551100345212.JPG', '2019-02-15'),
(10, 35, 15, 'Lowongan karyawan apotek dewi farma', '<p>Di butuhkan karyawan dewi farma apotek </p><p>Jl.asemtiga no.40 keraton,Tegal</p><p>Persaratan :</p><p>- wanita usia max.30th</p><p>- pendidikan terakhir minimal SMA/sederajat</p><p>- berdomisili di wilayah kota tegal</p><p>Lampiran :</p><p>- foto ijazah terakhir</p><p>- foto SKCK</p><p>- foto daftar riwayat hidup</p><p>- foto KTP </p><p>- foto diri 4x6</p>', 'SMA', '2019-02-28', '1550635169888.jpg', '2019-02-20'),
(11, 28, 15, 'Lowongan kerja kasir Flux Cafe', '<p>\r\n\r\n</p><p>Dibutuhkan kasir FLUX CAFE Tegal</p><p>Jl.Halmahera no.56 Kota Tegal</p><p>Persaratan : </p><p>- Wanita usia Max.24 tahun (Belum menikah)</p><p>- Bepenampilan menarik</p><p>- Pendidikan minimal SMA/sederajat</p><p>Lampiran : </p><p>- Foto ijazah terakhir</p><p>- Foto SKCK</p><p>- Foto KTP</p><p>- Foto daftar riwayat hidup</p><p>- FOTO Diri 4x6</p>\r\n\r\n<br><p></p>', 'SMA', '2019-02-28', '1550635533211.jpg', '2019-02-20'),
(12, 34, 15, 'Lowongan Kerja Kasir Vista Komputer Tegal', '<p>\r\n\r\n</p><p>Dibutuhkan kasir Vista Kompuer Tegal</p><p>Jl.Ar Hakim no.26 Kota Tegal</p><p>Persaratan :</p><p>- Wanita/Pria usia Max.25 tahun</p><p>- Bepenampilan menarik</p><p>- Pendidikan minimal SMA/sederajat</p><p>Lampiran :</p><p>- Foto ijazah terakhir</p><p>- Foto SKCK</p><p>- Foto KTP</p><p>- Foto daftar riwayat hidup</p><p>- FOTO Diri 4x6</p><p></p>', 'SMA', '2019-02-28', '1550634409701.jpg', '2019-02-20'),
(13, 34, 26, 'Lowongan kerja Teknisi Vista Komputer Tegal', '<p>\r\n\r\n</p><p>Dibutuhkan Teknisi Vista Kompuer Tegal</p><p>Jl.Ar Hakim no.26 Kota Tegal</p><p>Persaratan :</p><p>- Pria usia Max.25 tahun</p><p>- mengetahui tantang penanganan masalah hardware dan software komputer</p><p>- Pendidikan minimal SMA/sederajat</p><p>Lampiran :</p><p>- Foto ijazah terakhir</p><p>- Foto SKCK</p><p>- Foto KTP</p><p>- Foto daftar riwayat hidup</p><p>- FOTO Diri 4x6</p>\r\n\r\n<br><p></p>', 'SMA', '2019-03-10', '1550634926612.JPG', '2019-02-20'),
(14, 36, 26, 'Lowongan Kerja Mekanik Bengkel Mobil Trimo', '<p>\r\n\r\n</p><p>Dibutuhkan mekanik bengkel mobil trimo Tegal</p><p>Jl.brawijaya no.39 Tegal</p><p>Persaratan :</p><p>- Pria usia Max.25 tahun</p><p>- mengetahui dan berpengalaman tantang penanganan masalah mesin mobil</p><p>- Pendidikan minimal SMK/sederajat</p><p>Lampiran :</p><p>- Foto ijazah terakhir</p><p>- Foto SKCK</p><p>- Foto KTP</p><p>- Foto daftar riwayat hidup</p><p>- FOTO Diri 4x6</p>\r\n\r\n<br><p></p>', 'SMA', '2019-03-10', '1550636438218.jpg', '2019-02-20'),
(15, 1, 0, 'tt', '<p>trt??</p>', 'UMUM', '2019-06-30', '1560761677440.jpg', '2019-06-17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_tamu`
--

CREATE TABLE `tb_tamu` (
  `nama_tamu` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `pesan` text NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `tb_admin_loker`
--
ALTER TABLE `tb_admin_loker`
  ADD PRIMARY KEY (`id_admin_loker`);

--
-- Indexes for table `tb_jenis_pekerjaan`
--
ALTER TABLE `tb_jenis_pekerjaan`
  ADD PRIMARY KEY (`id_jenis_pekerjaan`);

--
-- Indexes for table `tb_loker_masuk`
--
ALTER TABLE `tb_loker_masuk`
  ADD PRIMARY KEY (`id_loker_masuk`);

--
-- Indexes for table `tb_pendaftar`
--
ALTER TABLE `tb_pendaftar`
  ADD PRIMARY KEY (`id_pendaftar`);

--
-- Indexes for table `tb_pesan`
--
ALTER TABLE `tb_pesan`
  ADD PRIMARY KEY (`id_pesan`) USING BTREE;

--
-- Indexes for table `tb_post_loker`
--
ALTER TABLE `tb_post_loker`
  ADD PRIMARY KEY (`id_post_loker`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_admin_loker`
--
ALTER TABLE `tb_admin_loker`
  MODIFY `id_admin_loker` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `tb_jenis_pekerjaan`
--
ALTER TABLE `tb_jenis_pekerjaan`
  MODIFY `id_jenis_pekerjaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `tb_loker_masuk`
--
ALTER TABLE `tb_loker_masuk`
  MODIFY `id_loker_masuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tb_pendaftar`
--
ALTER TABLE `tb_pendaftar`
  MODIFY `id_pendaftar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `tb_pesan`
--
ALTER TABLE `tb_pesan`
  MODIFY `id_pesan` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_post_loker`
--
ALTER TABLE `tb_post_loker`
  MODIFY `id_post_loker` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
