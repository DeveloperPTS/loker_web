-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2019 at 08:01 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loker_web`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_post_loker`
--

CREATE TABLE IF NOT EXISTS `tb_post_loker` (
  `id_post_loker` int(11) NOT NULL,
  `id_admin_loker` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `jenjang` enum('UMUM','SD','SMP','SMA','D1','D2','D3','D4','S1','S2') NOT NULL,
  `tanggal_berlaku` date NOT NULL,
  `foto_loker` varchar(30) NOT NULL,
  `tanggal_buat` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_post_loker`
--

INSERT INTO `tb_post_loker` (`id_post_loker`, `id_admin_loker`, `id_jenis`, `judul`, `deskripsi`, `jenjang`, `tanggal_berlaku`, `foto_loker`, `tanggal_buat`) VALUES
(1, 1, 0, 'DIBUTUHKAN MEKANIK KENDARAAN BESAR', 'PT Hino Motors Manufacturing Indonesia sedang membuka lowongan kerja untuk posisi sebagai Operator Produksi. Jika Anda berminat, Anda harus memenuhi persyaratan yang sudah ditentukan sebagai berikut:\r\n\r\nPersyaratan:\r\nLaki-Laki\r\nsia Minimal 18 - 25 Maksimal\r\nPendidikan Maksimal SMK Jurusan Mesin Produksi, Otomotif, Listrik, Eloktro, maupun Mekotronika/SMA sederajat yang relavan dengan nilai rata-rata\r\nTinggi baadan minimal 165cm dengan berat badan Propesional.\r\nKesehatan dalam kondisi prima, tidak buta warna dan tidak berkacamata.\r\nBersedia kerja shift\r\nBisa bekerja dalam kelompok dan dalam tekanan\r\nMemiliki Motivasi disiplin jujur dan propesional kerja lebih di Utamakan\r\nPengalaman kerja di manufacture otomotif menjadi nilai tambah.\r\nPerempuan\r\nUsia 25 Maksimal\r\nPendidikan Maksimal SMK Jurusan, Listrik, Eloktro, maupun IPS / IPS/SMA sederajat yang relavan dengan nilai rata-rata\r\nTinggi baadan minimal 150cm dengan berat badan 50 kl,\r\nKesehatan dalam kondisi prima, tidak buta warna dan tidak berkacamata.\r\nBersedia kerja shift\r\nBisa bekerja dalam kelompok dan dalam tekanan\r\nMemiliki Motivasi disiplin jujur dan propesional kerja lebih di Utamakan\r\nTidak sedang bekerja / kuliah lebih di utama yang belum menikah\r\nPengalaman kerja di manufacture otomotif menjadi nilai tambah.\r\n\r\nBagi yang sesuai kualifikasi dan berminat untuk bekerjaan sebagai Operator Produksi di PT Hino Motors Manufacturing Indonesia, Anda bisa mempersiapkan berkas lamaran dibawah ini:\r\nDokumen lamaran\r\nSurat lamaran dan daftar riwayat hidup\r\nFoto copy Ijazah dan Transkrip Nilai\r\nFoto copy SKCK\r\nFoto copy KTP / KK / NPWP\r\nFoto copy Surat Keterangan Sehat dari Dokter\r\nFoto copy Kartu Pencari Kerja (Jika Ada)\r\nRefrensi Kerja (yang sudah pernah kerja)\r\nPas foto ukuran 3X4 2.lembar) dan 4X6 2.lembar)\r\n\r\nJika anda berminat dan dan sesuai dengan kualifikasi diatas untuk mengikuti penyeleksian kerja operator produksi PT Hino Motors Manufacturing Indonesia Tingkat Smk/Sma Sederajat', 'D3', '2018-10-10', '', '2018-09-06'),
(3, 1, 1, 'Dibutuhkan Karyawan ', 'Dibutuhkan karyawan dengan kriteria :\r\n1. Manusia\r\n2. bependidikan\r\n3. Beragama', 'UMUM', '2018-12-28', '1537662503565.jpg', '2018-12-05'),
(8, 28, 1, 'Di butuhkan karyawan Cafe', '<p>Dibutuhkan karyawan FLUX CAFE Tegal</p><p>Jl.Halmahera no.56 Kota Tegal</p><p>Persaratan : </p><p>- Pria/Wanita usia Max.24 tahun (Belum menikah)</p><p>- Bepenampilan menarik</p><p>- Dapat mengendarai kendaraan  bermotor dan Mempunyai SIM C</p><p>Lampiran : </p><p>- Foto ijazah terakhir</p><p>- Foto SKCK</p><p>- Foto KTP</p><p>- Foto SIM C</p><p>- FOTO Diri 4x6</p>', 'UMUM', '2019-01-31', '1547505912905.jpg', '2019-01-14'),
(9, 1, 1, 'Di butuhkan karyawan manufacturing', '<p>Dibutuhkan karyawan mekanik kendaraan besar PT.HINO TEGAL</p><p>\r\n\r\nJl. Raya Keramat No.56, 445067\r\n\r\n<br></p><p>Persaratan :</p><p>- Pria usia max.30 tahun</p><p>- Lulusan SMK</p><p>- Sehat dan tidak memiliki riwayat penyakit </p><p>- Memiliki keterampilan di bidang otomotif</p><p>Lampiran :</p><p>- Foto ijazah</p><p>- Foto SKCK</p><p>- Foto surat keterangan sehat</p><p>- Foto sertifikat keahlian yang dimiliki</p><p>- Foto KTP</p><p>- Foto Diri 4x6</p>', 'SMA', '2019-02-15', '1547506827971.JPG', '2019-01-15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_post_loker`
--
ALTER TABLE `tb_post_loker`
  ADD PRIMARY KEY (`id_post_loker`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_post_loker`
--
ALTER TABLE `tb_post_loker`
  MODIFY `id_post_loker` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
